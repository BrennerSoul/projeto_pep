<?php

if(!isset($_SESSION)) {
    session_start();
}

if(isset($_POST['usuario'])):

    require_once "conexao_bd/conexao_db_syspront.php";
    require_once "conexao_bd/db_syspront.class.php";

    $usuario = $_POST['usuario'];
    $senha = md5($_POST['senha']);

    $db_syspront = sysPront::getInstance(Conexao::getInstance());
    $dados = $db_syspront->validate_login($usuario, $senha);
    $nome_usuario = $db_syspront->get_nome_usuario($usuario);
    $nome_medico = $db_syspront->get_nome_medico($usuario);

    if($dados == null):

        $_SESSION['num_tentativas']++;
        $_SESSION['data_entrada'] = date('Y-m-d H:i');

        if($_SESSION['num_tentativas'] == 1):
            $_SESSION['primeiro_acesso'] = $_SESSION['data_entrada'];
        endif;

        if($_SESSION['num_tentativas'] == 5):
            $_SESSION['ultimo_acesso'] = $_SESSION['data_entrada'];
        endif;

        header('Location: index.php#erro=1');

    else:

        $_SESSION['num_tentativas'] = 0;
        $_SESSION['id_usuario'] = $dados[0]->id_criptografado;
        $_SESSION['usuario'] = $dados[0]->nome_usuario;
        $_SESSION['tipo_usuario'] = $dados[0]->tipo_usuario;

        if($dados[0]->tipo_usuario == 'Administrador')
            $_SESSION['nome_do_usuario'] = $dados[0]->nome_usuario;
        else if($dados[0]->tipo_usuario == 'Discente')
            $_SESSION['nome_do_usuario'] = strstr($nome_usuario[0]->nome_discente, ' ', true);
        else if($dados[0]->tipo_usuario == 'Medico')
            $_SESSION['nome_do_usuario'] = strstr($nome_medico[0]->nome_medico, ' ', true);

        $db_syspront->gravar('', 'Acao: login; Usuario: ' . $dados[0]->nome_usuario . '; ');
        header('Location: prontuario/cadastro.php');

    endif;

else:
    if(!isset($_SESSION['usuario'])) header('Location: index.php#erro=1');
    else header('Location: prontuario/cadastro.php');
endif;