<?php

  if(!isset($_SESSION)) {
    session_start();
  }

  if(isset($_POST['nome'])):

    require_once "../conexao_bd/conexao_db_syspront.php";
    require_once "../conexao_bd/db_syspront.class.php";

    $db_syspront = sysPront::getInstance(Conexao::getInstance());

    $nome = $_POST['nome'];
    $especialidade = $_POST['especialidade'];
    $crm = $_POST['crm'];

    $erro = $db_syspront->insert_medico($nome, $especialidade, $crm);

    if($erro == 1062){
      header('Location: cadastro.php#success=false');
    } else {
      $db_syspront->gravar('../', 'Acao: cadastro_medico; Usuario: ' . $_SESSION['usuario'] . '; ');
      header('Location: cadastro.php#success=true');
    }

  else:
    if(!isset($_SESSION['usuario'])) header('Location: ../index.php#erro=1');
    else header('Location: consulta.php');

  endif;