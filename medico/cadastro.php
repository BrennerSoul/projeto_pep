<?php

    if(!isset($_SESSION)) {
        session_start();
    }

    if(!isset($_SESSION['usuario']) || $_SESSION['tipo_usuario'] != 'Administrador'){
        header('Location: ../index.php#erro=1');
    }

?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
 <meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="shortcut icon" href="../img/favicon.ico">
 <title>Projeto SysPront Fisioterapia - Cadastro de médicos</title>

 <script type="text/javascript" src="../js/jquery.min.js"></script>
 <link href="../css/bootstrap.min.css" rel="stylesheet">
 <link rel="stylesheet" type="text/css" href="../css/sys_pront_custom.css">

 <script type="text/javascript">

    $(document).ready(function(){

/*#################################################################################################*/
        //Controle do timeout
        var intervalo = '';

        contagem_timeout();
        intervalo = setTimeout(contagem_timeout, 1801000);

        function contagem_timeout(){
            $.ajax({
                url: '../sessao_timeout.php',
                method: 'post',
                data: { sessao: true },
                success: function(data){
                    if(data == 'logout'){
                        $.ajax({
                            url: '../logout.php',
                            method: 'post',
                            data: { logout: true,
                                    timeout: true },
                            success: function(data){
                                window.location.href = '../' + data;
                            }
                        });
                    }
                }
            });
        }

        $(document).off('mousedown').on('mousedown', function(){
            clearTimeout(intervalo);
            intervalo = setTimeout(contagem_timeout, 1801000);
        });

        $(document).off('mousemove').on('mousemove', function(){
            clearTimeout(intervalo);
            intervalo = setTimeout(contagem_timeout, 1801000);
        });

        $(document).off('keydown').on('keydown', function(){
            clearTimeout(intervalo);
            intervalo = setTimeout(contagem_timeout, 1801000);
        });

/*#################################################################################################*/
        //Controle da sidebar de acordo com o tamanho da tela
        //Ao redimensionar a tela, certifica-se de que a sidebar esteja na posição correta
        $(window).resize(function(){

            if($(window).width() >= 992){
                $("#sidebar-wrapper").css('width', 220);
                $("#page-content-wrapper").css('padding-left', 10);
                $("#page-content-wrapper").css('margin-left', 230);
            } else {
                $("#sidebar-wrapper").css('width', 0);
                $("#page-content-wrapper").css('padding-left', 15);
                $("#page-content-wrapper").css('margin-left', 0);
            }

        });

        //Ao clicar, mostra a sidebar
        $("#mostra-menu").click(function(e) {
            e.preventDefault();
            $("#sidebar-wrapper").css('width', 220);
            $("#page-content-wrapper").css('padding-left', 10);
            $("#page-content-wrapper").css('margin-left', 0);
        });

        //Ao clicar, fecha a sidebar
        $("#fecha-menu").click(function(e) {
            e.preventDefault();
            $("#sidebar-wrapper").css('width', 0);
            $("#page-content-wrapper").css('padding-left', 15);
            $("#page-content-wrapper").css('margin-left', 0);
        });

/*#################################################################################################*/

        $('#page-content-wrapper').fadeIn(500);
        $('#nome').focus();

        var hash_url = $(location).attr('hash');

        if(hash_url == '#success=true'){

            $('#modal-sucesso').modal();
            $('#page-content-wrapper').hide();

        } else if(hash_url == '#success=false'){

            $('#modal-erro-cadastro').modal();
            $('#page-content-wrapper').hide();

        }

        //Ao fechar modal sucesso
        $('#modal-sucesso').on('hidden.bs.modal', function (e) {
            window.location.replace('cadastro.php');
        });

        //Ao fechar modal sucesso
        $('#modal-erro-cadastro').on('hidden.bs.modal', function (e) {
            window.location.replace('cadastro.php');
        });

        $(document).off('keydown').keydown(function(event){
            var keycode = (event.keyCode ? event.keyCode : event.which);

            //Tecla enter
            if(keycode == '13'){
                submit_formulario();
            } else
            if(keycode == '27'){
                $('#modal-sucesso').modal('hide');
            }
        });

        //Validações dos campos de cadastro
        $('#btn-logar').off('click').click(function(){

            submit_formulario();

        });

        //Fecha os avisos
        $('#btn-close-warning').off('click').click(function(){

            $('.alert').fadeOut();

        });

        $('.btn-reset').off('click').click(function(){

            $('#nome').css('border-color', '');
            $('#especialidade').css('border-color', '');
            $('#crm').css('border-color', '');
            $('.alert').fadeOut();
            $('#nome').focus();

        });

        $('a').off('click').click(function(){
            if($(this).attr('id') == 'btn_logout'){
                $.ajax({
                    url: '../logout.php',
                    method: 'post',
                    data: { logout: true },
                    success: function(data){
                        window.location.href = '../' + data;
                    }
                });
            } else {
                event.preventDefault();
                linkLocation = this.href;
                $('#page-content-wrapper').fadeOut(200, function(){
                    window.location.href = linkLocation;
                });
            }
        });

        function submit_formulario(){

            var campos_vazios = false;
            var warning_text = '';

            if($('#nome').val() == ''){
                warning_text += 'Campo Nome completo deve ser preenchido!<br>';
                $('#nome').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#nome').css('border-color', '');
            }

            if($('#especialidade').val() == ''){
                warning_text += 'Campo Especialidade deve ser preenchido!<br>';
                $('#especialidade').css('border-color', 'red');

                campos_vazios = true;
            } else {
                $('#especialidade').css('border-color', '');
            }

            if($('#crm').val() == ''){
                warning_text += 'Campo CRM deve ser preenchido!<br>';
                $('#crm').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#crm').css('border-color', '');
            }

            if(campos_vazios){
                $('.alert').fadeIn();
                $('#warning-text').html(warning_text);
            } else {
                $('.alert').fadeOut();
                $('#form-cadastro').attr("action","bd_cadastro.php");
                $('#page-content-wrapper').fadeOut(200, function(){
                    $("#form-cadastro").submit();
                });
            }

        }

    });

 </script>

</head>
<body>

    <!-- Barra de navegação -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button id="btn_menu" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar_menu" aria-expanded="false" aria-controls="navbar" title="Menu">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div id="navbar" style="padding-left: 5px; padding-top: 2px;">
                    <h4>SISTEMA DE CONTROLE DE PRONTUÁRIO DE ESTAGIÁRIOS DO CURSO DE FISIOTERAPIA</h4>
                </div>
            </div>
            <div id="navbar" class="navbar-collapse collapse navbar_menu" style="padding-right: 5px;">
                <ul class="nav navbar-nav navbar-right" style="font-size: 17px;">
                    <li><h4 style="color: white; text-shadow: 2px 2px #000; margin-right: 20px; margin-top: 15px; padding-left: 5px;">Bem vindo, <?=$_SESSION['nome_do_usuario']?></h4></li>
                    <li><a href="../prontuario/cadastro.php" title="Início">
                        <span class="glyphicon glyphicon-home"></span>
                    </a></li>
                    <li><a href="../perfil/consulta.php" title="Gerenciar perfis">
                        <span class="glyphicon glyphicon-user"></span>
                    </a></li>
                    <li><a style="cursor: pointer;" id="btn_logout" title="Sair">
                        <span class="glyphicon glyphicon-log-out"></span>
                    </a></li>
                </ul>
            </div>
        </div>
    </nav> <!-- navbar -->

    <div class="container-fluid">
        <div>
            <button id="mostra-menu" type="button" title="Mostrar menu">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </button>

            <!-- Sidebar -->
            <div id="sidebar-wrapper">
                <ul class="sidebar-nav">
                    <button id="fecha-menu" title="Fechar" type="button" class="close" aria-label="Close" style="background: white; margin-right: 20px; padding: 0px 3px;">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <li class="topicos"> <strong style="font-size: 17px">CADASTROS</strong> </li>
                    <li class="selected">
                        <a href="cadastro.php"><strong>MÉDICO</strong></a>
                    </li>
                    <li>
                        <a href="../docente/cadastro.php"><strong>DOCENTE</strong></a>
                    </li>
                    <li>
                        <a href="../paciente/cadastro.php"><strong>PACIENTE</strong></a>
                    </li>
                    <li>
                        <a href="../discente/cadastro.php"><strong>DISCENTE</strong></a>
                    </li>
                    <li class="topicos"> <strong style="font-size: 17px">MOVIMENTOS</strong> </li>
                    <li>
                        <a href="../prontuario/cadastro.php"><strong>PRONTUÁRIO</strong></a>
                    </li>
                    <li class="topicos"> <strong style="font-size: 17px">CONSULTAS</strong> </li>
                    <li>
                        <a href="../prontuario/consulta.php"><strong>PRONTUÁRIO</strong></a>
                    </li>
                    <li>
                        <a href="consulta.php"><strong>MÉDICO</strong></a>
                    </li>
                    <li>
                        <a href="../docente/consulta.php"><strong>DOCENTE</strong></a>
                    </li>
                    <li>
                        <a href="../paciente/consulta.php"><strong>PACIENTE</strong></a>
                    </li>
                    <li>
                        <a href="../discente/consulta.php"><strong>DISCENTE</strong></a>
                    </li>
                </ul>
            </div> <!-- /#sidebar-wrapper -->

        </div>

        <!-- Modal cadastro com sucesso -->
        <div class="modal fade" id="modal-sucesso">
            <div class="modal-dialog">
                <div class="modal-content" style="border: 3px solid black">
                    <div class="modal-header" style="text-align: center;">
                        <button type="button" class="close" data-dismiss="modal"><span style="color: red">×</span></button>
                        <h4 class="modal-title" style="color: #01C325;">Registro inserido com sucesso!</h4>
                    </div>
                </div>
            </div>
        </div> <!--Modal-->

        <!-- Modal de erro de cadastro -->
        <div class="modal fade" id="modal-erro-cadastro">
            <div class="modal-dialog">
                <div class="modal-content" style="border: 3px solid #FA5B5B;">
                    <div class="modal-header" style="text-align: center;">
                        <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                        <h4 class="modal-title" style="color: red;">ERRO AO CADASTRAR MÉDICO</h4>
                    </div>
                    <div class="modal-body" style="text-align: center; font-weight: bold;">
                        Não é possível cadastrar este médico, porque já existe um registro com este CRM! Por favor, escolha um CRM disponível!
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
        </div> <!-- Modal -->

        <div id="page-content-wrapper" style="display: none;">

            <h3 class="page-header">Adicionar Médico</h3>

            <form id="form-cadastro" action="" method="post">
            <!-- area de campos do form -->
                <div class="row">
                    <div class="form-group col-sm-12">
                        <label for="nome">Nome completo <span style="color: red;">*</span></label>
                        <input type="text" class="form-control" id="nome" name="nome" maxlength="160">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-sm-6">
                        <label for="especialidade">Especialidade <span style="color: red;">*</span></label>
                        <input type="text" class="form-control" id="especialidade" name="especialidade" maxlength="60">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="crm">CRM <span style="color: red;">*</span></label>
                        <input type="text" class="form-control" id="crm" name="crm" maxlength="20">
                    </div>
                </div>
                <hr>
                <div id="actions" class="row">
                    <div class="col-md-12" style="text-align: center; margin-bottom: 15px">
                        <button id="btn-logar" type="button" class="btn btn-primary">Salvar</button>
                        <a href="consulta.php" class="btn btn-default">Cancelar</a>
                        <button type="reset" class="btn btn-danger btn-reset">Resetar</button>
                        <a href="consulta.php" class="btn btn-warning" style="color: black">Ver médicos cadastrados</a>
                    </div>
                </div>
                <div class="row">
                    <div class="alert alert-danger" style="display: none; text-align: center;">
                        <button id="btn-close-warning" type="button" class="close" title="Fechar"><span>×</span></button>
                        <strong id="warning-text"></strong>
                    </div>
                </div>
            </form>
        </div> <!-- page-content-wrapper -->
    </div>

 <script src="../js/bootstrap.min.js"></script>
</body>
</html>