<?php

  if(!isset($_SESSION)) {
    session_start();
  }

  if(isset($_POST['id_discente'])):

    require_once "../conexao_bd/conexao_db_syspront.php";
    require_once "../conexao_bd/db_syspront.class.php";

    $db_syspront = sysPront::getInstance(Conexao::getInstance());

    $id_discente = $_POST['id_discente'];

    $dados = $db_syspront->search_discente_by_id($id_discente);

    //Modal detalhes
    echo '<div class="modal fade" id="modal-mensagem">';
        echo '<div class="modal-dialog modal-mensagem-class">';
            echo '<div class="modal-content">';
                echo '<div class="modal-header" style="text-align: center;">';
                    echo '<button type="button" class="close" data-dismiss="modal"><span>×</span></button>';
                    echo '<h4 class="modal-title">Dados completos do discente</h4>';
                echo '</div>';
                echo '<div class="modal-body">';
                    echo '<div class="table-responsive">';
                        echo '<table class="table table-hover table-bordered">';
                            echo '<thead class="table-custom">';
                                echo '<tr>';
                                    echo '<th scope="col">Matrícula</th>';
                                    echo '<th scope="col">Nome</th>';
                                    echo '<th scope="col">Período</th>';
                                echo '</tr>';
                            echo '</thead>';
                            echo '<tbody>';
                                echo '<tr>';
                                    echo '<td>'.$dados[0]->num_matricula.'</td>';
                                    echo '<td>'.$dados[0]->nome_discente.'</td>';
                                    echo '<td>'.$dados[0]->periodo_faculdade.'</td>';
                                echo '</tr>';
                            echo '</tbody>';
                        echo '</table>';
                    echo '</div>';
                echo '</div>';
                echo '<div class="modal-footer">';
                    echo '<button class="btn btn-success btn_editar" data-id_discente="'. $dados[0]->id_discente.'">Editar</button>';
                    echo '<button class="btn btn-danger btn_remover" data-toggle="modal" data-target="#modal-remover" data-id_discente="'. $dados[0]->id_discente .'">Remover</button>';
                    echo '<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>';
                echo '</div>';
            echo '</div>';
        echo '</div>';
    echo '</div>';

  else:
    if(!isset($_SESSION['usuario'])) header('Location: ../index.php#erro=1');
    else header('Location: consulta.php');

  endif;