<?php

    if(!isset($_SESSION)) {
        session_start();
    }

    if(!isset($_SESSION['usuario']) || $_SESSION['tipo_usuario'] != 'Administrador'){
        header('Location: ../index.php#erro=1');
    }

    require_once "../conexao_bd/conexao_db_syspront.php";
    require_once "../conexao_bd/db_syspront.class.php";

    $id_discente = isset($_GET['value']) ? $_GET['value'] : 0;
    $num_erro = isset($_GET['erro']) ? $_GET['erro'] : 0;

    $db_syspront = sysPront::getInstance(Conexao::getInstance());
    $dados = $db_syspront->search_discente_by_id($id_discente);

?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
 <meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="shortcut icon" href="../img/favicon.ico">
 <title>Projeto SysPront Fisioterapia - Editar discente</title>

 <script type="text/javascript" src="../js/jquery.min.js"></script>
 <script type="text/javascript" src="../js/jquery.mask.min.js"></script>
 <link href="../css/bootstrap.min.css" rel="stylesheet">
 <link rel="stylesheet" type="text/css" href="../css/sys_pront_custom.css">

 <script type="text/javascript">

    $(document).ready(function(){

/*#################################################################################################*/
        //Controle do timeout
        var intervalo = '';

        contagem_timeout();
        intervalo = setTimeout(contagem_timeout, 1801000);

        function contagem_timeout(){
            $.ajax({
                url: '../sessao_timeout.php',
                method: 'post',
                data: { sessao: true },
                success: function(data){
                    if(data == 'logout'){
                        $.ajax({
                            url: '../logout.php',
                            method: 'post',
                            data: { logout: true,
                                    timeout: true },
                            success: function(data){
                                window.location.href = '../' + data;
                            }
                        });
                    }
                }
            });
        }

        $(document).off('mousedown').on('mousedown', function(){
            clearTimeout(intervalo);
            intervalo = setTimeout(contagem_timeout, 1801000);
        });

        $(document).off('mousemove').on('mousemove', function(){
            clearTimeout(intervalo);
            intervalo = setTimeout(contagem_timeout, 1801000);
        });

        $(document).off('keydown').on('keydown', function(){
            clearTimeout(intervalo);
            intervalo = setTimeout(contagem_timeout, 1801000);
        });

/*#################################################################################################*/
        //Controle da sidebar de acordo com o tamanho da tela
        //Ao redimensionar a tela, certifica-se de que a sidebar esteja na posição correta
        $(window).resize(function(){

            if($(window).width() >= 992){
                $("#sidebar-wrapper").css('width', 220);
                $("#page-content-wrapper").css('padding-left', 10);
                $("#page-content-wrapper").css('margin-left', 230);
            } else {
                $("#sidebar-wrapper").css('width', 0);
                $("#page-content-wrapper").css('padding-left', 15);
                $("#page-content-wrapper").css('margin-left', 0);
            }

        });

        //Ao clicar, mostra a sidebar
        $("#mostra-menu").click(function(e) {
            e.preventDefault();
            $("#sidebar-wrapper").css('width', 220);
            $("#page-content-wrapper").css('padding-left', 10);
            $("#page-content-wrapper").css('margin-left', 0);
        });

        //Ao clicar, fecha a sidebar
        $("#fecha-menu").click(function(e) {
            e.preventDefault();
            $("#sidebar-wrapper").css('width', 0);
            $("#page-content-wrapper").css('padding-left', 15);
            $("#page-content-wrapper").css('margin-left', 0);
        });

/*#################################################################################################*/

        $('#page-content-wrapper').fadeIn(500);

        //Mascarando inputs
        $('#periodo').mask('90');
        $('#matricula').mask('00099999999');

        $('#nome').focus();

        if($('#modal-erro-edicao').data('erro') == '1062'){

            $('#modal-erro-edicao').modal();
            $('#page-content-wrapper').hide();

        }

        //Ao fechar modal erro
        $('#modal-erro-edicao').on('hidden.bs.modal', function (e) {

            window.location.replace('alteracao.php?value=' + $('#id').val());
        });

        $(document).off('keydown').keydown(function(event){
            var keycode = (event.keyCode ? event.keyCode : event.which);

            //Tecla enter
            if(keycode == '13'){
                submit_formulario();
            }
        });

        //Validações dos campos de cadastro
        $('#btn-logar').off('click').click(function(){

            submit_formulario();

        });

        //Fecha os avisos
        $('#btn-close-warning').off('click').click(function(){

            $('.alert').fadeOut();

        });

        $('.btn-reset').off('click').click(function(){

            $('#nome').css('border-color', '');
            $('#nome').val('');
            $('#periodo').css('border-color', '');
            $('#periodo').val('');
            $('#matricula').css('border-color', '');
            $('#matricula').val('');
            $('.alert').fadeOut();
            $('#nome').focus();

        });

        $('a').off('click').click(function(){
            if($(this).attr('id') == 'btn_logout'){
                $.ajax({
                    url: '../logout.php',
                    method: 'post',
                    data: { logout: true },
                    success: function(data){
                        window.location.href = '../' + data;
                    }
                });
            } else {
                event.preventDefault();
                linkLocation = this.href;
                $('#page-content-wrapper').fadeOut(200, function(){
                    window.location.href = linkLocation;
                });
            }
        });

        //Faz as devidas validações e se aprovadas dá o submit no formulário
        function submit_formulario(){

            var campos_vazios = false;
            var warning_text = '';

            if($('#nome').val() == ''){
                warning_text += 'Campo Nome completo deve ser preenchido!<br>';
                $('#nome').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#nome').css('border-color', '');
            }

            if($('#periodo').val() == ''){
                warning_text += 'Campo Período da faculdade deve ser preenchido!<br>';
                $('#periodo').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#periodo').css('border-color', '');
            }

            if($('#matricula').val() == ''){
                warning_text += 'Campo Número da matrícula deve ser preenchido!<br>';
                $('#matricula').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#matricula').css('border-color', '');
            }

            if(campos_vazios){
                $('.alert').fadeIn();
                $('#warning-text').html(warning_text);
            } else {
                $('.alert').fadeOut();
                $('#form-edicao').attr("action","bd_alteracao.php");
                $('#page-content-wrapper').fadeOut(200, function(){
                    $("#form-edicao").submit();
                });
            }
        }
    });

 </script>

</head>
<body>

    <!-- Barra de navegação -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button id="btn_menu" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar_menu" aria-expanded="false" aria-controls="navbar" title="Menu">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div id="navbar" style="padding-left: 5px; padding-top: 2px;">
                    <h4>SISTEMA DE CONTROLE DE PRONTUÁRIO DE ESTAGIÁRIOS DO CURSO DE FISIOTERAPIA</h4>
                </div>
            </div>
            <div id="navbar" class="navbar-collapse collapse navbar_menu" style="padding-right: 5px;">
                <ul class="nav navbar-nav navbar-right" style="font-size: 17px;">
                    <li><h4 style="color: white; text-shadow: 2px 2px #000; margin-right: 20px; margin-top: 15px; padding-left: 5px;">Bem vindo, <?=$_SESSION['nome_do_usuario']?></h4></li>
                    <li><a href="../prontuario/cadastro.php" title="Início">
                        <span class="glyphicon glyphicon-home"></span>
                    </a></li>
                    <li><a href="../perfil/consulta.php" title="Gerenciar perfis">
                        <span class="glyphicon glyphicon-user"></span>
                    </a></li>
                    <li><a style="cursor: pointer;" id="btn_logout" title="Sair">
                        <span class="glyphicon glyphicon-log-out"></span>
                    </a></li>
                </ul>
            </div>
        </div>
    </nav> <!-- navbar -->

    <div class="container-fluid">
        <div>
            <button id="mostra-menu" type="button" title="Mostrar menu">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </button>

            <!-- Sidebar -->
            <div id="sidebar-wrapper">
                <ul class="sidebar-nav">
                    <button id="fecha-menu" title="Fechar" type="button" class="close" aria-label="Close" style="background: white; margin-right: 20px; padding: 0px 3px;">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <li class="topicos"> <strong style="font-size: 17px">CADASTROS</strong> </li>
                    <li>
                        <a href="../medico/cadastro.php"><strong>MÉDICO</strong></a>
                    </li>
                    <li>
                        <a href="../docente/cadastro.php"><strong>DOCENTE</strong></a>
                    </li>
                    <li>
                        <a href="../paciente/cadastro.php"><strong>PACIENTE</strong></a>
                    </li>
                    <li>
                        <a href="cadastro.php"><strong>DISCENTE</strong></a>
                    </li>
                    <li class="topicos"> <strong style="font-size: 17px">MOVIMENTOS</strong> </li>
                    <li>
                        <a href="../prontuario/cadastro.php"><strong>PRONTUÁRIO</strong></a>
                    </li>
                    <li class="topicos"> <strong style="font-size: 17px">CONSULTAS</strong> </li>
                    <li>
                        <a href="../prontuario/consulta.php"><strong>PRONTUÁRIO</strong></a>
                    </li>
                    <li>
                        <a href="../medico/consulta.php"><strong>MÉDICO</strong></a>
                    </li>
                    <li>
                        <a href="../docente/consulta.php"><strong>DOCENTE</strong></a>
                    </li>
                    <li>
                        <a href="../paciente/consulta.php"><strong>PACIENTE</strong></a>
                    </li>
                    <li>
                        <a href="consulta.php"><strong>DISCENTE</strong></a>
                    </li>
                </ul>
            </div> <!-- /#sidebar-wrapper -->

        </div>

        <!-- Modal de erro de edicao -->
        <div class="modal fade" id="modal-erro-edicao" data-erro="<?= $num_erro ?>">
            <div class="modal-dialog">
                <div class="modal-content" style="border: 3px solid #FA5B5B;">
                    <div class="modal-header" style="text-align: center;">
                        <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                        <h4 class="modal-title" style="color: red;">ERRO AO EDITAR DISCENTE</h4>
                    </div>
                    <div class="modal-body" style="text-align: center; font-weight: bold;">
                        Não é possível editar este discente, porque já existe um registro com este número de matrícula! Por favor, escolha um número de matrícula disponível!
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
        </div> <!-- Modal -->

        <div id="page-content-wrapper"  style="display: none">

            <?php if($dados == null):
                echo '<div class="alert alert-danger" style="text-align: center; margin-top: 50px;">';
                echo '<h4>Informações não encontradas!</h4>';
                echo '</div>';
            else: ?>

            <h3 class="page-header">Editar Discente</h3>

            <form id="form-edicao" action="" method="post">
            <!-- area de campos do form -->
            <input type="hidden" id="id" name="id" value="<?= $dados[0]->id_ds_criptografado ?>">
                <div class="row">
                    <div class="form-group col-sm-12">
                        <label for="nome">Nome completo <span style="color: red;">*</span></label>
                        <input type="text" class="form-control" id="nome" name="nome" value="<?= $dados[0]->nome_discente ?>" maxlength="160">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-sm-6">
                        <label for="periodo">Período da faculdade <span style="color: red;">*</span></label>
                        <input type="text" class="form-control" id="periodo" name="periodo" value="<?= $dados[0]->periodo_faculdade ?>">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="matricula">Número da matrícula <span style="color: red;">*</span></label>
                        <input type="text" class="form-control" id="matricula" name="matricula" value="<?= $dados[0]->num_matricula ?>">
                    </div>
                </div>
                <hr>
                <div id="actions" class="row">
                    <div class="col-md-12" style="text-align: center; margin-bottom: 15px">
                        <button id="btn-logar" type="button" class="btn btn-primary">Salvar</button>
                        <a href="consulta.php" class="btn btn-default">Cancelar</a>
                        <button type="button" class="btn btn-danger btn-reset">Resetar</button>
                        <a href="consulta.php" class="btn btn-warning" style="color: black">Ver discentes cadastrados</a>
                    </div>
                </div>
                <div class="row">
                    <div class="alert alert-danger" style="display: none; text-align: center;">
                        <button id="btn-close-warning" type="button" class="close" title="Fechar"><span>×</span></button>
                        <strong id="warning-text"></strong>
                    </div>
                </div>
            </form>
        <?php endif; ?>
        </div> <!-- page-content-wrapper -->
    </div>

 <script src="../js/bootstrap.min.js"></script>
</body>
</html>