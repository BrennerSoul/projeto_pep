-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 29-Jan-2019 às 11:52
-- Versão do servidor: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `syspront`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_discente`
--

CREATE TABLE `tb_discente` (
  `id_discente` int(11) NOT NULL,
  `nome_discente` varchar(160) NOT NULL,
  `periodo_faculdade` char(2) NOT NULL,
  `num_matricula` int(11) NOT NULL,
  `id_ds_criptografado` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_discente`
--

INSERT INTO `tb_discente` (`id_discente`, `nome_discente`, `periodo_faculdade`, `num_matricula`, `id_ds_criptografado`) VALUES
(20, 'Brenner de Araújo', '3', 5000, 'e5cc06a22d176a92e2638419dcb5fc63');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_docente`
--

CREATE TABLE `tb_docente` (
  `id_docente` int(11) NOT NULL,
  `nome_docente` varchar(160) NOT NULL,
  `crefito` varchar(50) NOT NULL,
  `id_d_criptografado` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_docente`
--

INSERT INTO `tb_docente` (`id_docente`, `nome_docente`, `crefito`, `id_d_criptografado`) VALUES
(4, 'Violet Carmel da Silva', '2345', 'dcfd2a8d0b203a5e12484820143b135b');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_login`
--

CREATE TABLE `tb_login` (
  `id_usuario` int(11) NOT NULL,
  `nome_usuario` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `senha` varchar(32) NOT NULL,
  `tipo_usuario` varchar(15) NOT NULL,
  `id_criptografado` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_login`
--

INSERT INTO `tb_login` (`id_usuario`, `nome_usuario`, `senha`, `tipo_usuario`, `id_criptografado`) VALUES
(4, 'Admin', '632d1f81dae9f16c0721d010c21b34db', 'Administrador', 'dcfd2a8d0b203a5e12484820143b135b'),
(31, '5000', '632d1f81dae9f16c0721d010c21b34db', 'Discente', '6fb526e898c6ed522b3c778dbc519d10'),
(32, '500MG', '632d1f81dae9f16c0721d010c21b34db', 'Medico', 'dac1c57fea5176e0e8d165d5b70fbf34'),
(33, 'brenner_admin', '632d1f81dae9f16c0721d010c21b34db', 'Administrador', 'ef73be0739ae5f938bfbfb0bc1ca144b');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_medico`
--

CREATE TABLE `tb_medico` (
  `id_medico` int(11) NOT NULL,
  `nome_medico` varchar(160) NOT NULL,
  `especialidade` varchar(60) NOT NULL,
  `crm_medico` varchar(20) NOT NULL,
  `id_m_criptografado` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_medico`
--

INSERT INTO `tb_medico` (`id_medico`, `nome_medico`, `especialidade`, `crm_medico`, `id_m_criptografado`) VALUES
(1, 'Jorel da Silva Sauro', 'Cardiologia', '500MG', '681afe34439cf1a3939f032f7bab1dde'),
(6, 'Juliana Moraes', 'Fisioterapia', '200GO', '3a9fbf61f7d7d7caca947891bb0ea8f8'),
(7, 'Mithrandir da Massa', 'Ortodontia', '350DF', '8803cb530daebc650716593bdaf77b8a'),
(8, 'Ziraldo de Lamela', 'Magizoologia', '4567MA', 'c3c95e7f3e28273e85e2c71e9dc4c66a');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_paciente`
--

CREATE TABLE `tb_paciente` (
  `id_paciente` int(11) NOT NULL,
  `nome_paciente` varchar(160) NOT NULL,
  `dt_nascimento` date NOT NULL,
  `sexo` char(1) NOT NULL,
  `nome_mae` varchar(160) NOT NULL,
  `naturalidade` varchar(43) NOT NULL,
  `endereco` varchar(90) NOT NULL,
  `numero` int(11) NOT NULL,
  `complemento` varchar(40) NOT NULL,
  `bairro` varchar(60) NOT NULL,
  `cidade` varchar(40) NOT NULL,
  `estado` char(2) NOT NULL,
  `cep` varchar(9) NOT NULL,
  `telefone_1` varchar(15) NOT NULL,
  `telefone_2` varchar(15) DEFAULT NULL,
  `num_sus` varchar(20) NOT NULL,
  `id_medico` varchar(32) NOT NULL,
  `diagnostico` text NOT NULL,
  `id_criptografado` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_paciente`
--

INSERT INTO `tb_paciente` (`id_paciente`, `nome_paciente`, `dt_nascimento`, `sexo`, `nome_mae`, `naturalidade`, `endereco`, `numero`, `complemento`, `bairro`, `cidade`, `estado`, `cep`, `telefone_1`, `telefone_2`, `num_sus`, `id_medico`, `diagnostico`, `id_criptografado`) VALUES
(4, 'Brenner de Araújo Rodrigues Barbosa', '1996-12-03', 'M', 'Marinês Rodrigues Barbosa', 'Uberaba/MG', 'Rua Oswaldo Zecca', 148, '', 'Jardim Belo Horizonte', 'Uberaba', 'MG', '38082-330', '(34) 99159-7448', '(34) 3316-9630', '898003433059308', '3a9fbf61f7d7d7caca947891bb0ea8f8', 'Paciente apresentou dores de cabeça e sangramentos nasais. Após exames percebeu-se que era apenas a alta temperatura o causador desses sintomas.', 'dcfd2a8d0b203a5e12484820143b135b'),
(5, 'Nora West-Allen', '1998-01-11', 'F', 'Iris West-Allen', 'Central City/RJ', 'Rua Chile', 30, 'casa 2', 'Fabrício', 'Uberaba', 'MG', '38067-200', '(34) 99145-1237', '', '988343400884278', '681afe34439cf1a3939f032f7bab1dde', 'Paciente apresentou fraturas no braço esquerdo devido à queda de bicicleta. Pela análise primária, o paciente está com o braço quebrado.', 'd61c433dff3ac2b724454fa0baae40de'),
(6, 'Dick Grayson', '1999-10-05', 'M', 'Martha Grayson', 'Itapagipe/MG', 'Rua Chile', 30, '', 'Fabrício', 'Uberaba', 'MG', '38067-200', '(34) 99208-5534', '', '777333322221111', '681afe34439cf1a3939f032f7bab1dde', 'Paciente apresenta cansaço diariamente. Após alguns exames foi contactado uma virose simples.', '3a9fbf61f7d7d7caca947891bb0ea8f8'),
(7, 'Roberto Campos', '1971-06-17', 'M', 'Marlene Campos', 'Santo André/SP', 'Rua dos Incas', 227, '', 'Vila Valparaíso', 'Santo André', 'SP', '09060-130', '(34) 99203-3993', '', '2332382738723823', '3a9fbf61f7d7d7caca947891bb0ea8f8', 'Hemorroida.', '8803cb530daebc650716593bdaf77b8a');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_prontuario`
--

CREATE TABLE `tb_prontuario` (
  `id_prontuario` int(11) NOT NULL,
  `id_paciente` varchar(32) NOT NULL,
  `id_docente` varchar(32) NOT NULL,
  `id_discente` varchar(32) NOT NULL,
  `data_prontuario` datetime NOT NULL,
  `relatorio` text NOT NULL,
  `observacao` text,
  `id_p_criptografado` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_prontuario`
--

INSERT INTO `tb_prontuario` (`id_prontuario`, `id_paciente`, `id_docente`, `id_discente`, `data_prontuario`, `relatorio`, `observacao`, `id_p_criptografado`) VALUES
(7, 'd61c433dff3ac2b724454fa0baae40de', 'dcfd2a8d0b203a5e12484820143b135b', 'e5cc06a22d176a92e2638419dcb5fc63', '2018-12-02 17:15:06', 'dfdajhfjdshfksjdaflhklsdjhfasdkjh', 'djfkhsdjfhsdkjfh', '8803cb530daebc650716593bdaf77b8a');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_discente`
--
ALTER TABLE `tb_discente`
  ADD PRIMARY KEY (`id_discente`),
  ADD UNIQUE KEY `id_criptografado` (`id_ds_criptografado`),
  ADD UNIQUE KEY `num_matricula` (`num_matricula`);

--
-- Indexes for table `tb_docente`
--
ALTER TABLE `tb_docente`
  ADD PRIMARY KEY (`id_docente`),
  ADD UNIQUE KEY `crefito` (`crefito`),
  ADD UNIQUE KEY `id_criptografado` (`id_d_criptografado`);

--
-- Indexes for table `tb_login`
--
ALTER TABLE `tb_login`
  ADD PRIMARY KEY (`id_usuario`),
  ADD UNIQUE KEY `nome_usuario` (`nome_usuario`),
  ADD UNIQUE KEY `id_criptografado` (`id_criptografado`);

--
-- Indexes for table `tb_medico`
--
ALTER TABLE `tb_medico`
  ADD PRIMARY KEY (`id_medico`),
  ADD UNIQUE KEY `crm_medico` (`crm_medico`),
  ADD UNIQUE KEY `id_criptografado` (`id_m_criptografado`);

--
-- Indexes for table `tb_paciente`
--
ALTER TABLE `tb_paciente`
  ADD PRIMARY KEY (`id_paciente`),
  ADD UNIQUE KEY `num_sus` (`num_sus`),
  ADD UNIQUE KEY `id_criptografado` (`id_criptografado`),
  ADD KEY `medico_fk` (`id_medico`);

--
-- Indexes for table `tb_prontuario`
--
ALTER TABLE `tb_prontuario`
  ADD PRIMARY KEY (`id_prontuario`),
  ADD UNIQUE KEY `id_criptografado` (`id_p_criptografado`),
  ADD KEY `paciente_fk` (`id_paciente`),
  ADD KEY `discente_fk` (`id_discente`),
  ADD KEY `docente_fk` (`id_docente`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_discente`
--
ALTER TABLE `tb_discente`
  MODIFY `id_discente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tb_docente`
--
ALTER TABLE `tb_docente`
  MODIFY `id_docente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_login`
--
ALTER TABLE `tb_login`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `tb_medico`
--
ALTER TABLE `tb_medico`
  MODIFY `id_medico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tb_paciente`
--
ALTER TABLE `tb_paciente`
  MODIFY `id_paciente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_prontuario`
--
ALTER TABLE `tb_prontuario`
  MODIFY `id_prontuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `tb_paciente`
--
ALTER TABLE `tb_paciente`
  ADD CONSTRAINT `medico_fk` FOREIGN KEY (`id_medico`) REFERENCES `tb_medico` (`id_m_criptografado`);

--
-- Limitadores para a tabela `tb_prontuario`
--
ALTER TABLE `tb_prontuario`
  ADD CONSTRAINT `discente_fk` FOREIGN KEY (`id_discente`) REFERENCES `tb_discente` (`id_ds_criptografado`),
  ADD CONSTRAINT `docente_fk` FOREIGN KEY (`id_docente`) REFERENCES `tb_docente` (`id_d_criptografado`),
  ADD CONSTRAINT `paciente_fk` FOREIGN KEY (`id_paciente`) REFERENCES `tb_paciente` (`id_criptografado`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
