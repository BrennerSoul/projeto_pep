<?php

if(!isset($_SESSION)) {
    session_start();
}

if(isset($_POST['sessao'])):

    $_SESSION['timeout'] = isset($_SESSION['timeout']) ? $_SESSION['timeout'] : time();
    $_SESSION['session_life'] = isset($_SESSION['session_life']) ? $_SESSION['session_life'] : 0;
    $inativa = 1800;

    $_SESSION['session_life'] = time() - $_SESSION['timeout'];

    if($_SESSION['session_life'] > $inativa)
        echo 'logout';

    $_SESSION['timeout'] = time();

else:
    if(!isset($_SESSION['usuario'])) header('Location: index.php#erro=1');
    else header('Location: prontuario/cadastro.php');
endif;