<?php

    //Verifica se já existe uma sessão iniciada
    if(!isset($_SESSION)) {
        session_start();
    }

    if(isset($_POST['tag_pesquisa'])):

        require_once "../conexao_bd/conexao_db_syspront.php";
        require_once "../conexao_bd/db_syspront.class.php";

        $tag_pesquisa = isset($_POST['tag_pesquisa']) ? $_POST['tag_pesquisa'] : '';
        $select_pesquisa = isset($_POST['select_pesquisa']) ? $_POST['select_pesquisa'] : 0;
        $select_ordem = isset($_POST['select_ordem']) ? $_POST['select_ordem'] : 0;
        $check_desc = isset($_POST['check_desc']) ? $_POST['check_desc'] : false;

        $db_syspront = sysPront::getInstance(Conexao::getInstance());

        $dados = $db_syspront->search_docente($tag_pesquisa, $select_pesquisa, $select_ordem, $check_desc);

        if($dados == null):

            echo '<div>';
            echo '<h4>Nenhum resultado encontrado!</h4>';
            echo '</div>';

        else:

                echo '<thead class="table-custom">';
                    echo '<tr>';
                        echo '<th style="width: 30%" scope="col">Crefito</th>';
                        echo '<th style="width: 55%" scope="col">Nome completo</th>';
                        echo '<th style="width: 15%" scope="col">Ações</th>';
                    echo '</tr>';
                echo '</thead>';
            foreach ($dados as $registro):
                echo '<tbody>';
                    echo '<tr>';
                        echo '<td>'. $registro->crefito .'</td>';
                        echo '<td>'. $registro->nome_docente .'</td>';
                        echo '<td style="padding: 0;">';
                            //echo '<button style="margin-right: 5px; width: 30%;" class="btn btn-primary btn_detalhes" data-id_docente="'. $registro->id_criptografado .'">Detalhes</button>';
                            echo '<button style="margin-right: 5px; width: 45%;" class="btn btn-success btn_editar" data-id_docente="'. $registro->id_d_criptografado.'" title="Editar"><span class="glyphicon glyphicon-edit"></span></button>';
                            echo '<button style="width: 45%;" class="btn btn-danger btn_remover" data-toggle="modal" data-target="#modal-remover" data-id_docente="'. $registro->id_d_criptografado .'" title="Remover"><span class="glyphicon glyphicon-trash"></span></button>';
                        echo '</td>';
                    echo '</tr>';
                echo '</tbody>';
            endforeach;
        endif;

    else:
    if(!isset($_SESSION['usuario'])) header('Location: ../index.php#erro=1');
    else header('Location: consulta.php');

  endif;