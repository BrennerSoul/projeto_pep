<?php

  if(!isset($_SESSION)) {
    session_start();
  }

  if(isset($_POST['nome'])):

    require_once "../conexao_bd/conexao_db_syspront.php";
    require_once "../conexao_bd/db_syspront.class.php";

    $db_syspront = sysPront::getInstance(Conexao::getInstance());

    $id = $_POST['id'];
    $nome = $_POST['nome'];
    $crefito = $_POST['crefito'];

    $erro = $db_syspront->update_docente($nome, $crefito, $id);

    if($erro == 1062){
      header('Location: alteracao.php?value='.$id. '&erro=1062');
    } else {
      $db_syspront->gravar('../', 'Acao: atualizacao_docente; Usuario: ' . $_SESSION['usuario'] . '; ');
      header('Location: consulta.php#success=true');
    }

  else:
    if(!isset($_SESSION['usuario'])) header('Location: ../index.php#erro=1');
    else header('Location: consulta.php');

  endif;