<?php

//Verifica se há alguma sessão iniciada
if(!isset($_SESSION)) {
    session_start();
}

//Verifica se há alguém logado
if(isset($_SESSION['usuario'])){
    header('Location: prontuario/cadastro.php#');
}

//Variáveis de sessão para controlar o número máximo de tentativas de login
$_SESSION['num_tentativas'] = isset($_SESSION['num_tentativas']) ? $_SESSION['num_tentativas'] : 0;
$_SESSION['data_entrada'] = isset($_SESSION['data_entrada']) ? $_SESSION['data_entrada'] : '';
$_SESSION['primeiro_acesso'] = isset($_SESSION['primeiro_acesso']) ? $_SESSION['primeiro_acesso'] : '';
$_SESSION['ultimo_acesso'] = isset($_SESSION['ultimo_acesso']) ? $_SESSION['ultimo_acesso'] : '';
$bloqueado = false;

$_SESSION['data_entrada'] = date('Y-m-d H:i');
$data_entrada = new DateTime($_SESSION['data_entrada']);
$data_antiga = new DateTime($_SESSION['primeiro_acesso']);
$data_atual = new DateTime($_SESSION['ultimo_acesso']);
$horas = $data_entrada->diff($data_antiga);
$minutos = $data_entrada->diff($data_atual);
$hora = intval($horas->format('%h'));
$minuto = intval($minutos->format('%i'));

//Se foram 5 tentativas em menos de uma hora, bloqueia o login por 60 minutos
if($_SESSION['num_tentativas'] >= 5 AND $hora < 1):
    $bloqueado = true;
endif;

if($hora >= 1):
    $_SESSION['num_tentativas'] = 0;
    $bloqueado = false;
endif;

?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
 <meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="shortcut icon" href="img/favicon.ico">
 <title>Projeto SysPront Fisioterapia - Login</title>

 <script type="text/javascript" src="js/jquery.min.js"></script>
 <link href="css/bootstrap.min.css" rel="stylesheet">
 <link rel="stylesheet" type="text/css" href="css/sys_pront_custom.css">

 <script type="text/javascript">

    $(document).ready(function(){

        var intervalo = '';

        <?php if($bloqueado): ?>
            intervalo = setInterval(function() { window.location.replace('index.php'); }, 5000);
        <?php else: ?>
            clearInterval(intervalo);
        <?php endif; ?>

        $('#btn_ver_senha').prop('disabled', true); //Desabilita o botão(olho) de ver a senha
        $('#usuario').focus(); //Coloca o foco no input do nome de usuário

        var hash_url = $(location).attr('hash'); //Pega o hash da url

        //Verifica através do hash no path se há erro de logon ou se deu logout
        if(hash_url == '#erro=1'){
            $('#warning-text').html('Nome de usuário e/ou senha estão incorretos!');
            $('#alert').fadeIn();
        } else if(hash_url == '#logout=true'){
            $('#warning-text').html('Você saiu!');
            $('#alert').fadeIn();
        } else if(hash_url == '#timeout=true'){
            $('#warning-text').html('Você foi desconectado por inatividade!');
            $('#alert').fadeIn();
        } else {
            $('#alert').fadeOut();
        }

        //Ao acessar o input senha, verifica se o olho deve ser ativado ou nao
        $('#senha').off('input').on('input',function(){

            if($('#senha').val() == '') {
                $('#btn_ver_senha').prop('disabled', true);
                $('#btn_ver_senha').prop('title', '');
            } else {
                $('#btn_ver_senha').prop('disabled', false);
                $('#btn_ver_senha').prop('title', 'Ver senha');
            }

        });

        //Ao clicar e segurar o olho da senha, mostra a senha
        $('#btn_ver_senha').off('mousedown').mousedown(function() {
            $('#img_olhos').removeClass('glyphicon-eye-close');
            $('#img_olhos').addClass('glyphicon-eye-open');
            $('#senha').attr('type', 'text');
        });

        $('#btn_ver_senha').off('touchstart').on('touchstart' ,function() {
            $('#img_olhos').removeClass('glyphicon-eye-close');
            $('#img_olhos').addClass('glyphicon-eye-open');
            $('#senha').attr('type', 'text');
        });

        //Ao soltar o olho da senha, oculta a senha
        $('#btn_ver_senha').off('mouseup').mouseup(function() {
            $('#img_olhos').removeClass('glyphicon-eye-open');
            $('#img_olhos').addClass('glyphicon-eye-close');
            $('#senha').attr('type', 'password');
        });

        //Ao soltar o olho da senha, oculta a senha
        $('#btn_ver_senha').off('touchend').on('touchend', function() {
            $('#img_olhos').removeClass('glyphicon-eye-open');
            $('#img_olhos').addClass('glyphicon-eye-close');
            $('#senha').attr('type', 'password');
        });

        //Evento chamado quando uma tecla é pressionada
        $(document).off('keypress').keypress(function(event){
            var keycode = (event.keyCode ? event.keyCode : event.which);

            //Tecla enter
            if(keycode == '13'){
                submit_formulario_login();
            }
        });

        //Ao clicar no botão logar, faz as devidas validações
        $('#btn-logar').off('click').click(function(){

            submit_formulario_login();

        });

        //Ao clicar em reset desativa o olho da senha
        $('.btn-reset').off('click').click(function(){
            $('#alert').fadeOut();
            $('#usuario').css('border-color', '');
            $('#senha').css('border-color', '');
            $('#btn_ver_senha').prop('disabled', true);
            $('#usuario').focus();
        });

        //Faz as devidas validações e se aprovadas dá o submit no formulário
        function submit_formulario_login(){

            if($('#usuario').val() == '' && $('#senha').val() == ''){

                $('#warning-text').html('Nome de usuário e senha não podem estar vazios!');
                $('#alert').fadeIn();
                $('#usuario').css('border-color', 'red');
                $('#senha').css('border-color', 'red');
                $('#usuario').focus();

            } else if($('#usuario').val() == ''){

                $('#warning-text').html('Nome de usuário não pode estar vazio!');
                $('#alert').fadeIn();
                $('#usuario').css('border-color', 'red');
                $('#senha').css('border-color', '');
                $('#usuario').focus();

            } else if($('#senha').val() == ''){

                $('#warning-text').html('Senha não pode estar vazia!');
                $('#alert').fadeIn();
                $('#senha').css('border-color', 'red');
                $('#usuario').css('border-color', '');
                $('#senha').focus();

            } else {

                $('#alert').fadeOut();
                $('#usuario').css('border-color', '');
                $('#senha').css('border-color', '');
                $('#form-login').attr("action","validate_login.php");
                $('#form-login').submit();
            }
        }

    });

 </script>

</head>
<body>

    <!-- Barra de navegação -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div id="navbar">
                <h4>SISTEMA DE CONTROLE DE PRONTUÁRIO DE ESTAGIÁRIOS DO CURSO DE FISIOTERAPIA</h4>
            </div>
        </div>
    </nav> <!-- navbar -->

<?php if($bloqueado): ?>

    <div class="alert alert-danger" style="margin-top: 100px; text-align: center;">
        <strong>Você foi bloqueado por falha de login. Volte em <?= (60-$minuto) ?> minuto(s)!</strong>
    </div>

<?php else: ?>
    <div class="container page-content">

        <div>
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3 class="page-header">LOGIN</h3>
                        <form id="form-login" action="" method="post">
                            <div class="row" align="center">
                                <div class="col-sm-2"></div>
                                <div class="form-group col-sm-8">
                                    <label for="usuario">Nome de usuário</label>
                                    <input type="text" style="text-align: center;" class="form-control" id="usuario" name="usuario" maxlength="20">
                                </div>
                                <div class="col-sm-2"></div>
                            </div>
                            <div class="row" align="center">
                                <div class="col-sm-2"></div>
                                <div class="form-group col-sm-8">
                                    <label for="senha">Senha</label>
                                    <div class="input-group">
                                        <input type="password" style="text-align: center;" class="form-control" id="senha" name="senha" maxlength="16">
                                        <span class="input-group-btn">
                                            <button id="btn_ver_senha" class="btn btn-default" type="button">
                                                <span id="img_olhos" class="glyphicon glyphicon-eye-close"></span>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-sm-2"></div>
                            </div>
                            <hr>
                            <div id="actions" class="row">
                                <div class="col-md-12 login">
                                    <button type="button" class="btn btn-primary" id="btn-logar">Entrar</button>
                                    <button type="reset" class="btn btn-default btn-reset">Resetar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div id="alert" class="alert alert-danger" style="display: none; text-align: center;">
                    <strong id="warning-text"></strong>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
<?php endif; ?>
 <script src="js/bootstrap.min.js"></script>
</body>
</html>