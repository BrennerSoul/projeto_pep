<?php

    if(!isset($_SESSION)) {
        session_start();
    }

    if(isset($_POST['id_usuario'])):

        require_once "../conexao_bd/conexao_db_syspront.php";
        require_once "../conexao_bd/db_syspront.class.php";

        $id_usuario = isset($_POST['id_usuario']) ? $_POST['id_usuario'] : 0;
        $senha = isset($_POST['senha']) ? $_POST['senha'] : '';
        $senha_cript = md5($senha);

        $db_syspront = sysPront::getInstance(Conexao::getInstance());
        $dados = $db_syspront->validate_senha_atual($id_usuario, $senha_cript);

        if($dados == null){
            echo 'false|' . $senha;
        } else {
            echo 'true|' . $senha;
        }

    else:
        if(!isset($_SESSION['usuario'])) header('Location: ../index.php#erro=1');
        else header('Location: consulta.php');
    endif;

?>