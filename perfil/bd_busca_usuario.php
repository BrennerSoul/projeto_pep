<?php

    if(!isset($_SESSION)) {
        session_start();
    }

    if(isset($_POST['id_usuario'])):

        require_once "../conexao_bd/conexao_db_syspront.php";
        require_once "../conexao_bd/db_syspront.class.php";

        $id_usuario = isset($_POST['id_usuario']) ? $_POST['id_usuario'] : 0;

        $db_syspront = sysPront::getInstance(Conexao::getInstance());
        $dados = $db_syspront->search_usuario_by_id($id_usuario);

        echo $dados[0]->id_criptografado . '|'. $dados[0]->nome_usuario . '|' . $dados[0]->tipo_usuario;

    else:
        if(!isset($_SESSION['usuario'])) header('Location: ../index.php#erro=1');
        else header('Location: consulta.php');
    endif;

?>