<?php

  if(!isset($_SESSION)) {
    session_start();
  }

  if(isset($_POST['id_usuario'])):

    require_once "../conexao_bd/conexao_db_syspront.php";
    require_once "../conexao_bd/db_syspront.class.php";

    $db_syspront = sysPront::getInstance(Conexao::getInstance());

    $id_criptografado = $_POST['id_usuario'];

    $db_syspront->gravar('../', 'Acao: exclusao_usuario; Usuario: ' . $_SESSION['usuario'] . '; ');
    $db_syspront->delete_usuario($id_criptografado);

  else:
    if(!isset($_SESSION['usuario'])) header('Location: ../index.php#erro=1');
    else header('Location: consulta.php');

  endif;