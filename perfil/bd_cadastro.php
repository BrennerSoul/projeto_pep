<?php

  if(!isset($_SESSION)) {
    session_start();
  }

  if(isset($_POST['nome_usuario'])):

    require_once "../conexao_bd/conexao_db_syspront.php";
    require_once "../conexao_bd/db_syspront.class.php";

    $db_syspront = sysPront::getInstance(Conexao::getInstance());

    $nome_usuario = $_POST['nome_usuario'];
    $senha = $_POST['senha'];
    $senha = md5($senha);
    $tipo_usuario = $_POST['tipo_usuario'];

    $erro = $db_syspront->insert_usuario($nome_usuario, $senha, $tipo_usuario);

    if($erro == 1062){

      echo '<div class="modal fade" id="modal-erro-cadastro">';
        echo '<div class="modal-dialog">';
          echo '<div class="modal-content" style="border: 3px solid #FA5B5B;">';
            echo '<div class="modal-header" style="text-align: center;">';
              echo '<button type="button" class="close" data-dismiss="modal"><span>×</span></button>';
              echo '<h4 class="modal-title" style="color: red;">ERRO AO CADASTRAR USUÁRIO</h4>';
            echo '</div>';
            echo '<div class="modal-body" style="text-align: center; font-weight: bold;">';
              echo 'Não é possível cadastrar este usuário, porque já existe um registro com este nome de usuário! Por favor, escolha um nome de usuário disponível!';
            echo '</div>';
            echo '<div class="modal-footer">';
              echo '<button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>';
            echo '</div>';
          echo '</div>';
        echo '</div>';
      echo '</div>';

    } else {
      echo 'no_error';
      $db_syspront->gravar('../', 'Acao: cadastro_usuario; Usuario: ' . $_SESSION['usuario'] . '; ');
    }

  else:
    if(!isset($_SESSION['usuario'])) header('Location: ../index.php#erro=1');
    else header('Location: consulta.php');

  endif;