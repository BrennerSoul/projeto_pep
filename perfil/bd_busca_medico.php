<?php

  if(!isset($_SESSION)) {
    session_start();
  }

  if(isset($_POST['tag_pesquisa'])):

    require_once "../conexao_bd/conexao_db_syspront.php";
    require_once "../conexao_bd/db_syspront.class.php";

    $tag_pesquisa = isset($_POST['tag_pesquisa']) ? $_POST['tag_pesquisa'] : '';
    $select_ordem = isset($_POST['select_ordem']) ? $_POST['select_ordem'] : '';
    $check_desc = isset($_POST['check_desc']) ? $_POST['check_desc'] : '';
    $modal = isset($_POST['modal']) ? $_POST['modal'] : '';

    $db_syspront = sysPront::getInstance(Conexao::getInstance());

    $dados = $db_syspront->search_medico($tag_pesquisa, 1, $select_ordem, $check_desc);
    $dados_usuario = $db_syspront->search_usuario('', '', 3, '', $_SESSION['id_usuario']);

    $array_nome_usuario = [];

    foreach ($dados_usuario as $registro_usuario) {
        array_push($array_nome_usuario, $registro_usuario->nome_usuario);
    }

    if($dados == null):

        echo '<div>';
        echo '<h4 style="text-align: center">Nenhum resultado encontrado!</h4>';
        echo '</div>';

    else:

        echo '<div class="table-responsive">';
            echo '<table class="table table-hover table-bordered">';
                echo '<thead class="table-custom">';
                    echo '<tr>';
                        echo '<th scope="col">CRM</th>';
                        echo '<th scope="col">Nome do médico</th>';
                        echo '<th scope="col">Especialidade</th>';
                    echo '</tr>';
                echo '</thead>';
                echo '<tbody>';

                foreach ($dados as $registro):
                    if($modal == 'cadastro'):
                        if (!in_array($registro->crm_medico, $array_nome_usuario)):
                            echo '<tr class="tr_medico" style="cursor: pointer;" data-crm_medico="'.$registro->crm_medico.'">';
                                echo '<td>'.$registro->crm_medico.'</td>';
                                echo '<td>'.$registro->nome_medico.'</td>';
                                echo '<td>'.$registro->especialidade.'</td>';
                            echo '</tr>';
                        endif;
                    else:
                        if (!in_array($registro->crm_medico, $array_nome_usuario) OR $registro->crm_medico == $modal):
                            echo '<tr class="tr_medico" style="cursor: pointer;" data-crm_medico="'.$registro->crm_medico.'">';
                                echo '<td>'.$registro->crm_medico.'</td>';
                                echo '<td>'.$registro->nome_medico.'</td>';
                                echo '<td>'.$registro->especialidade.'</td>';
                            echo '</tr>';
                        endif;
                    endif;
                endforeach;

                echo '</tbody>';
            echo '</table>';
        echo '</div>';
    endif;

  else:
    if(!isset($_SESSION['usuario'])) header('Location: ../index.php#erro=1');
    else header('Location: consulta.php');

  endif;