<?php

    if(!isset($_SESSION)) {
        session_start();
    }

    if(!isset($_SESSION['usuario']) || $_SESSION['tipo_usuario'] != 'Administrador'){
        header('Location: ../index.php#erro=1');
    }

?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
 <meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="shortcut icon" href="../img/favicon.ico">
 <title>Projeto SysPront Fisioterapia - Gerenciamento de usuários</title>

 <script type="text/javascript" src="../js/jquery.min.js"></script>
 <link href="../css/bootstrap.min.css" rel="stylesheet">
 <link rel="stylesheet" type="text/css" href="../css/sys_pront_custom.css">

 <script type="text/javascript">

    $(document).ready(function(){

/*#################################################################################################*/
        //Controle do timeout
        var intervalo = '';

        contagem_timeout();
        intervalo = setTimeout(contagem_timeout, 1801000);

        function contagem_timeout(){
            $.ajax({
                url: '../sessao_timeout.php',
                method: 'post',
                data: { sessao: true },
                success: function(data){
                    if(data == 'logout'){
                        $.ajax({
                            url: '../logout.php',
                            method: 'post',
                            data: { logout: true,
                                    timeout: true },
                            success: function(data){
                                window.location.href = '../' + data;
                            }
                        });
                    }
                }
            });
        }

        $(document).off('mousedown').on('mousedown', function(){
            clearTimeout(intervalo);
            intervalo = setTimeout(contagem_timeout, 1801000);
        });

        $(document).off('mousemove').on('mousemove', function(){
            clearTimeout(intervalo);
            intervalo = setTimeout(contagem_timeout, 1801000);
        });

        $(document).off('keydown').on('keydown', function(){
            clearTimeout(intervalo);
            intervalo = setTimeout(contagem_timeout, 1801000);
        });

/*#################################################################################################*/
        //Controle da sidebar de acordo com o tamanho da tela
        //Ao redimensionar a tela, certifica-se de que a sidebar esteja na posição correta
        $(window).resize(function(){

            if($(window).width() >= 992){
                $("#sidebar-wrapper").css('width', 220);
                $("#page-content-wrapper").css('padding-left', 10);
                $("#page-content-wrapper").css('margin-left', 230);
            } else {
                $("#sidebar-wrapper").css('width', 0);
                $("#page-content-wrapper").css('padding-left', 15);
                $("#page-content-wrapper").css('margin-left', 0);
            }

        });

        //Ao clicar, mostra a sidebar
        $("#mostra-menu").click(function(e) {
            e.preventDefault();
            $("#sidebar-wrapper").css('width', 220);
            $("#page-content-wrapper").css('padding-left', 10);
            $("#page-content-wrapper").css('margin-left', 0);
        });

        //Ao clicar, fecha a sidebar
        $("#fecha-menu").click(function(e) {
            e.preventDefault();
            $("#sidebar-wrapper").css('width', 0);
            $("#page-content-wrapper").css('padding-left', 15);
            $("#page-content-wrapper").css('margin-left', 0);
        });

/*#################################################################################################*/

        $('#page-content-wrapper').fadeIn(500);
        $('#tag_pesquisa').focus();

        $('a').off('click').click(function(){
            if($(this).attr('id') == 'btn_logout'){
                $.ajax({
                    url: '../logout.php',
                    method: 'post',
                    data: { logout: true },
                    success: function(data){
                        window.location.href = '../' + data;
                    }
                });
            } else {
                event.preventDefault();
                linkLocation = this.href;
                $('#page-content-wrapper').fadeOut(200, function(){
                    window.location.href = linkLocation;
                });
            }
        });

        var modal = '';

/*--------------------------- Consulta --------------------------------*/

        busca_dados_banco();

        //Verifica se o checkbox decrescente está ativo ou não
        var check_desc = false;

        $('#check_desc').off('click').click(function(){

            if ($('#check_desc').is(":checked"))
            {
                check_desc = true;
            } else {
                check_desc = false;
            }

            busca_dados_banco();

        });

        //Ao preencher a caixa de pesquisa, busca as informações no banco de dados
        $('#tag_pesquisa').off('input').on('input',function(e){

            busca_dados_banco();

        });

        //Ao mudar o select da pesquisa, busca as informações no banco de dados
        $('#select_pesquisa').off('change').on('change',function(e){

            busca_dados_banco();

        });

        //Ao mudar o select da ordem, busca as informações no banco de dados
        $('#select_ordem').off('change').on('change',function(e){

            busca_dados_banco();

        });

        //Busca os dados de pesquisa no banco de dados
        function busca_dados_banco(){

            $.ajax({
                url: 'bd_consulta.php',
                method: 'post',
                //data: $('#form_tweet').serialize(),
                data: { tag_pesquisa: $('#tag_pesquisa').val(),
                    select_pesquisa: $('#select_pesquisa').val(),
                    select_ordem: $('#select_ordem').val(),
                    check_desc: check_desc },
                success: function(data){
                    $('#registros').html(data);
                    btns_tabela();
                }
            });
        }

        //Faz com que os botoes gerados na tabela funcionem corretamente
        function btns_tabela (){

            var id_usuario = 0;

            $('.btn_editar').off('click').click(function(){

                id_usuario = $(this).data('id_usuario');

                $.ajax({
                    url: 'bd_busca_usuario.php',
                    method: 'post',
                    data: { id_usuario: id_usuario },
                    success: function(data){

                        var new_data = data.split('|');
                        $('#id').val(new_data[0]);
                        if(new_data[2] == 'Administrador'){
                            $('#tipo_usuario').val(1);
                            $('#ed_no-user').hide();
                            $('#ed_user-comum').hide();
                            $('#ed_user-medico').hide();
                            $('.ed_comum').attr('id', 'ed_comum');
                            $('.ed_medico').attr('id', 'ed_medico');
                            $('.ed_admin').attr('id', 'nome_usuario');
                            $('.ed_admin').val(new_data[1]);
                            $('#ed_user-adm').show();
                        }
                        else if(new_data[2] == 'Discente'){
                            $('#tipo_usuario').val(2);
                            $('#ed_no-user').hide();
                            $('#ed_user-adm').hide();
                            $('#ed_user-medico').hide();
                            $('.ed_admin').attr('id', 'ed_admin');
                            $('.ed_medico').attr('id', 'ed_medico');
                            $('.ed_comum').attr('id', 'nome_usuario');
                            $('.ed_comum').val(new_data[1]);
                            $('#ed_user-comum').show();
                        }
                        else if(new_data[2] == 'Medico'){
                            $('#tipo_usuario').val(3);
                            $('#ed_no-user').hide();
                            $('#ed_user-adm').hide();
                            $('#ed_user-comum').hide();
                            $('.ed_admin').attr('id', 'ed_admin');
                            $('.ed_comum').attr('id', 'ed_comum');
                            $('.ed_medico').attr('id', 'nome_usuario');
                            $('.ed_medico').val(new_data[1]);
                            $('#ed_user-medico').show();
                        }
                    }
                });
            });

            $('.btn_remover').off('click').click(function(){

                id_usuario = $(this).data('id_usuario');

            });

            $('#remove_usuario').off('click').click(function(){

                $.ajax({
                    url: 'bd_remocao.php',
                    method: 'post',
                    data: { id_usuario: id_usuario },
                    success: function(data){
                        $('#modal-remover').modal('hide');
                        $('#modal-remocao').modal();
                        busca_dados_banco();
                    }
                });
            });
        }

/*--------------------------- Modal Cadastro --------------------------------*/

        function limpa_campos(){

            $('.no-user-input').val('');
            $('.no-user-input').css('border-color', '');
            $('.admin').css('border-color', '');
            $('.admin').val('');
            $('.comum').css('border-color', '');
            $('.comum').val('');
            $('.medico').css('border-color', '');
            $('.medico').val('');
            $('#c_tipo_usuario').val('-');
            $('#c_tipo_usuario').css('border-color', '');
            $('#c_nova_senha').val('');
            $('#c_nova_senha').css('border-color', '');
            $('#c_conf_senha').val('');
            $('#c_conf_senha').css('border-color', '');
            $('#alert_cadastro').fadeOut();
            $('#user-comum').hide();
            $('#user-adm').hide();
            $('#user-medico').hide();
            $('#no-user').show();

        }

        $("#modal-cadastrar").on('shown.bs.modal', function () {
            modal = 'cadastro';
        });

        //Limpa os campos quando a modal de cadastro é fechada
        $('#modal-cadastrar').on('hidden.bs.modal', function (e) {

            limpa_campos();
            modal = '';

        });

        //Reseta tudo
        $('#c_btn_reset').off('click').click(function(){

            limpa_campos();

        });

        //Select do tipo de usuário do modal de cadastro
        $('#c_tipo_usuario').off('change').on('change',function(e){

            if($('#c_tipo_usuario').val() == 3){
                $('#no-user').hide();
                $('#user-adm').hide();
                $('#user-comum').hide();
                $('#user-medico').show();
                $('.admin').attr('id', 'admin');
                $('.admin').val('');
                $('.comum').attr('id', 'comum');
                $('.comum').val('');
                $('.medico').attr('id', 'c_nome_usuario');

            } else
            if($('#c_tipo_usuario').val() == 2){
                $('#no-user').hide();
                $('#user-adm').hide();
                $('#user-medico').hide();
                $('#user-comum').show();
                $('.admin').attr('id', 'admin');
                $('.admin').val('');
                $('.medico').attr('id', 'medico');
                $('.medico').val('');
                $('.comum').attr('id', 'c_nome_usuario');

            } else
            if($('#c_tipo_usuario').val() == 1) {
                $('#no-user').hide();
                $('#user-comum').hide();
                $('#user-medico').hide();
                $('#user-adm').show();
                $('.comum').attr('id', 'comum');
                $('.comum').val('');
                $('.medico').attr('id', 'medico');
                $('.medico').val('');
                $('.admin').attr('id', 'c_nome_usuario');
            }

        });

        //Validação e preenchimento dos campos do modal de cadastro do usuário
        $('#cadastra_usuario').off('click').click(function(){

            var campos_vazios = false;
            var warning_text = '';

            if($('.no-user-input').val() == '' && $('.comum').val() == '' && $('.admin').val() == '' && $('.medico').val() == ''){
                warning_text += 'Campo Nome de usuário deve ser preenchido!<br>';
                $('.no-user-input').css('border-color', 'red');
                $('.comum').css('border-color', 'red');
                $('.admin').css('border-color', 'red');
                $('.medico').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('.no-user-input').css('border-color', '');
                $('.comum').css('border-color', '');
                $('.admin').css('border-color', '');
                $('.medico').css('border-color', '');
            }
            if($('#c_tipo_usuario').val() == '-'){
                warning_text += 'Campo Tipo de usuário deve ser preenchido!<br>';
                $('#c_tipo_usuario').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#c_tipo_usuario').css('border-color', '');
            }
            if($('#c_nova_senha').val() == ''){
                warning_text += 'Campo Nova senha deve ser preenchido!<br>';
                $('#c_nova_senha').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#c_nova_senha').css('border-color', '');
            }
            if($('#c_conf_senha').val() == ''){
                warning_text += 'Campo Confirmação da nova senha deve ser preenchido!<br>';
                $('#c_conf_senha').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#c_conf_senha').css('border-color', '');
            }
            if($('#c_nova_senha').val() != '' && $('#c_nova_senha').val() != $('#c_conf_senha').val()){
                warning_text += 'Campo Senha e Confirmação da nova senha devem ser iguais!<br>';
                $('#c_nova_senha').css('border-color', 'red');
                $('#c_conf_senha').css('border-color', 'red');
                campos_vazios = true;
            } else if($('#c_nova_senha').val() != '' && $('#c_nova_senha').val() == $('#c_conf_senha').val()) {
                $('#c_nova_senha').css('border-color', '');
                $('#c_conf_senha').css('border-color', '');
            }

            if(campos_vazios){
                $('#alert_cadastro').fadeIn();
                $('#c_warning-text').html(warning_text);
            } else {
                $('#alert_cadastro').fadeOut();
                $('#c_warning-text').html('');

                //Somente letras e números
                var semCaracteres = /^[A-Za-z0-9]+$/;
                //Obrigatoriedade de letras e números
                var caracteres_obrigatorios = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/;

                if($('#c_nova_senha').val().length < 8 || !caracteres_obrigatorios.test($('#c_nova_senha').val()) || !semCaracteres.test($('#c_nova_senha').val())){

                    $('#alert_cadastro').fadeIn();
                    $('#c_warning-text').html('Senha deve ter no mínimo 8 caracteres!<br>Deve conter somente letras e números!<br>E, além de letras minúsculas, no mínimo uma letra maiúscula e um número!');
                    $('#c_nova_senha').css('border-color', 'red');

                } else {

                    $('#alert_cadastro').fadeOut();
                    $('#c_warning-text').html('');
                    $('#c_nova_senha').css('border-color', '');

                    var tipo_usuario = '';

                    if($('#c_tipo_usuario').val() == 1){
                        tipo_usuario = 'Administrador';
                    } else if($('#c_tipo_usuario').val() == 2){
                        tipo_usuario = 'Discente';
                    } else if($('#c_tipo_usuario').val() == 3){
                        tipo_usuario = 'Medico';
                    }

                    $.ajax({
                        url: 'bd_cadastro.php',
                        method: 'post',
                        data: { nome_usuario: $('#c_nome_usuario').val(),
                                senha: $('#c_nova_senha').val(),
                                tipo_usuario: tipo_usuario },
                        success: function(data){

                                if(data == 'no_error'){
                                    $('#modal-cadastrar').modal('hide');
                                    $('#c_modal-sucesso').modal();
                                } else {
                                    $('#div_erro_cadastro').html(data);
                                    $('#modal-erro-cadastro').modal();
                                }
                                busca_dados_banco();
                        }
                    });
                }
            }
        });

        //Fecha os avisos
        $('#c_btn-close-warning').off('click').click(function(){

            $('#alert_cadastro').fadeOut();

        });

        //Abre o modal discente com os discente para serem escolhidos
        $(document).off('click', '#btn_buscar_discente').on('click', '#btn_buscar_discente', function(){
            $('#modal-discente').modal();
            busca_discente_no_banco();
        });

        //Abre o modal discente com os discente para serem escolhidos
        $(document).off('click', '#btn_buscar_medico').on('click', '#btn_buscar_medico', function(){
            $('#modal-medico').modal();
            busca_medico_no_banco();
        });

/*----------------------- Dentro do modal discente ----------------------------------------------*/

        $(document).off('input', '#tag_pesquisa_discente').on('input', '#tag_pesquisa_discente', function(e){

            busca_discente_no_banco();

        });

        //Ao mudar o select da ordem, busca as informações no banco de dados
        $('#select_ordem_discente').off('change').on('change',function(e){

            busca_discente_no_banco();

        });

        //Verifica se o checkbox decrescente está ativo ou não
        var check_desc_discente = false;

        $('#check_desc_discente').off('click').click(function(){

            if ($('#check_desc_discente').is(":checked"))
            {
                check_desc_discente = true;
            } else {
                check_desc_discente = false;
            }

            busca_discente_no_banco();

        });

        //Fecha modal discente resetando os dados de pesquisa
        $('#modal-discente').on('hidden.bs.modal', function (e) {
            $('#tag_pesquisa_discente').val('');
            $('#select_ordem_discente').val(2);
            $('#check_desc_discente').prop('checked', false);
        });

        //Busca discente tanto para o modal de cadastro quanto para o de edição
        function busca_discente_no_banco(){

            $.ajax({
                url: 'bd_busca_discente.php',
                method: 'post',
                data: { tag_pesquisa: $('#tag_pesquisa_discente').val(),
                        select_ordem: $('#select_ordem_discente').val(),
                        check_desc: check_desc_discente,
                        modal: modal },
                success: function(data){
                    $('#div_discente').html(data);

                    $('.tr_discente').off('click').click(function(){
                        $('#c_nome_usuario').val($(this).data('matricula'));
                        $('.ed_comum').val($(this).data('matricula'));
                        $('#modal-discente').modal('hide');
                    });
                }
            });
        }

/*----------------------- Dentro do modal médico ----------------------------------------------*/

        $(document).off('input', '#tag_pesquisa_medico').on('input', '#tag_pesquisa_medico', function(e){

            busca_medico_no_banco();

        });

        //Ao mudar o select da ordem, busca as informações no banco de dados
        $('#select_ordem_medico').off('change').on('change',function(e){

            busca_medico_no_banco();

        });

        //Verifica se o checkbox decrescente está ativo ou não
        var check_desc_medico = false;

        $('#check_desc_medico').off('click').click(function(){

            if ($('#check_desc_medico').is(":checked"))
            {
                check_desc_medico = true;
            } else {
                check_desc_medico = false;
            }

            busca_medico_no_banco();

        });

        //Fecha modal medico resetando os dados de pesquisa
        $('#modal-medico').on('hidden.bs.modal', function (e) {
            $('#tag_pesquisa_medico').val('');
            $('#select_ordem_medico').val(2);
            $('#check_desc_medico').prop('checked', false);
        });

        //Busca medico tanto para o modal de cadastro quanto para o de edição
        function busca_medico_no_banco(){

            $.ajax({
                url: 'bd_busca_medico.php',
                method: 'post',
                data: { tag_pesquisa: $('#tag_pesquisa_medico').val(),
                        select_ordem: $('#select_ordem_medico').val(),
                        check_desc: check_desc_medico,
                        modal: modal },
                success: function(data){
                    $('#div_medico').html(data);

                    $('.tr_medico').off('click').click(function(){
                        $('#c_nome_usuario').val($(this).data('crm_medico'));
                        $('.ed_medico').val($(this).data('crm_medico'));
                        $('#modal-medico').modal('hide');
                    });
                }
            });
        }

/*--------------------- Alteração dos Dados do usuário logado ------------------------------------*/

        function limpa_my_campos(){

            $('#my_id').val('');
            $('#my_nome_usuario').val('');
            $('#my_nome_usuario').css('border-color', '');
            $('#my_tipo_usuario').css('border-color', '');
            $('#my_senha_atual').val('');
            $('#my_senha_atual').css('border-color', '');
            $('#my_nova_senha').val('');
            $('#my_nova_senha').css('border-color', '');
            $('#my_conf_senha').val('');
            $('#my_conf_senha').css('border-color', '');
            $('#alert_altera_dados').fadeOut();

        }

        //Limpa os campos quando a modal é fechada
        $('#modal-alterar_dados').on('hidden.bs.modal', function (e) {

            limpa_my_campos();

        });

        //Reseta tudo
        $('#my_btn_reset').off('click').click(function(){

            limpa_my_campos();

        });

        //Validação e edição dos campos do modal de alteração do usuário logado
        $('#my_altera_usuario').off('click').click(function(){

            var campos_vazios = false;
            var warning_text = '';

            if($('#my_nome_usuario').val() == ''){
                warning_text += 'Campo Nome de usuário deve ser preenchido!<br>';
                $('#my_nome_usuario').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#my_nome_usuario').css('border-color', '');
            }
            if($('#my_tipo_usuario').val() == ''){
                warning_text += 'Campo Tipo de usuário deve ser preenchido!<br>';
                $('#my_tipo_usuario').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#my_tipo_usuario').css('border-color', '');
            }
            if($('#my_senha_atual').val() == ''){
                warning_text += 'Campo Senha atual deve ser preenchido!<br>';
                $('#my_senha_atual').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#my_senha_atual').css('border-color', '');
            }
            if($('#my_nova_senha').val() == ''){
                warning_text += 'Campo Nova senha deve ser preenchido!<br>';
                $('#my_nova_senha').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#my_nova_senha').css('border-color', '');
            }
            if($('#my_conf_senha').val() == ''){
                warning_text += 'Campo Confirmação da nova senha deve ser preenchido!<br>';
                $('#my_conf_senha').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#my_conf_senha').css('border-color', '');
            }
            if($('#my_nova_senha').val() != '' && $('#my_nova_senha').val() != $('#my_conf_senha').val()){
                warning_text += 'Campo Senha e Confirmação da nova senha devem ser iguais!<br>';
                $('#my_nova_senha').css('border-color', 'red');
                $('#my_conf_senha').css('border-color', 'red');
                campos_vazios = true;
            } else if($('#my_nova_senha').val() != '' && $('#my_nova_senha').val() == $('#my_conf_senha').val()) {
                $('#my_nova_senha').css('border-color', '');
                $('#my_conf_senha').css('border-color', '');
            }

            if(campos_vazios){
                $('#alert_altera_dados').fadeIn();
                $('#my_warning-text').html(warning_text);
            } else {
                $('#alert_altera_dados').fadeOut();
                $('#my_warning-text').html('');

                $.ajax({
                    url: 'bd_busca_senha.php',
                    method: 'post',
                    data: { id_usuario: $('#my_id').val(),
                            senha: $('#my_senha_atual').val() },
                    success: function(data){

                        var new_data = data.split('|');

                        if(new_data[0] == 'false'){
                            $('#alert_altera_dados').fadeIn();
                            $('#my_warning-text').html('Senha atual informada está incorreta!<br>');
                            $('#my_senha_atual').css('border-color', 'red');
                            $('#my_senha_atual').val('');

                        } else if(new_data[0] == 'true' && new_data[1] == $('#my_nova_senha').val()){
                            $('#alert_altera_dados').fadeIn();
                            $('#my_warning-text').html('Senha atual e nova senha devem ser diferentes!');
                            $('#my_senha_atual').css('border-color', 'red');
                            $('#my_nova_senha').css('border-color', 'red');

                        } else if(new_data[0] == 'true' && new_data[1] != $('#my_nova_senha').val()) {

                            //Somente letras e números
                            var semCaracteres = /^[A-Za-z0-9]+$/;
                            //Obrigatoriedade de letras e números
                            var caracteres_obrigatorios = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/;
                            if($('#my_nova_senha').val().length < 8 || !caracteres_obrigatorios.test($('#my_nova_senha').val()) || !semCaracteres.test($('#my_nova_senha').val())){
                                $('#alert_altera_dados').fadeIn();
                                $('#my_warning-text').html('Nova senha deve ter no mínimo 8 caracteres!<br>Deve conter somente letras e números!<br>E, além de letras minúsculas, no mínimo uma letra maiúscula e um número!');
                                $('#my_nova_senha').css('border-color', 'red');
                            } else {
                                $('#alert_altera_dados').fadeOut();
                                $('#my_warning-text').html('');
                                $('#my_senha_atual').css('border-color', '');
                                $('#my_nova_senha').css('border-color', '');

                                $.ajax({
                                    url: 'bd_alteracao.php',
                                    method: 'post',
                                    data: { id: $('#my_id').val(),
                                            nome_usuario: $('#my_nome_usuario').val(),
                                            senha: $('#my_nova_senha').val(),
                                            tipo_usuario: $('#my_tipo_usuario').val() },
                                    success: function(data){

                                        if(data == 'no_error'){
                                            $('#modal-alterar_dados').modal('hide');
                                            $('#modal-logado-sucesso').modal();
                                        } else {
                                            $('#div_erro_edicao').html(data);
                                            $('#modal-erro-edicao').modal();
                                        }
                                    }
                                });
                            }
                        }
                    }
                });
            }
        });

        //Fecha os avisos
        $('#my_btn-close-warning').off('click').click(function(){

            $('#alert_altera_dados').fadeOut();

        });

        //Ao atualizar os dados do usuário, faz logout
        $('#modal-logado-sucesso').on('hidden.bs.modal', function (e) {

            $.ajax({
                url: '../logout.php',
                method: 'post',
                data: { logout: true },
                success: function(data){
                    window.location.href = '../' + data;
                }
            });

        });

/*--------------- Modal edição --------------------------------*/

        function limpa_campos_edicao(){

            $('#alert_edicao').fadeOut();
            $('#id').val('');
            $('.ed_no-user-input').val('');
            $('.ed_no-user-input').css('border-color', '');
            $('.ed_comum').val('');
            $('.ed_comum').css('border-color', '');
            $('.ed_admin').val('');
            $('.ed_admin').css('border-color', '');
            $('.ed_medico').val('');
            $('.ed_medico').css('border-color', '');
            $('#tipo_usuario').val('-');
            $('#tipo_usuario').css('border-color', '');
            $('#senha_atual').val('');
            $('#senha_atual').css('border-color', '');
            $('#nova_senha').val('');
            $('#nova_senha').css('border-color', '');
            $('#conf_senha').val('');
            $('#conf_senha').css('border-color', '');
            $('#ed_user-comum').hide();
            $('#ed_user-adm').hide();
            $('#ed_user-medico').hide();
            $('#ed_no-user').show();

        }

        $("#modal-editar").on('shown.bs.modal', function () {
            modal = $('#nome_usuario').val();
        });

        //Limpa os campos quando a modal é fechada
        $('#modal-editar').on('hidden.bs.modal', function (e) {

            limpa_campos_edicao();
            modal = '';

        });

        $('#tipo_usuario').off('change').on('change',function(e){

            if($('#tipo_usuario').val() == 3){
                $('#ed_no-user').hide();
                $('#ed_user-adm').hide();
                $('#ed_user-comum').hide();
                $('.ed_admin').attr('id', 'ed_admin');
                $('.ed_admin').val('');
                $('.ed_comum').attr('id', 'ed_comum');
                $('.ed_comum').val('');
                $('.ed_medico').attr('id', 'nome_usuario');
                $('#ed_user-medico').show();
            } else
            if($('#tipo_usuario').val() == 2){
                $('#ed_no-user').hide();
                $('#ed_user-adm').hide();
                $('#ed_user-medico').hide();
                $('.ed_admin').attr('id', 'ed_admin');
                $('.ed_admin').val('');
                $('.ed_medico').attr('id', 'ed_medico');
                $('.ed_medico').val('');
                $('.ed_comum').attr('id', 'nome_usuario');
                $('#ed_user-comum').show();
            } else
            if($('#tipo_usuario').val() == 1) {
                $('#ed_no-user').hide();
                $('#ed_user-comum').hide();
                $('#ed_user-medico').hide();
                $('.ed_comum').attr('id', 'ed_comum');
                $('.ed_comum').val('');
                $('.ed_medico').attr('id', 'ed_medico');
                $('.ed_medico').val('');
                $('.ed_admin').attr('id', 'nome_usuario');
                $('#ed_user-adm').show();
            }

        });

        //Reseta tudo
        $('#btn_reset').off('click').click(function(){
            limpa_campos_edicao();
        });

        //Validação e edição dos campos do modal de edição do usuário
        $('#edita_usuario').off('click').click(function(){

            var campos_vazios = false;
            var warning_text = '';

            if($('.ed_no-user-input').val() == '' && $('.ed_comum').val() == '' && $('.ed_admin').val() == '' && $('.ed_medico').val() == ''){
                warning_text += 'Campo Nome de usuário deve ser preenchido!<br>';
                $('.ed_no-user-input').css('border-color', 'red');
                $('.ed_comum').css('border-color', 'red');
                $('.ed_admin').css('border-color', 'red');
                $('.ed_medico').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('.ed_no-user-input').css('border-color', '');
                $('.ed_comum').css('border-color', '');
                $('.ed_admin').css('border-color', '');
                $('.ed_medico').css('border-color', '');
            }
            if($('#tipo_usuario').val() == '-'){
                warning_text += 'Campo Tipo de usuário deve ser preenchido!<br>';
                $('#tipo_usuario').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#tipo_usuario').css('border-color', '');
            }
            if($('#senha_atual').val() == ''){
                warning_text += 'Campo Senha atual deve ser preenchido!<br>';
                $('#senha_atual').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#senha_atual').css('border-color', '');
            }
            if($('#nova_senha').val() == ''){
                warning_text += 'Campo Nova senha deve ser preenchido!<br>';
                $('#nova_senha').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#nova_senha').css('border-color', '');
            }
            if($('#conf_senha').val() == ''){
                warning_text += 'Campo Confirmação da nova senha deve ser preenchido!<br>';
                $('#conf_senha').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#conf_senha').css('border-color', '');
            }
            if($('#nova_senha').val() != '' && $('#nova_senha').val() != $('#conf_senha').val()){
                warning_text += 'Campo Senha e Confirmação da nova senha devem ser iguais!<br>';
                $('#nova_senha').css('border-color', 'red');
                $('#conf_senha').css('border-color', 'red');
                campos_vazios = true;
            } else if($('#nova_senha').val() != '' && $('#nova_senha').val() == $('#conf_senha').val()) {
                $('#nova_senha').css('border-color', '');
                $('#conf_senha').css('border-color', '');
            }

            if(campos_vazios){
                $('#alert_edicao').fadeIn();
                $('#warning-text').html(warning_text);
            } else {
                $('#alert_edicao').fadeOut();
                $('#warning-text').html('');

                $.ajax({
                    url: 'bd_busca_senha.php',
                    method: 'post',
                    data: { id_usuario: $('#id').val(),
                            senha: $('#senha_atual').val() },
                    success: function(data){

                        var new_data = data.split('|');

                        if(new_data[0] == 'false'){
                            $('#alert_edicao').fadeIn();
                            $('#warning-text').html('Senha atual informada está incorreta!<br>');
                            $('#senha_atual').css('border-color', 'red');
                            $('#senha_atual').val('');

                        } else if(new_data[0] == 'true' && new_data[1] == $('#nova_senha').val()){
                            $('#alert_edicao').fadeIn();
                            $('#warning-text').html('Senha atual e nova senha devem ser diferentes!');
                            $('#senha_atual').css('border-color', 'red');
                            $('#nova_senha').css('border-color', 'red');

                        } else if(new_data[0] == 'true' && new_data[1] != $('#nova_senha').val()) {

                            //Somente letras e números
                            var semCaracteres = /^[A-Za-z0-9]+$/;
                            //Obrigatoriedade de letras e números
                            var caracteres_obrigatorios = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/;
                            if($('#nova_senha').val().length < 8 || !caracteres_obrigatorios.test($('#nova_senha').val()) || !semCaracteres.test($('#nova_senha').val())){
                                $('#alert_edicao').fadeIn();
                                $('#warning-text').html('Nova senha deve ter no mínimo 8 caracteres!<br>Deve conter somente letras e números!<br>E, além de letras minúsculas, no mínimo uma letra maiúscula e um número!');
                                $('#nova_senha').css('border-color', 'red');
                            } else {
                                $('#alert_edicao').fadeOut();
                                $('#warning-text').html('');
                                $('#senha_atual').css('border-color', '');
                                $('#nova_senha').css('border-color', '');

                                var tipo_usuario = '';

                                if($('#tipo_usuario').val() == 1){
                                    tipo_usuario = 'Administrador';
                                }
                                else if($('#tipo_usuario').val() == 2){
                                    tipo_usuario = 'Discente';
                                }
                                else if($('#tipo_usuario').val() == 3){
                                    tipo_usuario = 'Medico';
                                }

                                $.ajax({
                                    url: 'bd_alteracao.php',
                                    method: 'post',
                                    data: { id: $('#id').val(),
                                            nome_usuario: $('#nome_usuario').val(),
                                            senha: $('#nova_senha').val(),
                                            tipo_usuario: tipo_usuario },
                                    success: function(data){
                                        if(data == 'no_error'){
                                            $('#modal-editar').modal('hide');
                                            $('#modal-sucesso').modal();
                                        } else {
                                            $('#div_erro_edicao').html(data);
                                            $('#modal-erro-edicao').modal();
                                        }
                                        busca_dados_banco();
                                    }
                                });
                            }
                        }
                    }
                });
            }

        });

        //Fecha os avisos
        $('#btn-close-warning').click(function(){

            $('#alert_edicao').fadeOut();

        });
    });

 </script>

</head>
<body>

    <!-- Barra de navegação -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button id="btn_menu" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar_menu" aria-expanded="false" aria-controls="navbar" title="Menu">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div id="navbar" style="padding-left: 5px; padding-top: 2px;">
                    <h4>SISTEMA DE CONTROLE DE PRONTUÁRIO DE ESTAGIÁRIOS DO CURSO DE FISIOTERAPIA</h4>
                </div>
            </div>
            <div id="navbar" class="navbar-collapse collapse navbar_menu" style="padding-right: 5px;">
                <ul class="nav navbar-nav navbar-right">
                    <li><h4 style="color: white; text-shadow: 2px 2px #000; margin-right: 20px; margin-top: 15px; padding-left: 5px;">Bem vindo, <?=$_SESSION['nome_do_usuario']?></h4></li>
                    <li><a href="../prontuario/cadastro.php" title="Início">
                        <span class="glyphicon glyphicon-home"></span>
                    </a></li>
                    <li><a href="../perfil/consulta.php" style="color: yellow" title="Gerenciar perfis">
                        <span class="glyphicon glyphicon-user"></span>
                    </a></li>
                    <li><a style="cursor: pointer;" id="btn_logout" title="Sair">
                        <span class="glyphicon glyphicon-log-out"></span>
                    </a></li>
                </ul>
            </div>
        </div>
    </nav> <!-- navbar -->

    <div class="container-fluid">
        <div>
            <button id="mostra-menu" type="button" title="Mostrar menu">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </button>

            <!-- Sidebar -->
            <div id="sidebar-wrapper">
                <ul class="sidebar-nav">
                    <button id="fecha-menu" title="Fechar" type="button" class="close" aria-label="Close" style="background: white; margin-right: 20px; padding: 0px 3px;">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <li class="topicos"> <strong style="font-size: 17px">CADASTROS</strong> </li>
                    <li>
                        <a href="../medico/cadastro.php"><strong>MÉDICO</strong></a>
                    </li>
                    <li>
                        <a href="../docente/cadastro.php"><strong>DOCENTE</strong></a>
                    </li>
                    <li>
                        <a href="../paciente/cadastro.php"><strong>PACIENTE</strong></a>
                    </li>
                    <li>
                        <a href="../discente/cadastro.php"><strong>DISCENTE</strong></a>
                    </li>
                    <li class="topicos"> <strong style="font-size: 17px">MOVIMENTOS</strong> </li>
                    <li>
                        <a href="../prontuario/cadastro.php"><strong>PRONTUÁRIO</strong></a>
                    </li>
                    <li class="topicos"> <strong style="font-size: 17px">CONSULTAS</strong> </li>
                    <li>
                        <a href="../prontuario/consulta.php"><strong>PRONTUÁRIO</strong></a>
                    </li>
                    <li>
                        <a href="../medico/consulta.php"><strong>MÉDICO</strong></a>
                    </li>
                    <li>
                        <a href="../docente/consulta.php"><strong>DOCENTE</strong></a>
                    </li>
                    <li>
                        <a href="../paciente/consulta.php"><strong>PACIENTE</strong></a>
                    </li>
                    <li>
                        <a href="../discente/consulta.php"><strong>DISCENTE</strong></a>
                    </li>
                </ul>
            </div> <!-- /#sidebar-wrapper -->

        </div>

        <!-- Modal cadastrado com sucesso -->
        <div class="modal fade" id="c_modal-sucesso">
            <div class="modal-dialog">
                <div class="modal-content" style="border: 3px solid black">
                    <div class="modal-header" style="text-align: center;">
                        <button type="button" class="close" data-dismiss="modal"><span style="color: red">×</span></button>
                        <h4 class="modal-title" style="color: #01C325;">Registro inserido com sucesso!</h4>
                    </div>
                </div>
            </div>
        </div> <!--Modal-->

        <!-- Modal do usuario logado alterado com sucesso -->
        <div class="modal fade" id="modal-logado-sucesso">
            <div class="modal-dialog">
                <div class="modal-content" style="border: 3px solid black">
                    <div class="modal-header" style="text-align: center;">
                        <button type="button" class="close" data-dismiss="modal"><span style="color: red">×</span></button>
                        <h4 class="modal-title" style="color: #01C325;">Dados do usuário atualizados com sucesso!</h4>
                    </div>
                </div>
            </div>
        </div> <!--Modal-->

        <!-- Modal editado com sucesso -->
        <div class="modal fade" id="modal-sucesso">
            <div class="modal-dialog">
                <div class="modal-content" style="border: 3px solid black">
                    <div class="modal-header" style="text-align: center;">
                        <button type="button" class="close" data-dismiss="modal"><span style="color: red">×</span></button>
                        <h4 class="modal-title" style="color: #01C325;">Registro atualizado com sucesso!</h4>
                    </div>
                </div>
            </div>
        </div> <!--Modal-->

        <!-- Modal removido com sucesso -->
        <div class="modal fade" id="modal-remocao">
            <div class="modal-dialog">
                <div class="modal-content" style="border: 3px solid black">
                    <div class="modal-header" style="text-align: center;">
                        <button type="button" class="close" data-dismiss="modal"><span style="color: red">×</span></button>
                        <h4 class="modal-title" style="color: #01C325;">Registro removido com sucesso!</h4>
                    </div>
                </div>
            </div>
        </div> <!--Modal-->

        <!-- Modal remover usuário -->
        <div class="modal fade" id="modal-remover">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header" style="text-align: center;">
                        <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                        <h4 class="modal-title">Remover usuário</h4>
                    </div>
                    <div class="modal-body">
                      Deseja realmente remover este usuário?
                    </div>
                    <div class="modal-footer">
                        <button id="remove_usuario" type="button" class="btn btn-primary">Sim</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Não</button>
                    </div>
                </div>
            </div>
        </div> <!--Modal-->

        <!-- Modal alterar seus dados -->
        <div class="modal fade" id="modal-alterar_dados">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header" style="text-align: center;">
                        <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                        <h4 class="modal-title">Alterar seus dados</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="my_id" value="<?= $_SESSION['id_usuario'] ?>">
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="my_nome_usuario">Nome de usuário <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="my_nome_usuario" value="<?= $_SESSION['usuario'] ?>">
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="my_tipo_usuario">Tipo de usuário <span style="color: red;">*</span></label>
                                <input disabled="disabled" type="text" class="form-control" id="my_tipo_usuario" value="<?= $_SESSION['tipo_usuario'] ?>">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <label for="my_senha_atual">Senha atual <span style="color: red;">*</span></label>
                                <input type="password" class="form-control" id="my_senha_atual">
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="my_nova_senha">Nova senha <span style="color: red;">*</span></label>
                                <input type="password" class="form-control" id="my_nova_senha">
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="my_conf_senha">Confirmação da nova senha <span style="color: red;">*</span></label>
                                <input type="password" class="form-control" id="my_conf_senha">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" style="text-align: center;">
                        <button id="my_altera_usuario" type="button" class="btn btn-primary">Salvar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button id="my_btn_reset" type="button" class="btn btn-danger">Resetar</button>
                        <hr>
                        <div class="row">
                            <div id="alert_altera_dados" class="alert alert-danger" style="display: none; text-align: center;">
                                <button id="my_btn-close-warning" type="button" class="close" title="Fechar"><span>×</span></button>
                                <strong id="my_warning-text"></strong>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!--Modal-->

        <!-- Modal editar usuário -->
        <div class="modal fade" id="modal-editar">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header" style="text-align: center;">
                        <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                        <h4 class="modal-title">Editar usuário</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="id">
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <div id="ed_no-user">
                                    <label>Nome de usuário <span style="color: red;">*</span></label>
                                    <input type="text" class="form-control ed_no-user-input" readonly="readonly" placeholder="Primeiro selecione o tipo de usuário">
                                </div>
                                <div id="ed_user-comum" style="display: none;">
                                    <label for="nome_usuario">Nome de usuário <span style="color: red;">*</span></label>
                                    <div class="input-group">
                                        <input type="text" placeholder="Matrícula do discente" class="form-control ed_comum" readonly="readonly">
                                        <span class="input-group-btn">
                                            <button class="btn btn-primary" id="btn_buscar_discente" type="button">Buscar discente</button>
                                        </span>
                                    </div>
                                </div>
                                <div id="ed_user-adm" style="display: none;">
                                    <label for="nome_usuario">Nome de usuário <span style="color: red;">*</span></label>
                                    <input type="text" class="form-control ed_admin" maxlength="20">
                                </div>
                                <div id="ed_user-medico" style="display: none;">
                                    <label for="nome_usuario">Nome de usuário <span style="color: red;">*</span></label>
                                    <div class="input-group">
                                        <input type="text" placeholder="CRM do médico" class="form-control ed_medico" readonly="readonly">
                                        <span class="input-group-btn">
                                            <button class="btn btn-primary" id="btn_buscar_medico" type="button">Buscar médico</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="tipo_usuario">Tipo de usuário <span style="color: red;">*</span></label>
                                <select class="form-control" id="tipo_usuario" name="tipo_usuario">
                                    <option value="-" style="display: none;"></option>
                                    <option value="1">Administrador</option>
                                    <option value="2">Discente</option>
                                    <option value="3">Médico</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <label for="senha_atual">Senha atual <span style="color: red;">*</span></label>
                                <input type="password" class="form-control" id="senha_atual" maxlength="15">
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="nova_senha">Nova senha <span style="color: red;">*</span></label>
                                <input type="password" class="form-control" id="nova_senha" maxlength="15">
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="conf_senha">Confirmação da nova senha <span style="color: red;">*</span></label>
                                <input type="password" class="form-control" id="conf_senha" maxlength="15">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" style="text-align: center;">
                        <button id="edita_usuario" type="button" class="btn btn-primary">Salvar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button id="btn_reset" type="button" class="btn btn-danger">Resetar</button>
                        <hr>
                        <div class="row">
                            <div id="alert_edicao" class="alert alert-danger" style="display: none; text-align: center;">
                                <button id="btn-close-warning" type="button" class="close" title="Fechar"><span>×</span></button>
                                <strong id="warning-text"></strong>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!--Modal-->

        <!-- Modal cadastrar usuário -->
        <div class="modal fade" id="modal-cadastrar">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header" style="text-align: center;">
                        <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                        <h4 class="modal-title">Cadastrar usuário</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <div id="no-user">
                                    <label>Nome de usuário <span style="color: red;">*</span></label>
                                    <input type="text" class="form-control no-user-input" readonly="readonly" placeholder="Primeiro selecione o tipo de usuário">
                                </div>
                                <div id="user-comum" style="display: none;">
                                    <label for="c_nome_usuario">Nome de usuário <span style="color: red;">*</span></label>
                                    <div class="input-group">
                                        <input type="text" placeholder="Matrícula do discente" class="form-control comum" readonly="readonly">
                                        <span class="input-group-btn">
                                            <button class="btn btn-primary" id="btn_buscar_discente" type="button">Buscar discente</button>
                                        </span>
                                    </div>
                                </div>
                                <div id="user-adm" style="display: none;">
                                    <label for="c_nome_usuario">Nome de usuário <span style="color: red;">*</span></label>
                                    <input type="text" class="form-control admin" maxlength="20">
                                </div>
                                <div id="user-medico" style="display: none;">
                                    <label for="c_nome_usuario">Nome de usuário <span style="color: red;">*</span></label>
                                    <div class="input-group">
                                        <input type="text" placeholder="CRM do médico" class="form-control medico" readonly="readonly">
                                        <span class="input-group-btn">
                                            <button class="btn btn-primary" id="btn_buscar_medico" type="button">Buscar médico</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="c_tipo_usuario">Tipo de usuário <span style="color: red;">*</span></label>
                                <select class="form-control" id="c_tipo_usuario" name="c_tipo_usuario">
                                    <option value="-" style="display: none;"></option>
                                    <option value="1">Administrador</option>
                                    <option value="2">Discente</option>
                                    <option value="3">Médico</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="c_nova_senha">Senha <span style="color: red;">*</span></label>
                                <input type="password" class="form-control" id="c_nova_senha" maxlength="15">
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="c_conf_senha">Confirmação da senha <span style="color: red;">*</span></label>
                                <input type="password" class="form-control" id="c_conf_senha" maxlength="15">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" style="text-align: center;">
                        <button id="cadastra_usuario" type="button" class="btn btn-primary">Salvar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button id="c_btn_reset" type="button" class="btn btn-danger">Resetar</button>
                        <hr>
                        <div class="row">
                            <div id="alert_cadastro" class="alert alert-danger" style="display: none; text-align: center;">
                                <button id="c_btn-close-warning" type="button" class="close" title="Fechar"><span>×</span></button>
                                <strong id="c_warning-text"></strong>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!--Modal-->

        <!-- Modal discente -->
        <div class="modal fade" id="modal-discente">
            <div class="modal-dialog modal-mensagem-class">
                <div class="modal-content">
                    <div class="modal-header" style="text-align: center;">
                        <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                        <h4 class="modal-title">Discentes disponíveis</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-6 coluna-pesquisa">
                                <input type="text" id="tag_pesquisa_discente" class="form-control" placeholder="Pesquise aqui..." maxlength="160">
                            </div>
                            <div class="col-md-4 coluna-ordem">
                                <div class="input-group ordenacao">
                                    <label class="form-control">Ordenar por:</label>
                                    <span class="input-group-btn">
                                        <select id="select_ordem_discente" class="btn btn-default">
                                            <option value="1">Matrícula</option>
                                            <option value="2" selected>Nome</option>
                                            <option value="3">Período</option>
                                        </select>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-2 checkbox">
                                <label><input id="check_desc_discente" type="checkbox" value="1"><strong>Decrescente</strong></label>
                            </div>
                        </div>
                        <hr>
                        <div id="div_discente"></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal médicos -->
        <div class="modal fade" id="modal-medico">
            <div class="modal-dialog modal-mensagem-class">
                <div class="modal-content">
                    <div class="modal-header" style="text-align: center;">
                        <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                        <h4 class="modal-title">Médicos disponíveis</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-6 coluna-pesquisa">
                                <input type="text" id="tag_pesquisa_medico" class="form-control" placeholder="Pesquise aqui..." maxlength="160">
                            </div>
                            <div class="col-md-4 coluna-ordem">
                                <div class="input-group ordenacao">
                                    <label class="form-control">Ordenar por:</label>
                                    <span class="input-group-btn">
                                        <select id="select_ordem_medico" class="btn btn-default">
                                            <option value="1">CRM</option>
                                            <option value="2" selected>Nome</option>
                                            <option value="3">Especialidade</option>
                                        </select>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-2 checkbox">
                                <label><input id="check_desc_medico" type="checkbox" value="1"><strong>Decrescente</strong></label>
                            </div>
                        </div>
                        <hr>
                        <div id="div_medico"></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal erro de cadastro -->
        <div id="div_erro_cadastro"></div>

        <!-- Modal erro de atualização -->
        <div id="div_erro_edicao"></div>

        <div id="page-content-wrapper" style="display: none;">

            <h3 class="page-header">Usuários Cadastrados
            <button style="float: right;" class="btn btn-primary btn_cadastrar btn-novo" title="Novo usuário" data-toggle="modal" data-target="#modal-cadastrar">
                <span class="glyphicon glyphicon-plus"></span>
            </button>
            <button style="float: right; margin-right: 5px;" class="btn btn-success btn-novo" title="Alterar seus dados" data-toggle="modal" data-target="#modal-alterar_dados">
                <span class="glyphicon glyphicon-pencil"></span>
            </button></h3>

            <form method="get" class="form-group row">
                <div class="col-lg-6 coluna-pesquisa">
                    <div class="input-group">
                        <input type="text" id="tag_pesquisa" name="tag_pesquisa" class="form-control" placeholder="Pesquise aqui..." maxlength="80">
                        <span class="input-group-btn">
                            <select id="select_pesquisa" class="btn btn-default">
                              <option value="1" selected>Geral</option>
                              <option value="2">Nome de usuário</option>
                              <option value="3">Tipo de usuário</option>
                            </select>
                        </span>
                    </div>
                </div>
                <div class="col-md-4 coluna-ordem">
                    <div class="input-group ordenacao">
                        <label class="form-control">Ordenar por: </label>
                        <span class="input-group-btn">
                            <select id="select_ordem" class="btn btn-default">
                                <option value="1" selected>Nome de usuário</option>
                                <option value="2">Tipo de usuário</option>
                            </select>
                        </span>
                    </div>
                </div>
                <div class="col-md-2 checkbox">
                    <label><input id="check_desc" type="checkbox" value="1"><strong>Decrescente</strong></label>
                </div>
            </form>
            <br>

            <div class="table-responsive">
                <table id="registros" class="table table-hover table-bordered">
                </table>
            </div>

        </div> <!-- page-content-wrapper -->
    </div>

 <script src="../js/bootstrap.min.js"></script>
</body>
</html>