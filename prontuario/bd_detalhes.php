<?php

  if(!isset($_SESSION)) {
    session_start();
  }

  if(isset($_POST['id_prontuario'])):

    require_once "../conexao_bd/conexao_db_syspront.php";
    require_once "../conexao_bd/db_syspront.class.php";

    $db_syspront = sysPront::getInstance(Conexao::getInstance());

    $id_prontuario = $_POST['id_prontuario'];

    $dados = $db_syspront->search_prontuario_by_id($id_prontuario);

    $dt_nascimento = new DateTime($dados[0]->dt_nascimento);
    $data_formatada = $dt_nascimento->format('d/m/Y');

    $dt_prontuario = new DateTime($dados[0]->data_prontuario);
    $data_f_prontuario = $dt_prontuario->format('d/m/Y H:i');

    $endereco_completo = '';
        if(empty($dados[0]->complemento))
            $endereco_completo = $dados[0]->endereco . ', ' . $dados[0]->numero . ' - ' . $dados[0]->bairro . ', ' . $dados[0]->cidade . ' - ' . $dados[0]->estado . ', ' . $dados[0]->cep;
        else
            $endereco_completo = $dados[0]->endereco . ', ' . $dados[0]->numero . ', ' . $dados[0]->complemento . ' - ' . $dados[0]->bairro . ', ' . $dados[0]->cidade . ' - ' . $dados[0]->estado . ', ' . $dados[0]->cep;

    if(empty($dados[0]->telefone_2))
        $telefones = $dados[0]->telefone_1;
    else
        $telefones = $dados[0]->telefone_1 . ' | ' . $dados[0]->telefone_2;

    $sexo = '';
    if($dados[0]->sexo == 'M') $sexo = 'Masculino';
    else if($dados[0]->sexo == 'F') $sexo = 'Feminino';

    //Modal detalhes
    echo '<div class="modal fade" id="modal-prontuario">';
        echo '<div class="modal-dialog modal-mensagem-class">';
            echo '<div class="modal-content">';
                echo '<div class="modal-header" style="text-align: center;">';
                    echo '<button type="button" class="close" data-dismiss="modal"><span>×</span></button>';
                    echo '<h3 class="modal-title">Dados completos do prontuário</h3>';
                echo '</div>';
                echo '<div class="modal-body">';
                    echo '<div class="table-responsive">';
                        echo '<table class="table table-hover table-bordered">';
                            echo '<thead class="table-custom">';
                                echo '<tr>';
                                    echo '<th style="border: 2px solid black;" scope="col">Nome do paciente</th>';
                                    echo '<th style="border: 2px solid black;" scope="col">Data de nascimento</th>';
                                    echo '<th style="border: 2px solid black;" scope="col">Sexo</th>';
                                echo '</tr>';
                            echo '</thead>';
                            echo '<tbody style="font-weight: bold;">';
                                echo '<tr>';
                                    echo '<td style="border: 2px solid black;">'.$dados[0]->nome_paciente.'</td>';
                                    echo '<td style="border: 2px solid black;">'.$data_formatada.'</td>';
                                    echo '<td style="border: 2px solid black;">'.$sexo.'</td>';
                                echo '</tr>';
                            echo '</tbody>';
                            echo '<thead class="table-custom">';
                                echo '<tr>';
                                    echo '<th style="border: 2px solid black;" scope="col">Nome da mãe</th>';
                                    echo '<th style="border: 2px solid black;" colspan="2" scope="col">Naturalidade</th>';
                                echo '</tr>';
                            echo '</thead>';
                            echo '<tbody style="font-weight: bold;">';
                                echo '<tr>';
                                    echo '<td style="border: 2px solid black;">'.$dados[0]->nome_mae.'</td>';
                                    echo '<td style="border: 2px solid black;" colspan="2">'.$dados[0]->naturalidade.'</td>';
                                echo '</tr>';
                            echo '</tbody>';
                            echo '<thead class="table-custom">';
                                echo '<tr>';
                                    echo '<th style="border: 2px solid black;" scope="col">Endereço completo</th>';
                                    echo '<th style="border: 2px solid black;" colspan="2" scope="col">Telefone(s)</th>';
                                echo '</tr>';
                            echo '</thead>';
                            echo '<tbody style="font-weight: bold;">';
                                echo '<tr>';
                                    echo '<td style="border: 2px solid black;">'.$endereco_completo.'</td>';
                                    echo '<td style="border: 2px solid black;" colspan="2">'.$telefones.'</td>';
                                echo '</tr>';
                            echo '</tbody>';
                        echo '<table class="table table-hover table-bordered">';
                            echo '<thead class="table-custom">';
                                echo '<tr>';
                                    echo '<th style="border: 2px solid black;" scope="col">Nome do médico</th>';
                                    echo '<th style="border: 2px solid black;" scope="col">Especialidade</th>';
                                    echo '<th style="border: 2px solid black;" scope="col">Número do SUS</th>';
                                echo '</tr>';
                            echo '</thead>';
                            echo '<tbody style="font-weight: bold;">';
                                echo '<tr>';
                                    echo '<td style="border: 2px solid black;">'.$dados[0]->nome_medico.'</td>';
                                    echo '<td style="border: 2px solid black;">'.$dados[0]->especialidade.'</td>';
                                    echo '<td style="border: 2px solid black;">'.$dados[0]->num_sus.'</td>';
                                echo '</tr>';
                            echo '</tbody>';
                            echo '<thead class="table-custom">';
                                echo '<tr>';
                                    echo '<th style="border: 2px solid black;" colspan="3" scope="col">Diagnóstico</th>';
                                echo '</tr>';
                            echo '</thead>';
                            echo '<tbody style="font-weight: bold;">';
                                echo '<tr>';
                                    echo '<td style="border: 2px solid black;" colspan="3">'.$dados[0]->diagnostico.'</td>';
                                echo '</tr>';
                            echo '</tbody>';
                        echo '</table>';
                        echo '<table class="table table-hover table-bordered">';
                            echo '<thead class="table-custom">';
                                echo '<tr>';
                                    echo '<th style="border: 2px solid black;" colspan="3" scope="col">Relatório</th>';
                                echo '</tr>';
                            echo '</thead>';
                            echo '<tbody style="font-weight: bold;">';
                                echo '<tr>';
                                    echo '<td style="border: 2px solid black;" colspan="3">'.$dados[0]->relatorio.'</td>';
                                echo '</tr>';
                            echo '</tbody>';
                            echo '<thead class="table-custom">';
                                echo '<tr>';
                                    echo '<th style="border: 2px solid black;" colspan="2" scope="col">Observações</th>';
                                    echo '<th style="border: 2px solid black;" scope="col">Data da última modificação</th>';
                                echo '</tr>';
                            echo '</thead>';
                            echo '<tbody style="font-weight: bold;">';
                                echo '<tr>';
                                    echo '<td style="border: 2px solid black;" colspan="2">'.$dados[0]->observacao.'</td>';
                                    echo '<td style="border: 2px solid black;">'.$data_f_prontuario.'</td>';
                                echo '</tr>';
                            echo '</tbody>';
                        echo '</table>';
                    echo '</div>';
                echo '</div>';
            echo '</div>';
        echo '</div>';
    echo '</div>';

  else:
    if(!isset($_SESSION['usuario'])) header('Location: ../index.php#erro=1');
    else header('Location: cadastro.php');

  endif;