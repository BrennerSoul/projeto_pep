<?php

    //Verifica se já existe uma sessão iniciada
    if(!isset($_SESSION)) {
        session_start();
    }

    if(isset($_POST['tag_pesquisa'])):

        require_once "../conexao_bd/conexao_db_syspront.php";
        require_once "../conexao_bd/db_syspront.class.php";

        $tag_pesquisa = isset($_POST['tag_pesquisa']) ? $_POST['tag_pesquisa'] : '';
        $select_pesquisa = isset($_POST['select_pesquisa']) ? $_POST['select_pesquisa'] : 0;
        $select_ordem = isset($_POST['select_ordem']) ? $_POST['select_ordem'] : 0;
        $check_desc = isset($_POST['check_desc']) ? $_POST['check_desc'] : false;
        $id_paciente = isset($_POST['id_paciente']) ? $_POST['id_paciente'] : 0;

        $db_syspront = sysPront::getInstance(Conexao::getInstance());

        $dados = $db_syspront->search_prontuario($tag_pesquisa, $select_pesquisa, $select_ordem, $check_desc, $id_paciente);

        if($dados == null):

            echo '<div>';
            echo '<h4>Nenhum resultado encontrado!</h4>';
            echo '</div>';

        else:

                echo '<thead class="table-custom">';
                    echo '<tr>';
                        echo '<th style="width: 5%" scope="col"></th>';
                        echo '<th style="width: 25%" scope="col">Docente</th>';
                        echo '<th style="width: 25%" scope="col">Discente</th>';
                        echo '<th style="width: 15%" scope="col">Data</th>';
                        echo '<th style="width: 30%" scope="col">Relatório</th>';
                        //echo '<th style="width: 30%" scope="col">Ações</th>';
                    echo '</tr>';
                echo '</thead>';
            foreach ($dados as $registro):
                $data_prontuario = new DateTime($registro->data_prontuario);
                $data_formatada = $data_prontuario->format('d/m/Y H:i');
                echo '<tbody>';
                    echo '<tr style="cursor: pointer;" id="false">';
                        echo '<td><input class="check_prontuario" id="'.$registro->id_p_criptografado.'" type="checkbox" value="1"></td>';
                        echo '<td>'. $registro->nome_docente .'</td>';
                        echo '<td>'. $registro->nome_discente .'</td>';
                        echo '<td>'. $data_formatada .'</td>';
                        echo '<td>'. substr($registro->relatorio, 0, 15) . '...' .'</td>';
                        /*echo '<td style="padding: 0;">';
                            echo '<button style="margin-right: 5px; width: 30%;" class="btn btn-primary btn_detalhes" data-id_medico="'. $registro->id_medico .'">Detalhes</button>';
                            echo '<button style="margin-right: 5px; width: 30%;" class="btn btn-success btn_editar" data-id_medico="'. $registro->id_medico.'">Editar</button>';
                            echo '<button style="width: 30%;" class="btn btn-danger btn_remover" data-toggle="modal" data-target="#modal-remover" data-id_medico="'. $registro->id_medico .'">Remover</button>';
                        echo '</td>';*/
                    echo '</tr>';
                echo '</tbody>';
            endforeach;
        endif;

    else:
    if(!isset($_SESSION['usuario'])) header('Location: ../index.php#erro=1');
    else header('Location: cadastro.php');

  endif;
