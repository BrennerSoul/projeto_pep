<?php

if(!isset($_SESSION)) {
    session_start();
}

if(isset($_POST['usuario'])):

    require_once "../conexao_bd/conexao_db_syspront.php";
    require_once "../conexao_bd/db_syspront.class.php";

    $usuario = $_POST['usuario'];
    $senha = md5($_POST['senha']);

    $db_syspront = sysPront::getInstance(Conexao::getInstance());

    $dados = $db_syspront->validate_login($usuario, $senha);

    if($dados == null):
        echo 'dados_nulos';
    else:

        if($dados[0]->tipo_usuario == 'Administrador'):
            echo 'dados_admin';
        else:
            echo 'dados_comum';
        endif;

    endif;

else:
    if(!isset($_SESSION['usuario'])) header('Location: index.php#erro=1');
    else header('Location: cadastro.php');
endif;