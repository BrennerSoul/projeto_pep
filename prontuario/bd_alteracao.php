<?php

  if(!isset($_SESSION)) {
    session_start();
  }

  if(isset($_POST['id'])):

    require_once "../conexao_bd/conexao_db_syspront.php";
    require_once "../conexao_bd/db_syspront.class.php";

    $db_syspront = sysPront::getInstance(Conexao::getInstance());

    $id = $_POST['id'];
    $id_paciente = $_POST['id_paciente'];
    $id_docente = $_POST['id_docente'];
    $id_discente = $_POST['id_discente'];
    $data = $_POST['data'];
    $relatorio = $_POST['relatorio'];
    $observacao = $_POST['observacao'];

    $data_prontuario = new DateTime(str_replace('/', '-', $data));
    $data_formatada = $data_prontuario->format('Y-m-d H:i:s');

    $db_syspront->gravar('../', 'Acao: atualizacao_prontuario; Usuario: ' . $_SESSION['usuario'] . '; ');
    $db_syspront->update_prontuario($id_paciente, $id_docente, $id_discente, $data_formatada, $relatorio, $observacao, $id);

  else:
    if(!isset($_SESSION['usuario'])) header('Location: ../index.php#erro=1');
    else header('Location: cadastro.php');

  endif;