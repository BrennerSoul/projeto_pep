<?php

  if(!isset($_SESSION)) {
    session_start();
  }

  if(isset($_POST['tag_pesquisa'])):

    require_once "../conexao_bd/conexao_db_syspront.php";
    require_once "../conexao_bd/db_syspront.class.php";

    $tag_pesquisa = isset($_POST['tag_pesquisa']) ? $_POST['tag_pesquisa'] : '';
    $select_ordem = isset($_POST['select_ordem']) ? $_POST['select_ordem'] : '';
    $check_desc = isset($_POST['check_desc']) ? $_POST['check_desc'] : '';

    $db_syspront = sysPront::getInstance(Conexao::getInstance());

    $dados = $db_syspront->search_docente($tag_pesquisa, 1, $select_ordem, $check_desc);

    if($dados == null):

        echo '<div>';
        echo '<h4 style="text-align: center">Nenhum resultado encontrado!</h4>';
        echo '</div>';

    else:

        echo '<div class="table-responsive">';
            echo '<table class="table table-hover table-bordered">';
                echo '<thead class="table-custom">';
                    echo '<tr>';
                        echo '<th scope="col">Crefito</th>';
                        echo '<th scope="col">Nome do docente</th>';
                    echo '</tr>';
                echo '</thead>';
                echo '<tbody>';
                foreach ($dados as $registro):
                    echo '<tr id="'.$registro->id_d_criptografado.'" class="tr_docente" style="cursor: pointer;" data-nome_docente="'.$registro->nome_docente.'">';
                        echo '<td>'.$registro->crefito.'</td>';
                        echo '<td>'.$registro->nome_docente.'</td>';
                    echo '</tr>';
                endforeach;
                echo '</tbody>';
            echo '</table>';
        echo '</div>';
    endif;

  else:
    if(!isset($_SESSION['usuario'])) header('Location: ../index.php#erro=1');
    else header('Location: cadastro.php');

  endif;