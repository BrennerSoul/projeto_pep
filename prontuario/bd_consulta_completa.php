<?php

    //Verifica se já existe uma sessão iniciada
    if(!isset($_SESSION)) {
        session_start();
    }

    if(isset($_POST['tag_pesquisa'])):

        require_once "../conexao_bd/conexao_db_syspront.php";
        require_once "../conexao_bd/db_syspront.class.php";

        $tag_pesquisa = isset($_POST['tag_pesquisa']) ? $_POST['tag_pesquisa'] : '';
        $select_pesquisa = isset($_POST['select_pesquisa']) ? $_POST['select_pesquisa'] : 0;
        $select_ordem = isset($_POST['select_ordem']) ? $_POST['select_ordem'] : 0;
        $check_desc = isset($_POST['check_desc']) ? $_POST['check_desc'] : false;

        $db_syspront = sysPront::getInstance(Conexao::getInstance());

        $dados = $db_syspront->search_prontuario_completo($tag_pesquisa, $select_pesquisa, $select_ordem, $check_desc);

        if($dados == null):

            echo '<div>';
            echo '<h4>Nenhum resultado encontrado!</h4>';
            echo '</div>';

        else:

                echo '<thead class="table-custom">';
                    echo '<tr>';
                        echo '<th scope="col">Paciente</th>';
                        echo '<th scope="col">Médico</th>';
                        echo '<th scope="col">Docente</th>';
                        echo '<th scope="col">Discente</th>';
                        echo '<th scope="col">Data</th>';
                        echo '<th scope="col"></th>';
                    echo '</tr>';
                echo '</thead>';
            foreach ($dados as $registro):
                $data_prontuario = new DateTime($registro->data_prontuario);
                $data_formatada = $data_prontuario->format('d/m/Y H:i');
                echo '<tbody>';
                    echo '<tr>';
                        echo '<td>'. $registro->nome_paciente .'</td>';
                        echo '<td>'. $registro->nome_medico .'</td>';
                        echo '<td>'. $registro->nome_docente .'</td>';
                        echo '<td>'. $registro->nome_discente .'</td>';
                        echo '<td>'. $data_formatada .'</td>';
                        echo '<td style="padding: 0;"><button style="width: 90%" id="'.$registro->id_p_criptografado.'" type="button" class="btn btn-info btn_detalhes" title="Detalhes"><span class="glyphicon glyphicon-th-list"></span></button></td>';
                    echo '</tr>';
                echo '</tbody>';
            endforeach;
        endif;

    else:
    if(!isset($_SESSION['usuario'])) header('Location: ../index.php#erro=1');
    else header('Location: cadastro.php');

  endif;
