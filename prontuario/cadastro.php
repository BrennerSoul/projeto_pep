<!-- Gerenciamento dos prontuários por paciente -->
<?php

    //Se não houver sessão iniciada, cria sessão
    if(!isset($_SESSION)) {
        session_start();
    }

    //Se não houver ninguém logado, redireciona para o login com erro
    if(!isset($_SESSION['usuario'])){
        header('Location: ../index.php#erro=1');
    }

    if($_SESSION['tipo_usuario'] == 'Medico')
        header('Location: consulta.php');

?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
 <meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="shortcut icon" href="../img/favicon.ico">
 <title>Projeto SysPront Fisioterapia - Gerenciamento de prontuários</title>

 <script type="text/javascript" src="../js/jquery.min.js"></script>
 <script type="text/javascript" src="../js/bootstrap-datetimepicker.js"></script>
 <link rel="stylesheet" type="text/css" media="screen" href="../css/datetimepicker.css">
 <link href="../css/bootstrap.min.css" rel="stylesheet">
 <link rel="stylesheet" type="text/css" href="../css/sys_pront_custom.css">

 <script type="text/javascript">

    $(document).ready(function(){

/*#################################################################################################*/
        //Controle do timeout
        var intervalo = '';

        contagem_timeout();
        intervalo = setTimeout(contagem_timeout, 1801000);

        function contagem_timeout(){
            $.ajax({
                url: '../sessao_timeout.php',
                method: 'post',
                data: { sessao: true },
                success: function(data){
                    if(data == 'logout'){
                        $.ajax({
                            url: '../logout.php',
                            method: 'post',
                            data: { logout: true,
                                    timeout: true },
                            success: function(data){
                                window.location.href = '../' + data;
                            }
                        });
                    }
                }
            });
        }

        $(document).off('mousedown').on('mousedown', function(){
            clearTimeout(intervalo);
            intervalo = setTimeout(contagem_timeout, 1801000);
        });

        $(document).off('mousemove').on('mousemove', function(){
            clearTimeout(intervalo);
            intervalo = setTimeout(contagem_timeout, 1801000);
        });

        $(document).off('keydown').on('keydown', function(){
            clearTimeout(intervalo);
            intervalo = setTimeout(contagem_timeout, 1801000);
        });

/*#################################################################################################*/
        //Controle da sidebar de acordo com o tamanho da tela
        //Ao redimensionar a tela, certifica-se de que a sidebar esteja na posição correta
        $(window).resize(function(){

            if($(window).width() >= 992){
                $("#sidebar-wrapper").css('width', 220);
                $("#page-content-wrapper").css('padding-left', 10);
                $("#page-content-wrapper").css('margin-left', 230);
            } else {
                $("#sidebar-wrapper").css('width', 0);
                $("#page-content-wrapper").css('padding-left', 15);
                $("#page-content-wrapper").css('margin-left', 0);
            }

        });

        //Ao clicar, mostra a sidebar
        $("#mostra-menu").click(function(e) {
            e.preventDefault();
            $("#sidebar-wrapper").css('width', 220);
            $("#page-content-wrapper").css('padding-left', 10);
            $("#page-content-wrapper").css('margin-left', 0);
        });

        //Ao clicar, fecha a sidebar
        $("#fecha-menu").click(function(e) {
            e.preventDefault();
            $("#sidebar-wrapper").css('width', 0);
            $("#page-content-wrapper").css('padding-left', 15);
            $("#page-content-wrapper").css('margin-left', 0);
        });

/*#################################################################################################*/

        $('#page-content-wrapper').fadeIn(500);

        //Controle e opções do datetimepicker
        $('.form_datetime').datetimepicker({
            format: 'dd/mm/yyyy hh:ii',
            startDate: '-150y',
            endDate: '+5m',
            weekStart: 1,
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0,
            showMeridian: 0
        });

        //Inicializa oculto o formulário de cadastro dos dados
        $('#cria_pront').hide();

        $('#check_desc').prop('checked', true);

        //Pega o hash da url e divide em hash success e id do paciente
        var hash_url = $(location).attr('hash');
        var new_hash = hash_url.substr(0, 13);
        var id_paciente = hash_url.substr(17);

        //Pega o parametro da url e divide em nome e valor
        var sPageURL = window.location.search.substring(1);
        var sParameterName = sPageURL.split('=');

        //Se o hash for de cadastro com sucesso, exibe o modal e traz os novos dados
        if(new_hash == '#success=true'){

            $('#modal-sucesso').modal();
            busca_paciente_pelo_id();

        } else if(new_hash == '#succeed=true'){
        //Se o hash for de edicao com sucesso, exibe o modal e traz os novos dados
            $('#modal-atualizacao').modal();
            busca_paciente_pelo_id();

        } else if(sParameterName[0] == 'value' && sParameterName[1] != ''){
            //Se o parametro for id atribui o id e chama o paciente
            id_paciente = sParameterName[1];
            busca_paciente_pelo_id();

        } else {
            //Inicia a página com o modal de escolha do paciente
            $('#modal-paciente').modal();
            busca_paciente_no_banco();
        }

        $('a').off('click').click(function(){
            if($(this).attr('id') == 'btn_logout'){
                $.ajax({
                    url: '../logout.php',
                    method: 'post',
                    data: { logout: true },
                    success: function(data){
                        window.location.href = '../' + data;
                    }
                });
            } else {
                event.preventDefault();
                linkLocation = this.href;
                $('#page-content-wrapper').fadeOut(200, function(){
                    window.location.href = linkLocation;
                });
            }
        });

        //Adiciona zero à esquerda nas horas e minutos
        function addZero(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }

        //Atualiza data e hora no input
        $('.reload_date_time').off('click').click(function(){

            var d = new Date();
            var strDate = d.getDate() + "/" + (d.getMonth()+1) + "/" + d.getFullYear() + " " + addZero(d.getHours()) + ":" + addZero(d.getMinutes());
            $('#data_prontuario').val(strDate);

        });

        //Ao clicar em novo prontuário, traz inputs para cadastro
        $('#btn_novo').off('click').click(function(){
            $('#registros').fadeOut(700);
            $('.botoes').fadeOut(700);
            $('.hr-custom').fadeOut(700, function(){
                $('#cria_pront').fadeIn(500);
                var d = new Date();
                var strDate = d.getDate() + "/" + (d.getMonth()+1) + "/" + d.getFullYear() + " " + addZero(d.getHours()) + ":" + addZero(d.getMinutes());
                $('#data_prontuario').val(strDate);
            });
        });

        //Ao clicar em cancelar, volta página ao normal
        $('#btn_cancelar').off('click').click(function(){
            $('#data_prontuario').val('');
            $('#nome_docente').val('');
            $('#nome_discente').val('');
            $('#relatorio').val('');
            $('#observacao').val('');

            $('#id_paciente').val('');
            $('#nome_paciente').css('border-color', '');
            $('#nome_medico').css('border-color', '');
            $('#nome_docente').css('border-color', '');
            $('#nome_discente').css('border-color', '');
            $('#relatorio').css('border-color', '');
            $('#observacao').css('border-color', '');
            $('#data_prontuario').css('border-color', '');

            $('#cria_pront').fadeOut(700, function(){
                $('.hr-custom').fadeIn(500);
                $('#registros').fadeIn(500);
                $('.botoes').fadeIn(500);
                $('.alert').fadeOut();
                $('#modal-paciente').modal();
            });

            busca_prontuario_no_banco();
        });

        //Validações dos campos de cadastro
        $('#btn-logar').off('click').click(function(){

            var campos_vazios = false;
            var warning_text = '';

            if($('#nome_paciente').val() == ''){
                warning_text += 'Campo Nome do paciente deve ser preenchido!<br>';
                $('#nome_paciente').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#nome_paciente').css('border-color', '');
            }

            if($('#nome_medico').val() == ''){
                warning_text += 'Campo Nome do médico deve ser preenchido!<br>';
                $('#nome_medico').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#nome_medico').css('border-color', '');
            }

            if($('#nome_docente').val() == ''){
                warning_text += 'Campo Nome do docente deve ser preenchido!<br>';
                $('#nome_docente').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#nome_docente').css('border-color', '');
            }

            if($('#nome_discente').val() == ''){
                warning_text += 'Campo Nome do discente deve ser preenchido!<br>';
                $('#nome_discente').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#nome_discente').css('border-color', '');
            }

            if($('#relatorio').val() == ''){
                warning_text += 'Campo Relatório deve ser preenchido!<br>';
                $('#relatorio').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#relatorio').css('border-color', '');
            }

            if($('#data_prontuario').val() == ''){
                warning_text += 'Campo Data deve ser preenchido!<br>';
                $('#data_prontuario').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#data_prontuario').css('border-color', '');
            }

            if(campos_vazios){
                $('.alert').fadeIn();
                $('#warning-text').html(warning_text);
                $('html, body').animate({
                    scrollTop: ($('#warning-text').offset().top)
                },1000);
            } else {
                $('.alert').fadeOut();
                $('#form-cadastro').attr("action","bd_cadastro.php");
                $('#page-content-wrapper').fadeOut(200, function(){
                    $("#form-cadastro").submit();
                });
            }

        });

        //Fecha os avisos
        $('#btn-close-warning').off('click').click(function(){

            $('html, body').animate({
                scrollTop: 31,
            }, 700, function(){
                $('.alert').fadeOut();
            });

        });

        $('.btn-reset').off('click').click(function(){

            $('html, body').animate({
                scrollTop: 31,
            }, 700, function(){
                $('.alert').fadeOut();
                $('#modal-paciente').modal();
            });

            $('#id_paciente').val('');
            $('#nome_paciente').css('border-color', '');
            $('#nome_medico').css('border-color', '');
            $('#nome_docente').css('border-color', '');
            $('#nome_discente').css('border-color', '');
            $('#relatorio').css('border-color', '');
            $('#observacao').css('border-color', '');
            $('#data_prontuario').css('border-color', '');

        });

        //Traz lista de pacientes para escolha
        $('#btn_buscar_paciente').off('click').click(function(){

            $('#modal-paciente').modal();
            busca_paciente_no_banco();

        });

        //Ao preencher a caixa de pesquisa, busca as informações no banco de dados
        $('#tag_pesquisa_paciente').off('input').on('input',function(e){

            busca_paciente_no_banco();

        });

        //Ao mudar o select da ordem, busca as informações no banco de dados
        $('#select_ordem_paciente').off('change').on('change',function(e){

            busca_paciente_no_banco();

        });

        //Verifica se o checkbox decrescente está ativo ou não
        var check_desc_paciente = false;

        $('#check_desc_paciente').off('click').click(function(){

            if ($('#check_desc_paciente').is(":checked"))
            {
                check_desc_paciente = true;
            } else {
                check_desc_paciente = false;
            }

            busca_paciente_no_banco();

        });

        //Limpa os campos de pesquisa e ordenacao quando a modal é fechada
        $('#modal-paciente').on('hidden.bs.modal', function (e) {
            $('#tag_pesquisa_paciente').val('');
            $('#select_ordem_paciente').val(1);
            $('#check_desc_paciente').prop('checked', false);
        });

        var id_prontuario = 0;

        $('#btn_editar').off('click').click(function(){

            <?php if($_SESSION['tipo_usuario'] == 'Administrador'): ?>
                window.location.replace('alteracao.php?value='+id_prontuario);
            <?php else: ?>
                $('#modal-admin').modal();
                $('#acao').val('editar');
            <?php endif; ?>
        });

        //Função que mostra os detalhes do prontuário
        function detalhes(){

            $.ajax({
                url: 'bd_detalhes.php',
                method: 'post',
                data: { id_prontuario: id_prontuario },
                success: function(data){
                    $('#dados_prontuario').html(data);
                    $('#modal-prontuario').modal();
                }
            });

        }

        $('#btn_imprimir').off('click').click(function(){

            window.open('imprimir.php?value='+id_prontuario);

        });

        $('#btn_detalhes').off('click').click(function(){

            <?php if($_SESSION['tipo_usuario'] == 'Administrador'): ?>
                detalhes();
            <?php else: ?>
                $('#modal-admin').modal();
                $('#acao').val('detalhes');
            <?php endif; ?>

        });

        $('#btn_remover').off('click').click(function(){

            <?php if($_SESSION['tipo_usuario'] == 'Administrador'): ?>
                $('#modal-remover').modal();
            <?php else: ?>
                $('#modal-admin').modal();
                $('#acao').val('remover');
            <?php endif; ?>

        });

        $('#remove_prontuario').off('click').click(function(){

            $.ajax({
                url: 'bd_remocao.php',
                method: 'post',
                data: { id_prontuario: id_prontuario },
                success: function(data){
                    $('#modal-remover').modal('hide');
                    $('#modal-remocao').modal();
                    $('#btn_editar').attr('disabled', true);
                    $('#btn_remover').attr('disabled', true);
                    $('#btn_detalhes').attr('disabled', true);
                    $('#btn_imprimir').attr('disabled', true);
                    $('#btn_novo').attr('disabled', false);
                    busca_prontuario_no_banco();
                }
            });
        });

        $('#acessa_admin').off('click').click(function(){

            var campos_vazios = false;

            if($('#usuario').val() == ''){
                $('#usuario').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#usuario').css('border-color', '');
            }

            if($('#senha').val() == ''){
                $('#senha').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#senha').css('border-color', '');
            }

            if(!campos_vazios){

                $.ajax({
                    url: 'bd_validate_login.php',
                    method: 'post',
                    data: { usuario: $('#usuario').val(),
                            senha: $('#senha').val() },
                    success: function(data){
                        var acesso = data;

                        if(acesso == 'dados_nulos' || acesso == 'dados_comum'){
                            $('#usuario').val('');
                            $('#senha').val('');
                            $('#usuario').css('border-color', 'red');
                            $('#senha').css('border-color', 'red');
                        } else if(acesso == 'dados_admin'){

                            $('#modal-admin').modal('hide');

                            if($('#acao').val() == 'editar'){
                                window.location.replace('alteracao.php?value='+id_prontuario);
                            } else if($('#acao').val() == 'detalhes'){
                                detalhes();
                            } else if($('#acao').val() == 'remover'){
                                $('#modal-remover').modal();
                            }
                        }
                    }
                });
            }
        });

        //Limpa os campos quando a modal é fechada
        $('#modal-admin').on('hidden.bs.modal', function (e) {
            $('#usuario').val('');
            $('#senha').val('');
            $('#acao').val('');
            $('#usuario').css('border-color', '');
            $('#senha').css('border-color', '');
        });

        //Ao preencher a caixa de pesquisa, busca as informações no banco de dados
        $('#tag_pesquisa').off('input').on('input',function(e){

            busca_prontuario_no_banco();

        });

        //Ao mudar o select da pesquisa, busca as informações no banco de dados
        $('#select_pesquisa').off('change').on('change',function(e){

            busca_prontuario_no_banco();

        });

        //Ao mudar o select da ordem, busca as informações no banco de dados
        $('#select_ordem').off('change').on('change',function(e){

            busca_prontuario_no_banco();

        });

        //Verifica se o checkbox decrescente está ativo ou não
        var check_desc = true;

        $('#check_desc').off('click').click(function(){

            if ($('#check_desc').is(":checked"))
            {
                check_desc = true;
            } else {
                check_desc = false;
            }

            busca_prontuario_no_banco();

        });

        function busca_prontuario_no_banco() {

            $.ajax({
                url: 'bd_consulta.php',
                method: 'post',
                data: { tag_pesquisa: $('#tag_pesquisa').val(),
                        select_pesquisa: $('#select_pesquisa').val(),
                        select_ordem: $('#select_ordem').val(),
                        check_desc: check_desc,
                        id_paciente: $('#id_paciente').val() },
                success: function(data){
                    $('#registros').html(data);

                    $(this).removeClass('success');
                    id_prontuario = 0;
                    $('#btn_editar').attr('disabled', true);
                    $('#btn_remover').attr('disabled', true);
                    $('#btn_detalhes').attr('disabled', true);
                    $('#btn_imprimir').attr('disabled', true);
                    $('#btn_novo').attr('disabled', false);

                    $('.check_prontuario').parent().parent().off('click').click(function(){

                        $('.check_prontuario').parent().parent().find('.check_prontuario').not(this).prop('checked', false);
                        $('.check_prontuario').parent().parent().not(this).removeClass('success');
                        $('.check_prontuario').parent().parent().not(this).attr('id', 'false');

                        if($(this).attr('id') == 'false'){
                            $(this).find('.check_prontuario').prop('checked', true);
                            $(this).attr('id', 'true');
                        } else {
                            $(this).find('.check_prontuario').prop('checked', false);
                            $(this).attr('id', 'false');
                        }

                        if ($(this).find('.check_prontuario').is(":checked"))
                        {
                            $(this).addClass('success');
                            id_prontuario = $(this).find('.check_prontuario').attr('id');
                            $('#btn_editar').attr('disabled', false);
                            $('#btn_remover').attr('disabled', false);
                            $('#btn_detalhes').attr('disabled', false);
                            $('#btn_imprimir').attr('disabled', false);
                            $('#btn_novo').attr('disabled', true);

                        } else {
                            $(this).removeClass('success');
                            id_prontuario = 0;
                            $('#btn_editar').attr('disabled', true);
                            $('#btn_remover').attr('disabled', true);
                            $('#btn_detalhes').attr('disabled', true);
                            $('#btn_imprimir').attr('disabled', true);
                            $('#btn_novo').attr('disabled', false);
                        }
                    });

                    $('.check_prontuario').off('click').click(function(){

                        $('.check_prontuario').not(this).prop('checked', false);
                        $('.check_prontuario').not(this).parent().parent().removeClass('success');

                        if ($('.check_prontuario').is(":checked"))
                        {
                            $(this).parent().parent().addClass('success');
                            id_prontuario = $(this).attr('id');
                            $('#btn_editar').attr('disabled', false);
                            $('#btn_remover').attr('disabled', false);
                            $('#btn_detalhes').attr('disabled', false);
                            $('#btn_imprimir').attr('disabled', false);
                            $('#btn_novo').attr('disabled', true);

                        } else {
                            $(this).parent().parent().removeClass('success');
                            id_prontuario = 0;
                            $('#btn_editar').attr('disabled', true);
                            $('#btn_remover').attr('disabled', true);
                            $('#btn_detalhes').attr('disabled', true);
                            $('#btn_imprimir').attr('disabled', true);
                            $('#btn_novo').attr('disabled', false);
                        }
                    });
                }
            });
        }

        function busca_paciente_pelo_id(){

            $.ajax({
                url: 'bd_detalhes_paciente.php',
                method: 'post',
                data: { id_paciente: id_paciente },
                success: function(data){
                    $('#dados_paciente').html(data);
                    $('#id_paciente').val($('.div_dados_paciente').attr("id"));
                    $('#nome_paciente').val($('.div_dados_paciente').data('paciente_nome'));
                    $('#id_medico').val($('.div_dados_paciente').data('medico_id'));
                    $('#nome_medico').val($('.div_dados_paciente').data('medico_nome'));

                    busca_prontuario_no_banco();
                }
            });

        }

        function busca_paciente_no_banco(){

            $.ajax({
                url: 'bd_busca_paciente.php',
                method: 'post',
                data: { tag_pesquisa: $('#tag_pesquisa_paciente').val(),
                        select_ordem: $('#select_ordem_paciente').val(),
                        check_desc: check_desc_paciente },
                success: function(data){
                    $('#div_paciente').html(data);

                    $('.tr_paciente').off('click').click(function(){
                        $('#id_paciente').val($(this).attr("id"));
                        $('#nome_paciente').val($(this).data('nome_paciente'));
                        $('#id_medico').val($(this).data('id_medico'));
                        $('#nome_medico').val($(this).data('nome_medico'));
                        $('#modal-paciente').modal('hide');

                        busca_prontuario_no_banco();
                    });
                }
            });
        }

        $('#btn_buscar_docente').off('click').click(function(){

            $('#modal-docente').modal();
            busca_docente_no_banco();

        });

        //Ao preencher a caixa de pesquisa, busca as informações no banco de dados
        $('#tag_pesquisa_docente').off('input').on('input',function(e){

            busca_docente_no_banco();

        });

        //Ao mudar o select da ordem, busca as informações no banco de dados
        $('#select_ordem_docente').off('change').on('change',function(e){

            busca_docente_no_banco();

        });

        //Verifica se o checkbox decrescente está ativo ou não
        var check_desc_docente = false;

        $('#check_desc_docente').off('click').click(function(){

            if ($('#check_desc_docente').is(":checked"))
            {
                check_desc_docente = true;
            } else {
                check_desc_docente = false;
            }

            busca_docente_no_banco();

        });

        //Limpa os campos de pesquisa e ordenacao quando a modal é fechada
        $('#modal-docente').on('hidden.bs.modal', function (e) {
            $('#tag_pesquisa_docente').val('');
            $('#select_ordem_docente').val(2);
            $('#check_desc_docente').prop('checked', false);
        });

        function busca_docente_no_banco(){

            $.ajax({
                url: 'bd_busca_docente.php',
                method: 'post',
                data: { tag_pesquisa: $('#tag_pesquisa_docente').val(),
                        select_ordem: $('#select_ordem_docente').val(),
                        check_desc: check_desc_docente },
                success: function(data){
                    $('#div_docente').html(data);

                    $('.tr_docente').off('click').click(function(){
                        $('#id_docente').val($(this).attr("id"));
                        $('#nome_docente').val($(this).data('nome_docente'));
                        $('#modal-docente').modal('hide');
                    });
                }
            });
        }

        $('#btn_buscar_discente').off('click').click(function(){

            $('#modal-discente').modal();
            busca_discente_no_banco();

        });

        //Ao preencher a caixa de pesquisa, busca as informações no banco de dados
        $('#tag_pesquisa_discente').off('input').on('input',function(e){

            busca_discente_no_banco();

        });

        //Ao mudar o select da ordem, busca as informações no banco de dados
        $('#select_ordem_discente').off('change').on('change',function(e){

            busca_discente_no_banco();

        });

        //Verifica se o checkbox decrescente está ativo ou não
        var check_desc_discente = false;

        $('#check_desc_discente').off('click').click(function(){

            if ($('#check_desc_discente').is(":checked"))
            {
                check_desc_discente = true;
            } else {
                check_desc_discente = false;
            }

            busca_discente_no_banco();

        });

        //Limpa os campos de pesquisa e ordenacao quando a modal é fechada
        $('#modal-discente').on('hidden.bs.modal', function (e) {
            $('#tag_pesquisa_discente').val('');
            $('#select_ordem_discente').val(2);
            $('#check_desc_discente').prop('checked', false);
        });

        function busca_discente_no_banco(){

            $.ajax({
                url: 'bd_busca_discente.php',
                method: 'post',
                data: { tag_pesquisa: $('#tag_pesquisa_discente').val(),
                        select_ordem: $('#select_ordem_discente').val(),
                        check_desc: check_desc_discente },
                success: function(data){
                    $('#div_discente').html(data);

                    $('.tr_discente').off('click').click(function(){
                        $('#id_discente').val($(this).attr("id"));
                        $('#nome_discente').val($(this).data('nome_discente'));
                        $('#modal-discente').modal('hide');
                    });
                }
            });
        }
    });

 </script>

</head>
<body>

    <!-- Barra de navegação -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button id="btn_menu" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar_menu" aria-expanded="false" aria-controls="navbar" title="Menu">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div id="navbar" style="padding-left: 5px; padding-top: 2px;">
                    <h4>SISTEMA DE CONTROLE DE PRONTUÁRIO DE ESTAGIÁRIOS DO CURSO DE FISIOTERAPIA</h4>
                </div>
            </div>
            <div id="navbar" class="navbar-collapse collapse navbar_menu" style="padding-right: 5px;">
                <ul class="nav navbar-nav navbar-right" style="font-size: 17px;">
                    <li><h4 style="color: white; text-shadow: 2px 2px #000; margin-right: 20px; margin-top: 15px; padding-left: 5px;">Bem vindo, <?=$_SESSION['nome_do_usuario']?></h4></li>
                    <li><a href="../prontuario/cadastro.php" title="Início" style="color: yellow;">
                        <span class="glyphicon glyphicon-home"></span>
                    </a></li>
                <?php if($_SESSION['tipo_usuario'] == 'Administrador'): ?>
                    <li><a href="../perfil/consulta.php" title="Gerenciar perfis">
                        <span class="glyphicon glyphicon-user"></span>
                    </a></li>
                <?php endif; ?>
                    <li><a style="cursor: pointer;" id="btn_logout" title="Sair">
                        <span class="glyphicon glyphicon-log-out"></span>
                    </a></li>
                </ul>
            </div>
        </div>
    </nav> <!-- navbar -->

    <div class="container-fluid">
        <div>
            <button id="mostra-menu" type="button" title="Mostrar menu">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </button>

            <!-- Sidebar -->
            <div id="sidebar-wrapper">
                <ul class="sidebar-nav">
                    <button id="fecha-menu" title="Fechar" type="button" class="close" aria-label="Close" style="background: white; margin-right: 20px; padding: 0px 3px;">
                        <span aria-hidden="true">&times;</span>
                    </button>
                <?php if($_SESSION['tipo_usuario'] == 'Administrador'): ?>
                    <li class="topicos"> <strong style="font-size: 17px">CADASTROS</strong> </li>
                    <li>
                        <a href="../medico/cadastro.php"><strong>MÉDICO</strong></a>
                    </li>
                    <li>
                        <a href="../docente/cadastro.php"><strong>DOCENTE</strong></a>
                    </li>
                    <li>
                        <a href="../paciente/cadastro.php"><strong>PACIENTE</strong></a>
                    </li>
                    <li>
                        <a href="../discente/cadastro.php"><strong>DISCENTE</strong></a>
                    </li>
                <?php endif; ?>
                    <li class="topicos"> <strong style="font-size: 17px">MOVIMENTOS</strong> </li>
                    <li class="selected">
                        <a href="cadastro.php"><strong>PRONTUÁRIO</strong></a>
                    </li>
                    <li class="topicos"> <strong style="font-size: 17px">CONSULTAS</strong> </li>
                    <li>
                        <a href="consulta.php"><strong>PRONTUÁRIO</strong></a>
                    </li>
                <?php if($_SESSION['tipo_usuario'] == 'Administrador'): ?>
                    <li>
                        <a href="../medico/consulta.php"><strong>MÉDICO</strong></a>
                    </li>
                    <li>
                        <a href="../docente/consulta.php"><strong>DOCENTE</strong></a>
                    </li>
                    <li>
                        <a href="../paciente/consulta.php"><strong>PACIENTE</strong></a>
                    </li>
                    <li>
                        <a href="../discente/consulta.php"><strong>DISCENTE</strong></a>
                    </li>
                <?php endif; ?>
                </ul>
            </div> <!-- /#sidebar-wrapper -->

        </div>

        <!-- Modal senha admin -->
        <div class="modal fade" id="modal-admin">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header" style="text-align: center;">
                        <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                        <input type="hidden" id="acao" value="">
                        <h4 class="modal-title">Esta ação requer permissão de administrador</h4>
                        <h4 class="modal-title">Favor inserir usuário e senha</h4>
                    </div>
                    <div class="modal-body">
                      <div class="row" align="center">
                        <div class="col-sm-2"></div>
                        <div class="form-group col-sm-8">
                            <label for="usuario">Nome de usuário</label>
                            <input type="text" style="text-align: center;" class="form-control" id="usuario">
                        </div>
                        <div class="col-sm-2"></div>
                      </div>
                      <div class="row" align="center">
                        <div class="col-sm-2"></div>
                        <div class="form-group col-sm-8">
                            <label for="senha">Senha</label>
                            <input type="password" style="text-align: center;" class="form-control" id="senha">
                        </div>
                        <div class="col-sm-2"></div>
                      </div>
                    </div>
                    <div class="modal-footer">
                        <button id="acessa_admin" type="button" class="btn btn-primary">Acessar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
            </div>
        </div> <!--Modal-->

        <!-- Modal cadastro com sucesso -->
        <div class="modal fade" id="modal-sucesso">
            <div class="modal-dialog">
                <div class="modal-content" style="border: 2px solid black">
                    <div class="modal-header" style="text-align: center;">
                        <button type="button" class="close" data-dismiss="modal"><span style="color: red">×</span></button>
                        <h4 class="modal-title" style="color: #01C325; text-shadow: 1px 1px #000;">Registro inserido com sucesso!</h4>
                    </div>
                </div>
            </div>
        </div> <!--Modal-->

        <!-- Modal atualizado com sucesso -->
        <div class="modal fade" id="modal-atualizacao">
            <div class="modal-dialog">
                <div class="modal-content" style="border: 2px solid black">
                    <div class="modal-header" style="text-align: center;">
                        <button type="button" class="close" data-dismiss="modal"><span style="color: red">×</span></button>
                        <h4 class="modal-title" style="color: #01C325; text-shadow: 1px 1px #000;">Registro atualizado com sucesso!</h4>
                    </div>
                </div>
            </div>
        </div> <!--Modal-->

        <!-- Modal removido com sucesso -->
        <div class="modal fade" id="modal-remocao">
            <div class="modal-dialog">
                <div class="modal-content" style="border: 2px solid black">
                    <div class="modal-header" style="text-align: center;">
                        <button type="button" class="close" data-dismiss="modal"><span style="color: red">×</span></button>
                        <h4 class="modal-title" style="color: #01C325; text-shadow: 1px 1px #000;">Registro removido com sucesso!</h4>
                    </div>
                </div>
            </div>
        </div> <!--Modal-->

        <!-- Modal remover prontuário -->
        <div class="modal fade" id="modal-remover">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header" style="text-align: center;">
                        <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                        <h4 class="modal-title">Remover prontuário</h4>
                    </div>
                    <div class="modal-body">
                      Deseja realmente remover este prontuário?
                    </div>
                    <div class="modal-footer">
                        <button id="remove_prontuario" type="button" class="btn btn-primary">Sim</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Não</button>
                    </div>
                </div>
            </div>
        </div> <!--Modal-->

        <!-- Modal paciente -->
        <div class="modal fade" id="modal-paciente">
            <div class="modal-dialog modal-mensagem-class">
                <div class="modal-content">
                    <div class="modal-header" style="text-align: center;">
                        <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                        <h4 class="modal-title">Escolha o paciente</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-6 coluna-pesquisa">
                                <input type="text" id="tag_pesquisa_paciente" class="form-control" placeholder="Pesquise aqui..." maxlength="80">
                            </div>
                            <div class="col-md-4 coluna-ordem">
                                <div class="input-group ordenacao">
                                    <label class="form-control">Ordenar por:</label>
                                    <span class="input-group-btn">
                                        <select id="select_ordem_paciente" class="btn btn-default" style="border: 2px solid gray;">
                                            <option value="3">Número do SUS</option>
                                            <option value="1" selected>Paciente</option>
                                            <option value="4">Médico</option>
                                        </select>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-2 checkbox">
                                <label><input id="check_desc_paciente" type="checkbox" value="1"><strong>Decrescente</strong></label>
                            </div>
                        </div>
                        <hr>
                        <div id="div_paciente"></div>
                    </div>
                </div>
            </div>
        </div><!--Modal paciente-->

        <!-- Modal docente -->
        <div class="modal fade" id="modal-docente">
            <div class="modal-dialog modal-mensagem-class">
                <div class="modal-content">
                    <div class="modal-header" style="text-align: center;">
                        <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                        <h4 class="modal-title">Docentes Cadastrados</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-6 coluna-pesquisa">
                                <input type="text" id="tag_pesquisa_docente" class="form-control" placeholder="Pesquise aqui..." maxlength="80">
                            </div>
                            <div class="col-md-4 coluna-ordem">
                                <div class="input-group ordenacao">
                                    <label class="form-control">Ordenar por:</label>
                                    <span class="input-group-btn">
                                        <select id="select_ordem_docente" class="btn btn-default" style="border: 2px solid gray;">
                                            <option value="1">Crefito</option>
                                            <option value="2" selected>Nome</option>
                                        </select>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-2 checkbox">
                                <label><input id="check_desc_docente" type="checkbox" value="1"><strong>Decrescente</strong></label>
                            </div>
                        </div>
                        <hr>
                        <div id="div_docente"></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal discente -->
        <div class="modal fade" id="modal-discente">
            <div class="modal-dialog modal-mensagem-class">
                <div class="modal-content">
                    <div class="modal-header" style="text-align: center;">
                        <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                        <h4 class="modal-title">Discentes Cadastrados</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-6 coluna-pesquisa">
                                <input type="text" id="tag_pesquisa_discente" class="form-control" placeholder="Pesquise aqui..." maxlength="80">
                            </div>
                            <div class="col-md-4 coluna-ordem">
                                <div class="input-group ordenacao">
                                    <label class="form-control">Ordenar por:</label>
                                    <span class="input-group-btn">
                                        <select id="select_ordem_discente" class="btn btn-default" style="border: 2px solid gray;">
                                            <option value="1">Matrícula</option>
                                            <option value="2" selected>Nome</option>
                                            <option value="3">Período</option>
                                        </select>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-2 checkbox">
                                <label><input id="check_desc_discente" type="checkbox" value="1"><strong>Decrescente</strong></label>
                            </div>
                        </div>
                        <hr>
                        <div id="div_discente"></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Div que receberá o conteúdo de detalhes do prontuário -->
        <div id="dados_prontuario"></div>

        <div id="page-content-wrapper" style="display: none;">

            <!-- Início do conteúdo em si -->
            <h3 class="page-header">Prontuários</h3>

            <div id="dados_paciente"></div>

            <form id="form-cadastro" action="" method="post">
            <!-- area de campos do form -->
                <div class="row">
                    <div class="form-group col-sm-6">
                        <input type="hidden" name="id_paciente" id="id_paciente">
                        <label for="nome_paciente">Nome do paciente <span style="color: red;">*</span></label>
                        <div class="input-group">
                            <input type="text" placeholder="Clique em &quot;Buscar&quot; para encontrar um paciente ..." class="form-control" id="nome_paciente" name="nome_paciente" readonly="readonly">
                            <span class="input-group-btn">
                                <button class="btn btn-primary" id="btn_buscar_paciente" type="button">Buscar paciente</button>
                            </span>
                        </div>
                    </div>
                    <div class="form-group col-sm-6">
                        <input type="hidden" name="id_medico" id="id_medico">
                        <label for="nome_medico">Nome do médico <span style="color: red;">*</span></label>
                        <input type="text" class="form-control" id="nome_medico" name="nome_medico" readonly="readonly" placeholder="Para buscar o médico deve-se, primeiramente, buscar o paciente">
                    </div>
                </div>
                <hr class="hr-custom">
                <div class="row botoes">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <div class="input-group">
                            <input type="text" id="tag_pesquisa" name="tag_pesquisa" class="form-control" placeholder="Pesquise aqui..." maxlength="80">
                            <span class="input-group-btn">
                                <select id="select_pesquisa" class="btn btn-default">
                                  <option value="1" selected>Geral</option>
                                  <option value="3">Docente</option>
                                  <option value="4">Discente</option>
                                  <option value="2">Data</option>
                                  <option value="5">Relatório</option>
                                </select>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <br>
                <div class="row botoes">
                    <div class="col-md-2"></div>
                    <div class="col-md-3">
                        <div class="input-group ordenacao">
                            <label id="ordem" class="form-control">Ordenar por: </label>
                            <span class="input-group-btn">
                                <select id="select_ordem" class="btn btn-default">
                                    <option value="1">Docente</option>
                                    <option value="2">Discente</option>
                                    <option value="3" selected>Data</option>
                                    <option value="4">Relatório</option>
                                </select>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-2 checkbox">
                        <label><input id="check_desc" type="checkbox" value="1"><strong>Decrescente</strong></label>
                    </div>
                    <div class="col-lg-3" style="text-align: right;">
                        <button id="btn_novo" type="button" class="btn btn-primary" title="Novo prontuário">
                            <span class="glyphicon glyphicon-plus"></span>
                        </button>
                        <button id="btn_editar" type="button" class="btn btn-success" disabled="disabled" title="Editar prontuário">
                            <span class="glyphicon glyphicon-edit"></span>
                        </button>
                        <button id="btn_remover" type="button" class="btn btn-danger" disabled="disabled" title="Remover prontuário">
                            <span class="glyphicon glyphicon-trash"></span>
                        </button>
                        <button id="btn_detalhes" type="button" class="btn btn-info" disabled="disabled" title="Detalhes">
                            <span class="glyphicon glyphicon-th-list"></span>
                        </button>
                        <button id="btn_imprimir" type="button" class="btn btn-warning" disabled="disabled" title="Imprimir">
                            <span class="glyphicon glyphicon-print"></span>
                        </button>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <hr class="hr-custom">
                <div id="cria_pront">
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <input type="hidden" name="id_docente" id="id_docente">
                            <label for="nome_docente">Nome do docente <span style="color: red;">*</span></label>
                            <div class="input-group">
                                <input type="text" placeholder="Clique em &quot;Buscar&quot; para encontrar um docente ..." class="form-control" id="nome_docente" name="nome_docente" readonly="readonly">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary" id="btn_buscar_docente" type="button">Buscar docente</button>
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-sm-6">
                            <input type="hidden" name="id_discente" id="id_discente">
                            <label for="nome_discente">Nome do discente <span style="color: red;">*</span></label>
                            <div class="input-group">
                                <input type="text" placeholder="Clique em &quot;Buscar&quot; para encontrar um discente ..." class="form-control" id="nome_discente" name="nome_discente" readonly="readonly">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary" id="btn_buscar_discente" type="button">Buscar discente</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="relatorio">Relatório: <span style="color: red;">*</span></label>
                            <textarea class="form-control" rows="5" id="relatorio" name="relatorio" maxlength="5000"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="observacao">Observações:</label>
                            <textarea class="form-control" rows="3" id="observacao" name="observacao" maxlength="2500"></textarea>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="data_prontuario">Data: <span style="color: red;">*</span></label>
                            <div class="input-group date form_datetime col-md-5" data-link-field="dtp_input1" style="width: 100%;">
                                <input class="form-control" size="16" type="text" id="data_prontuario" name="data_prontuario" readonly>
                                <span class="input-group-addon"><span title="Atualizar data e hora" class="glyphicon glyphicon-repeat reload_date_time"></span></span>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                            </div>
                            <input type="hidden" id="dtp_input1" value="">
                        </div>
                    </div>
                    <hr>
                    <div id="actions" class="row">
                        <div class="col-md-12" style="text-align: center; margin-bottom: 15px">
                            <button id="btn-logar" type="button" class="btn btn-primary">Salvar</button>
                            <button id="btn_cancelar" type="button" class="btn btn-default">Cancelar</button>
                            <button type="reset" class="btn btn-danger btn-reset">Resetar</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="alert alert-danger" style="display: none; text-align: center;">
                            <button id="btn-close-warning" type="button" class="close" title="Fechar"><span>×</span></button>
                            <strong id="warning-text"></strong>
                        </div>
                    </div>
                </div>
            </form>

            <div class="table-responsive">
                <table id="registros" class="table table-hover table-bordered">
                </table>
            </div>

        </div> <!-- page-content-wrapper -->
    </div>

 <script src="../js/bootstrap.min.js"></script>
</body>
</html>