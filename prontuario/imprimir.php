<?php

if(!isset($_SESSION)) {
    session_start();
}

if(isset($_GET['value'])):

    require_once "../conexao_bd/conexao_db_syspront.php";
    require_once "../conexao_bd/db_syspront.class.php";

    $id_prontuario = isset($_GET['value']) ? $_GET['value'] : 0;

    $db_syspront = sysPront::getInstance(Conexao::getInstance());
    $dados = $db_syspront->search_prontuario_by_id($id_prontuario);

    $data_prontuario = new DateTime($dados[0]->data_prontuario);
    $data_formatada = $data_prontuario->format('d/m/Y H:i');

    $dt_nascimento = new DateTime($dados[0]->dt_nascimento);
    $data_n_formatada = $dt_nascimento->format('d/m/Y');

    $sexo = '';
    if($dados[0]->sexo == 'M') $sexo = 'Masculino';
    else if($dados[0]->sexo == 'F') $sexo = 'Feminino';

    $endereco_completo = '';
    if(empty($dados[0]->complemento))
        $endereco_completo = $dados[0]->endereco . ', ' . $dados[0]->numero . ' - ' . $dados[0]->bairro . ', ' . $dados[0]->cidade . ' - ' . $dados[0]->estado . ', ' . $dados[0]->cep;
    else
        $endereco_completo = $dados[0]->endereco . ', ' . $dados[0]->numero . ', ' . $dados[0]->complemento . ' - ' . $dados[0]->bairro . ', ' . $dados[0]->cidade . ' - ' . $dados[0]->estado . ', ' . $dados[0]->cep;

    if(empty($dados[0]->telefone_2))
        $telefones = $dados[0]->telefone_1;
    else
        $telefones = $dados[0]->telefone_1 . ' | ' . $dados[0]->telefone_2;

    // Require composer autoload
    require_once __DIR__ . '/../vendor/autoload.php';
    // Create an instance of the class:
    //$mpdf = new \Mpdf\Mpdf();
    $mpdf = new \Mpdf\Mpdf(['tempDir' => __DIR__ . '/../mpdf_temp']);

    $bootstrap = file_get_contents('../css/bootstrap.min.css');
    $custom_css = file_get_contents('../css/sys_pront_custom.css');

    $html = '<div>';
    $html .= '<h2 class="page-header page-custom-pdf">Prontuário médico</h2>';
    $html .= '<table style="margin-top: 30px;" class="table table-custom-pdf">';
    $html .= '<tr>';
    $html .= '<th class="titulos-pdf" style="width: 60%"><h4>Nome do paciente:</h4></th>';
    $html .= '<th style="width: 10%"></th>';
    $html .= '<th class="titulos-pdf" style="width: 30%"><h4>Data de nascimento:</h4></th>';
    $html .= '</tr>';
    $html .= '<tr>';
    $html .= '<td class="dados-pdf"><h4>'.$dados[0]->nome_paciente.'</h4></td>';
    $html .= '<td></td>';
    $html .= '<td class="dados-pdf"><h4>'.$data_n_formatada.'</h4></td>';
    $html .= '</tr>';
    $html .= '</table>';
    $html .= '<table class="table table-custom-pdf">';
    $html .= '<tr>';
    $html .= '<th class="titulos-pdf" style="width: 60%"><h4>Nome da mãe:</h4></th>';
    $html .= '<th style="width: 10%"></th>';
    $html .= '<th class="titulos-pdf" style="width: 30%"><h4>Sexo:</h4></th>';
    $html .= '</tr>';
    $html .= '<tr>';
    $html .= '<td class="dados-pdf"><h4>'.$dados[0]->nome_mae.'</h4></td>';
    $html .= '<td></td>';
    $html .= '<td class="dados-pdf"><h4>'.$sexo.'</h4></td>';
    $html .= '</tr>';
    $html .= '</table>';
    $html .= '<table class="table table-custom-pdf">';
    $html .= '<tr>';
    $html .= '<th class="titulos-pdf" style="width: 40%"><h4>Naturalidade:</h4></th>';
    $html .= '<th style="width: 10%"></th>';
    $html .= '<th class="titulos-pdf" style="width: 50%"><h4>Telefone(s):</h4></th>';
    $html .= '</tr>';
    $html .= '<tr>';
    $html .= '<td class="dados-pdf"><h4>'.$dados[0]->naturalidade.'</h4></td>';
    $html .= '<td></td>';
    $html .= '<td class="dados-pdf"><h4>'.$telefones.'</h4></td>';
    $html .= '</tr>';
    $html .= '</table>';
    $html .= '<table class="table table-custom-pdf">';
    $html .= '<tr>';
    $html .= '<th class="titulos-pdf" style="width: 100%"><h4>Endereço:</h4></th>';
    $html .= '</tr>';
    $html .= '<tr>';
    $html .= '<td class="dados-pdf"><h4>'.$endereco_completo.'</h4></td>';
    $html .= '</tr>';
    $html .= '</table>';
    $html .= '<table class="table table-custom-pdf">';
    $html .= '<tr>';
    $html .= '<th class="titulos-pdf" style="width: 60%"><h4>Nome do médico:</h4></th>';
    $html .= '<th style="width: 10%"></th>';
    $html .= '<th class="titulos-pdf" style="width: 30%"><h4>Especialidade:</h4></th>';
    $html .= '</tr>';
    $html .= '<tr>';
    $html .= '<td class="dados-pdf"><h4>'.$dados[0]->nome_medico.'</h4></td>';
    $html .= '<td></td>';
    $html .= '<td class="dados-pdf"><h4>'.$dados[0]->especialidade.'</h4></td>';
    $html .= '</tr>';
    $html .= '</table>';
    $html .= '<table class="table table-custom-pdf">';
    $html .= '<tr>';
    $html .= '<th class="titulos-pdf" style="width: 40%"><h4>Número do SUS:</h4></th>';
    $html .= '<th style="width: 10%"></th>';
    $html .= '<th class="titulos-pdf" style="width: 50%"><h4>Data da última modificação:</h4></th>';
    $html .= '</tr>';
    $html .= '<tr>';
    $html .= '<td class="dados-pdf"><h4>'.$dados[0]->num_sus.'</h4></td>';
    $html .= '<td></td>';
    $html .= '<td class="dados-pdf"><h4>'.$data_formatada.'</h4></td>';
    $html .= '</tr>';
    $html .= '</table>';
    $html .= '<table class="table table-custom-pdf">';
    $html .= '<tr>';
    $html .= '<th class="titulos-pdf" style="width: 100%"><h4>Diagnóstico:</h4></th>';
    $html .= '</tr>';
    $html .= '<tr>';
    $html .= '<td class="dados-pdf"><h4>'.$dados[0]->diagnostico.'</h4></td>';
    $html .= '</tr>';
    $html .= '</table>';
    $html .= '<table class="table table-custom-pdf">';
    $html .= '<tr>';
    $html .= '<th class="titulos-pdf" style="width: 100%"><h4>Relatório:</h4></th>';
    $html .= '</tr>';
    $html .= '<tr>';
    $html .= '<td class="dados-pdf"><h4>'.$dados[0]->relatorio.'</h4></td>';
    $html .= '</tr>';
    $html .= '</table>';
    $html .= '<table class="table table-custom-pdf">';
    $html .= '<tr>';
    if(!empty($dados[0]->observacao)):
        $html .= '<th class="titulos-pdf" style="width: 100%"><h4>Observações:</h4></th>';
    endif;
    $html .= '</tr>';
    $html .= '<tr>';
    if(!empty($dados[0]->observacao)):
        $html .= '<td class="dados-pdf"><h4>'.$dados[0]->observacao.'</h4></td>';
    endif;
    $html .= '</tr>';
    $html .= '</table>';
    $html .= '</div>';

    // Write some HTML code:
    $mpdf->SetTitle('Projeto SysPront Fisioterapia - Impressão do prontuário');
    $mpdf->SetAuthor('Brenner de Araújo Rodrigues Barbosa');
    $mpdf->WriteHTML($bootstrap, 1);
    $mpdf->WriteHTML($custom_css, 1);
    $mpdf->WriteHTML($html);
    $mpdf->SetProtection(array('copy','print'), '', md5('Hesoyam123'));

    // Output a PDF file directly to the browser
    $mpdf->Output('prontuario.pdf', \Mpdf\Output\Destination::INLINE);

else:
    if(!isset($_SESSION['usuario'])) header('Location: ../index.php#erro=1');
    else header('Location: cadastro.php');

endif;