<?php

  if(!isset($_SESSION)) {
    session_start();
  }

  if(isset($_POST['id_paciente'])):

    require_once "../conexao_bd/conexao_db_syspront.php";
    require_once "../conexao_bd/db_syspront.class.php";

    $db_syspront = sysPront::getInstance(Conexao::getInstance());

    $id_paciente = $_POST['id_paciente'];

    $dados = $db_syspront->search_paciente_by_id($id_paciente);

    echo '<div class="div_dados_paciente" style="display: none;" id="'.$dados[0]->id_criptografado.'" data-paciente_nome="'.$dados[0]->nome_paciente.'" data-medico_id="'.$dados[0]->id_m_criptografado.'" data-medico_nome="'.$dados[0]->nome_medico.'">';
    echo '</div>';

  else:
    if(!isset($_SESSION['usuario'])) header('Location: ../index.php#erro=1');
    else header('Location: cadastro.php');

  endif;