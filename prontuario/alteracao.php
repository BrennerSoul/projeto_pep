<?php

    if(!isset($_SESSION)) {
        session_start();
    }

    if(!isset($_SESSION['usuario'])){
        header('Location: ../index.php#erro=1');
    }

    require_once "../conexao_bd/conexao_db_syspront.php";
    require_once "../conexao_bd/db_syspront.class.php";

    $id_prontuario = isset($_GET['value']) ? $_GET['value'] : 0;

    $db_syspront = sysPront::getInstance(Conexao::getInstance());
    $dados = $db_syspront->search_prontuario_by_id($id_prontuario);

    $data_prontuario = new DateTime($dados[0]->data_prontuario);
    $data_formatada = $data_prontuario->format('d/m/Y H:i');

?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
 <meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="shortcut icon" href="../img/favicon.ico">
 <title>Projeto SysPront Fisioterapia - Editar prontuário</title>

 <script type="text/javascript" src="../js/jquery.min.js"></script>
 <script type="text/javascript" src="../js/bootstrap-datetimepicker.js"></script>
 <link rel="stylesheet" type="text/css" media="screen" href="../css/datetimepicker.css">
 <link href="../css/bootstrap.min.css" rel="stylesheet">
 <link rel="stylesheet" type="text/css" href="../css/sys_pront_custom.css">

 <script type="text/javascript">

    $(document).ready(function(){

/*#################################################################################################*/
        //Controle do timeout
        var intervalo = '';

        contagem_timeout();
        intervalo = setTimeout(contagem_timeout, 1801000);

        function contagem_timeout(){
            $.ajax({
                url: '../sessao_timeout.php',
                method: 'post',
                data: { sessao: true },
                success: function(data){
                    if(data == 'logout'){
                        $.ajax({
                            url: '../logout.php',
                            method: 'post',
                            data: { logout: true,
                                    timeout: true },
                            success: function(data){
                                window.location.href = '../' + data;
                            }
                        });
                    }
                }
            });
        }

        $(document).off('mousedown').on('mousedown', function(){
            clearTimeout(intervalo);
            intervalo = setTimeout(contagem_timeout, 1801000);
        });

        $(document).off('mousemove').on('mousemove', function(){
            clearTimeout(intervalo);
            intervalo = setTimeout(contagem_timeout, 1801000);
        });

        $(document).off('keydown').on('keydown', function(){
            clearTimeout(intervalo);
            intervalo = setTimeout(contagem_timeout, 1801000);
        });

/*#################################################################################################*/
        //Controle da sidebar de acordo com o tamanho da tela
        //Ao redimensionar a tela, certifica-se de que a sidebar esteja na posição correta
        $(window).resize(function(){

            if($(window).width() >= 992){
                $("#sidebar-wrapper").css('width', 220);
                $("#page-content-wrapper").css('padding-left', 10);
                $("#page-content-wrapper").css('margin-left', 230);
            } else {
                $("#sidebar-wrapper").css('width', 0);
                $("#page-content-wrapper").css('padding-left', 15);
                $("#page-content-wrapper").css('margin-left', 0);
            }

        });

        //Ao clicar, mostra a sidebar
        $("#mostra-menu").click(function(e) {
            e.preventDefault();
            $("#sidebar-wrapper").css('width', 220);
            $("#page-content-wrapper").css('padding-left', 10);
            $("#page-content-wrapper").css('margin-left', 0);
        });

        //Ao clicar, fecha a sidebar
        $("#fecha-menu").click(function(e) {
            e.preventDefault();
            $("#sidebar-wrapper").css('width', 0);
            $("#page-content-wrapper").css('padding-left', 15);
            $("#page-content-wrapper").css('margin-left', 0);
        });

/*#################################################################################################*/

        $('#page-content-wrapper').fadeIn(500);

        //Controlador do calendário
        $('.form_datetime').datetimepicker({
            //language:  'pt-BR',
            format: 'dd/mm/yyyy hh:ii',
            startDate: '-150y',
            endDate: '+5m',
            weekStart: 1,
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0,
            showMeridian: 0
        });

        //Logout de forma mais segura
        $('a').off('click').click(function(){
            if($(this).attr('id') == 'btn_logout'){
                $.ajax({
                    url: '../logout.php',
                    method: 'post',
                    data: { logout: true },
                    success: function(data){
                        window.location.href = '../' + data;
                    }
                });
            } else {
                event.preventDefault();
                linkLocation = this.href;
                $('#page-content-wrapper').fadeOut(200, function(){
                    window.location.href = linkLocation;
                });
            }
        });

        //Adiciona zero à esquerda nas horas e minutos
        function addZero(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }

        //Atualiza data e hora no input
        $('.reload_date_time').off('click').click(function(){

            var d = new Date();
            var strDate = d.getDate() + "/" + (d.getMonth()+1) + "/" + d.getFullYear() + " " + addZero(d.getHours()) + ":" + addZero(d.getMinutes());
            $('#data_prontuario').val(strDate);

        });

        //Validações dos campos de cadastro
        $('#btn-logar').off('click').click(function(){

            var campos_vazios = false;
            var warning_text = '';

            if($('#nome_paciente').val() == ''){
                warning_text += 'Campo Nome do paciente deve ser preenchido!<br>';
                $('#nome_paciente').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#nome_paciente').css('border-color', '');
            }

            if($('#nome_medico').val() == ''){
                warning_text += 'Campo Nome do médico deve ser preenchido!<br>';
                $('#nome_medico').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#nome_medico').css('border-color', '');
            }

            if($('#nome_docente').val() == ''){
                warning_text += 'Campo Nome do docente deve ser preenchido!<br>';
                $('#nome_docente').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#nome_docente').css('border-color', '');
            }

            if($('#nome_discente').val() == ''){
                warning_text += 'Campo Nome do discente deve ser preenchido!<br>';
                $('#nome_discente').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#nome_discente').css('border-color', '');
            }

            if($('#relatorio').val() == ''){
                warning_text += 'Campo Relatório deve ser preenchido!<br>';
                $('#relatorio').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#relatorio').css('border-color', '');
            }

            if($('#data_prontuario').val() == ''){
                warning_text += 'Campo Data deve ser preenchido!<br>';
                $('#data_prontuario').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#data_prontuario').css('border-color', '');
            }

            if(campos_vazios){
                $('.alert').fadeIn();
                $('#warning-text').html(warning_text);
                $('html, body').animate({
                    scrollTop: ($('#warning-text').offset().top)
                },1000);
            } else {
                $('.alert').fadeOut();
                $('#form-edicao').attr("action","bd_alteracao.php");
                $('#page-content-wrapper').fadeOut(200, function(){
                    $("#form-edicao").submit();
                });
            }

        });

        $('#btn_cancelar').off('click').click(function(){
            window.location.replace('cadastro.php?value=' + $('#id_paciente').val());
        });

        //Fecha os avisos
        $('#btn-close-warning').off('click').click(function(){

            $('html, body').animate({
                scrollTop: 31,
            }, 700, function(){
                $('.alert').fadeOut();
            });

        });

        $('.btn-reset').off('click').click(function(){

            $('html, body').animate({
                scrollTop: 31,
            }, 700, function(){
                $('.alert').fadeOut();
            });

            $('#id_paciente').val('');
            $('#nome_paciente').val('');
            $('#nome_paciente').css('border-color', '');
            $('#nome_medico').val('');
            $('#nome_medico').css('border-color', '');
            $('#nome_docente').val('');
            $('#nome_docente').css('border-color', '');
            $('#nome_discente').val('');
            $('#nome_discente').css('border-color', '');
            $('#relatorio').val('');
            $('#relatorio').css('border-color', '');
            $('#observacao').val('');
            $('#observacao').css('border-color', '');
            $('#data_prontuario').val('');
            $('#data_prontuario').css('border-color', '');

        });

        $('#btn_buscar_paciente').off('click').click(function(){

            $('#modal-paciente').modal();

            busca_paciente_no_banco();

        });

        //Ao preencher a caixa de pesquisa, busca as informações no banco de dados
        $('#tag_pesquisa_paciente').off('input').on('input',function(e){

            busca_paciente_no_banco();

        });

        //Ao mudar o select da ordem, busca as informações no banco de dados
        $('#select_ordem_paciente').off('change').on('change',function(e){

            busca_paciente_no_banco();

        });

        //Verifica se o checkbox decrescente está ativo ou não
        var check_desc_paciente = false;

        $('#check_desc_paciente').off('click').click(function(){

            if ($('#check_desc_paciente').is(":checked"))
            {
                check_desc_paciente = true;
            } else {
                check_desc_paciente = false;
            }

            busca_paciente_no_banco();

        });

        //Limpa os campos de pesquisa e ordenacao quando a modal é fechada
        $('#modal-paciente').on('hidden.bs.modal', function (e) {
            $('#tag_pesquisa_paciente').val('');
            $('#select_ordem_paciente').val(1);
            $('#check_desc_paciente').prop('checked', false);
        });

        function busca_paciente_no_banco(){

            $.ajax({
                url: 'bd_busca_paciente.php',
                method: 'post',
                data: { tag_pesquisa: $('#tag_pesquisa_paciente').val(),
                        select_ordem: $('#select_ordem_paciente').val(),
                        check_desc: check_desc_paciente },
                success: function(data){
                    $('#div_paciente').html(data);

                    $('.tr_paciente').off('click').click(function(){
                        $('#id_paciente').val($(this).attr("id"));
                        $('#nome_paciente').val($(this).data('nome_paciente'));
                        $('#id_medico').val($(this).data('id_medico'));
                        $('#nome_medico').val($(this).data('nome_medico'));
                        $('#modal-paciente').modal('hide');

                        //busca_prontuario_no_banco();
                    });
                }
            });
        }

        $('#btn_buscar_docente').off('click').click(function(){

            $('#modal-docente').modal();

            busca_docente_no_banco();

        });

        //Ao preencher a caixa de pesquisa, busca as informações no banco de dados
        $('#tag_pesquisa_docente').off('input').on('input',function(e){

            busca_docente_no_banco();

        });

        //Ao mudar o select da ordem, busca as informações no banco de dados
        $('#select_ordem_docente').off('change').on('change',function(e){

            busca_docente_no_banco();

        });

        //Verifica se o checkbox decrescente está ativo ou não
        var check_desc_docente = false;

        $('#check_desc_docente').off('click').click(function(){

            if ($('#check_desc_docente').is(":checked"))
            {
                check_desc_docente = true;
            } else {
                check_desc_docente = false;
            }

            busca_docente_no_banco();

        });

        //Limpa os campos de pesquisa e ordenacao quando a modal é fechada
        $('#modal-docente').on('hidden.bs.modal', function (e) {
            $('#tag_pesquisa_docente').val('');
            $('#select_ordem_docente').val(2);
            $('#check_desc_docente').prop('checked', false);
        });

        function busca_docente_no_banco(){

            $.ajax({
                url: 'bd_busca_docente.php',
                method: 'post',
                data: { tag_pesquisa: $('#tag_pesquisa_docente').val(),
                        select_ordem: $('#select_ordem_docente').val(),
                        check_desc: check_desc_docente },
                success: function(data){
                    $('#div_docente').html(data);

                    $('.tr_docente').off('click').click(function(){
                        $('#id_docente').val($(this).attr("id"));
                        $('#nome_docente').val($(this).data('nome_docente'));
                        $('#modal-docente').modal('hide');
                    });
                }
            });
        }

        $('#btn_buscar_discente').off('click').click(function(){

            $('#modal-discente').modal();

            busca_discente_no_banco();

        });

        //Ao preencher a caixa de pesquisa, busca as informações no banco de dados
        $('#tag_pesquisa_discente').off('input').on('input',function(e){

            busca_discente_no_banco();

        });

        //Ao mudar o select da ordem, busca as informações no banco de dados
        $('#select_ordem_discente').off('change').on('change',function(e){

            busca_discente_no_banco();

        });

        //Verifica se o checkbox decrescente está ativo ou não
        var check_desc_discente = false;

        $('#check_desc_discente').off('click').click(function(){

            if ($('#check_desc_discente').is(":checked"))
            {
                check_desc_discente = true;
            } else {
                check_desc_discente = false;
            }

            busca_discente_no_banco();

        });

        //Limpa os campos de pesquisa e ordenacao quando a modal é fechada
        $('#modal-discente').on('hidden.bs.modal', function (e) {
            $('#tag_pesquisa_discente').val('');
            $('#select_ordem_discente').val(2);
            $('#check_desc_discente').prop('checked', false);
        });

        function busca_discente_no_banco(){

            $.ajax({
                url: 'bd_busca_discente.php',
                method: 'post',
                data: { tag_pesquisa: $('#tag_pesquisa_discente').val(),
                        select_ordem: $('#select_ordem_discente').val(),
                        check_desc: check_desc_discente },
                success: function(data){
                    $('#div_discente').html(data);

                    $('.tr_discente').off('click').click(function(){
                        $('#id_discente').val($(this).attr("id"));
                        $('#nome_discente').val($(this).data('nome_discente'));
                        $('#modal-discente').modal('hide');
                    });
                }
            });
        }

    });

 </script>

</head>
<body>

    <!-- Barra de navegação -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button id="btn_menu" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar_menu" aria-expanded="false" aria-controls="navbar" title="Menu">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div id="navbar" style="padding-left: 5px; padding-top: 2px;">
                    <h4>SISTEMA DE CONTROLE DE PRONTUÁRIO DE ESTAGIÁRIOS DO CURSO DE FISIOTERAPIA</h4>
                </div>
            </div>
            <div id="navbar" class="navbar-collapse collapse navbar_menu" style="padding-right: 5px;">
                <ul class="nav navbar-nav navbar-right" style="font-size: 17px;">
                    <li><h4 style="color: white; text-shadow: 2px 2px #000; margin-right: 20px; margin-top: 15px; padding-left: 5px;">Bem vindo, <?=$_SESSION['nome_do_usuario']?></h4></li>
                    <li><a href="../prontuario/cadastro.php" title="Início">
                        <span class="glyphicon glyphicon-home"></span>
                    </a></li>
                    <li><a href="../perfil/consulta.php" title="Gerenciar perfis">
                        <span class="glyphicon glyphicon-user"></span>
                    </a></li>
                    <li><a style="cursor: pointer;" id="btn_logout" title="Sair">
                        <span class="glyphicon glyphicon-log-out"></span>
                    </a></li>
                </ul>
            </div>
        </div>
    </nav> <!-- navbar -->

    <div class="container-fluid">
        <div>
            <button id="mostra-menu" type="button" title="Mostrar menu">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </button>

            <!-- Sidebar -->
            <div id="sidebar-wrapper">
                <ul class="sidebar-nav">
                    <button id="fecha-menu" title="Fechar" type="button" class="close" aria-label="Close" style="background: white; margin-right: 20px; padding: 0px 3px;">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <li class="topicos"> <strong style="font-size: 17px">CADASTROS</strong> </li>
                    <li>
                        <a href="../medico/cadastro.php"><strong>MÉDICO</strong></a>
                    </li>
                    <li>
                        <a href="../docente/cadastro.php"><strong>DOCENTE</strong></a>
                    </li>
                    <li>
                        <a href="../paciente/cadastro.php"><strong>PACIENTE</strong></a>
                    </li>
                    <li>
                        <a href="../discente/cadastro.php"><strong>DISCENTE</strong></a>
                    </li>
                    <li class="topicos"> <strong style="font-size: 17px">MOVIMENTOS</strong> </li>
                    <li>
                        <a href="cadastro.php"><strong>PRONTUÁRIO</strong></a>
                    </li>
                    <li class="topicos"> <strong style="font-size: 17px">CONSULTAS</strong> </li>
                    <li>
                        <a href="consulta.php"><strong>PRONTUÁRIO</strong></a>
                    </li>
                    <li>
                        <a href="../medico/consulta.php"><strong>MÉDICO</strong></a>
                    </li>
                    <li>
                        <a href="../docente/consulta.php"><strong>DOCENTE</strong></a>
                    </li>
                    <li>
                        <a href="../paciente/consulta.php"><strong>PACIENTE</strong></a>
                    </li>
                    <li>
                        <a href="../discente/consulta.php"><strong>DISCENTE</strong></a>
                    </li>
                </ul>
            </div> <!-- /#sidebar-wrapper -->

        </div>

        <div id="page-content-wrapper" style="display: none;">

            <!-- Modal paciente -->
            <div class="modal fade" id="modal-paciente">
                <div class="modal-dialog modal-mensagem-class">
                    <div class="modal-content">
                        <div class="modal-header" style="text-align: center;">
                            <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                            <h4 class="modal-title">Escolha o paciente</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-6 coluna-pesquisa">
                                    <input type="text" id="tag_pesquisa_paciente" class="form-control" placeholder="Pesquise aqui..." maxlength="80">
                                </div>
                                <div class="col-md-4 coluna-ordem">
                                    <div class="input-group ordenacao">
                                        <label class="form-control">Ordenar por:</label>
                                        <span class="input-group-btn">
                                            <select id="select_ordem_paciente" class="btn btn-default" style="border: 2px solid gray;">
                                                <option value="3">Número do SUS</option>
                                                <option value="1" selected>Paciente</option>
                                                <option value="4">Médico</option>
                                            </select>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-2 checkbox">
                                    <label><input id="check_desc_paciente" type="checkbox" value="1"><strong>Decrescente</strong></label>
                                </div>
                            </div>
                            <hr>
                            <div id="div_paciente"></div>
                        </div>
                    </div>
                </div>
            </div><!--Modal paciente-->

            <!-- Modal docente -->
            <div class="modal fade" id="modal-docente">
                <div class="modal-dialog modal-mensagem-class">
                    <div class="modal-content">
                        <div class="modal-header" style="text-align: center;">
                            <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                            <h4 class="modal-title">Docentes Cadastrados</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-6 coluna-pesquisa">
                                    <input type="text" id="tag_pesquisa_docente" class="form-control" placeholder="Pesquise aqui..." maxlength="80">
                                </div>
                                <div class="col-md-4 coluna-ordem">
                                    <div class="input-group ordenacao">
                                        <label class="form-control">Ordenar por:</label>
                                        <span class="input-group-btn">
                                            <select id="select_ordem_docente" class="btn btn-default" style="border: 2px solid gray;">
                                                <option value="1">Crefito</option>
                                                <option value="2" selected>Nome</option>
                                            </select>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-2 checkbox">
                                    <label><input id="check_desc_docente" type="checkbox" value="1"><strong>Decrescente</strong></label>
                                </div>
                            </div>
                            <hr>
                            <div id="div_docente"></div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal discente -->
            <div class="modal fade" id="modal-discente">
                <div class="modal-dialog modal-mensagem-class">
                    <div class="modal-content">
                        <div class="modal-header" style="text-align: center;">
                            <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                            <h4 class="modal-title">Discentes Cadastrados</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-6 coluna-pesquisa">
                                    <input type="text" id="tag_pesquisa_discente" class="form-control" placeholder="Pesquise aqui..." maxlength="80">
                                </div>
                                <div class="col-md-4 coluna-ordem">
                                    <div class="input-group ordenacao">
                                        <label class="form-control">Ordenar por:</label>
                                        <span class="input-group-btn">
                                            <select id="select_ordem_discente" class="btn btn-default" style="border: 2px solid gray;">
                                                <option value="1">Matrícula</option>
                                                <option value="2" selected>Nome</option>
                                                <option value="3">Período</option>
                                            </select>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-2 checkbox">
                                    <label><input id="check_desc_discente" type="checkbox" value="1"><strong>Decrescente</strong></label>
                                </div>
                            </div>
                            <hr>
                            <div id="div_discente"></div>
                        </div>
                    </div>
                </div>
            </div>

            <?php if($dados == null):
                echo '<div class="alert alert-danger" style="text-align: center; margin-top: 50px;">';
                echo '<h4>Informações não encontradas!</h4>';
                echo '</div>';
            else: ?>

            <h3 class="page-header">Editar Prontuário</h3>

            <form id="form-edicao" action="" method="post">
            <!-- area de campos do form -->
                <?= '<input type="hidden" name="id" value="'.$dados[0]->id_p_criptografado.'">' ?>
                <div class="row">
                    <div class="form-group col-sm-6">
                        <?= '<input type="hidden" name="id_paciente" id="id_paciente" value="'.$dados[0]->id_criptografado.'">' ?>
                        <label for="nome_paciente">Nome do paciente <span style="color: red;">*</span></label>
                        <div class="input-group">
                            <?= '<input type="text" placeholder="Clique em &quot;Buscar&quot; para encontrar um paciente ..." class="form-control" id="nome_paciente" name="nome_paciente" readonly="readonly" value="'.$dados[0]->nome_paciente.'">' ?>
                            <span class="input-group-btn">
                                <button class="btn btn-primary" id="btn_buscar_paciente" type="button">Buscar paciente</button>
                            </span>
                        </div>
                    </div>
                    <div class="form-group col-sm-6">
                        <?= '<input type="hidden" name="id_medico" id="id_medico" value="'.$dados[0]->id_m_criptografado.'">' ?>
                        <label for="nome_medico">Nome do médico <span style="color: red;">*</span></label>
                        <?= '<input type="text" class="form-control" id="nome_medico" name="nome_medico" readonly="readonly" placeholder="Para buscar o médico deve-se, primeiramente, buscar o paciente" value="'.$dados[0]->nome_medico.'">' ?>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-sm-6">
                        <?= '<input type="hidden" name="id_docente" id="id_docente" value="'.$dados[0]->id_d_criptografado.'">' ?>
                        <label for="nome_docente">Nome do docente <span style="color: red;">*</span></label>
                        <div class="input-group">
                            <?= '<input type="text" placeholder="Clique em &quot;Buscar&quot; para encontrar um docente ..." class="form-control" id="nome_docente" name="nome_docente" readonly="readonly" value="'.$dados[0]->nome_docente.'">' ?>
                            <span class="input-group-btn">
                                <button class="btn btn-primary" id="btn_buscar_docente" type="button">Buscar docente</button>
                            </span>
                        </div>
                    </div>
                    <div class="form-group col-sm-6">
                        <?= '<input type="hidden" name="id_discente" id="id_discente" value="'.$dados[0]->id_ds_criptografado.'">' ?>
                        <label for="nome_discente">Nome do discente <span style="color: red;">*</span></label>
                        <div class="input-group">
                            <?= '<input type="text" placeholder="Clique em &quot;Buscar&quot; para encontrar um discente ..." class="form-control" id="nome_discente" name="nome_discente" readonly="readonly" value="'.$dados[0]->nome_discente.'">' ?>
                            <span class="input-group-btn">
                                <button class="btn btn-primary" id="btn_buscar_discente" type="button">Buscar discente</button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-sm-12">
                        <label for="relatorio">Relatório: <span style="color: red;">*</span></label>
                        <?= '<textarea class="form-control" rows="5" id="relatorio" name="relatorio" maxlength="5000">'.$dados[0]->relatorio.'</textarea>' ?>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-sm-6">
                        <label for="observacao">Observações:</label>
                        <?= '<textarea class="form-control" rows="3" id="observacao" name="observacao" maxlength="2500">'.$dados[0]->observacao.'</textarea>' ?>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="data_prontuario">Data: <span style="color: red;">*</span></label>
                        <div class="input-group date form_datetime col-md-5" data-link-field="dtp_input1" style="width: 100%;">
                            <?= '<input class="form-control" size="16" type="text" id="data_prontuario" name="data_prontuario" readonly value="'.$data_formatada.'">' ?>
                            <span class="input-group-addon"><span title="Atualizar data e hora" class="glyphicon glyphicon-repeat reload_date_time"></span></span>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                        </div>
                        <input type="hidden" id="dtp_input1" value="">
                    </div>
                    </div>
                    <hr>
                    <div id="actions" class="row">
                        <div class="col-md-12" style="text-align: center; margin-bottom: 15px">
                            <button id="btn-logar" type="button" class="btn btn-primary">Salvar</button>
                            <button id="btn_cancelar" type="button" class="btn btn-default">Cancelar</button>
                            <button type="button" class="btn btn-danger btn-reset">Resetar</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="alert alert-danger" style="display: none; text-align: center;">
                            <button id="btn-close-warning" type="button" class="close" title="Fechar"><span>×</span></button>
                            <strong id="warning-text"></strong>
                        </div>
                    </div>
            </form>
        <?php endif; ?>
        </div> <!-- page-content-wrapper -->
    </div>

 <script src="../js/bootstrap.min.js"></script>
</body>
</html>