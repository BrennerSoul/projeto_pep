<?php

  if(!isset($_SESSION)) {
    session_start();
  }

  if (strcmp(basename($_SERVER['SCRIPT_NAME']), basename(__FILE__)) === 0){
      header('Location: ../index.php#erro=1');
  }
/*
 * Classe para operações CRUD nas tabelas
 */
class sysPront{

  /*
   * Atributo para conexão com o banco de dados
   */
  private $pdo = null;

  /*
   * Atributo estático para instância da própria classe
   */
  private static $sysPront = null;

  /*
   * Construtor da classe como private
   * @param $conexao - Conexão com o banco de dados
   */
  private function __construct($conexao){
    $this->pdo = $conexao;
  }

  /*
  * Método estático para retornar um objeto sysPront
  * Verifica se já existe uma instância desse objeto, caso não, instância um novo
  * @param $conexao - Conexão com o banco de dados
  * @return $sysPront - Instancia do objeto sysPront
  */
  public static function getInstance($conexao){
   if (!isset(self::$sysPront)):
    self::$sysPront = new sysPront($conexao);
   endif;
   return self::$sysPront;
  }

  function gravar($arquivo, $texto){
    //Variável arquivo armazena o nome e extensão do arquivo.
    $arquivo .= "arquivos_log/log_mes".date('m')."_ano".date('Y').".txt";

    //Variável $fp armazena a conexão com o arquivo e o tipo de ação.
    $fp = fopen($arquivo, "a+");

    //Escreve no arquivo aberto.
    fwrite($fp, $texto . date('d/m/Y H:i') . "\r\n");

    //Fecha o arquivo.
    fclose($fp);

  }

  public function get_auto_increment($tabela){
    if(!empty($tabela)):
      try{
        $sql = "SELECT auto_increment FROM information_schema.tables WHERE table_name = ? AND table_schema = 'syspront'";
        $stm = $this->pdo->prepare($sql);
        $stm->bindValue(1, $tabela);
        $stm->execute();
        $dados = $stm->fetchAll(PDO::FETCH_OBJ);
        return $dados;
      }catch(PDOException $erro){
        return $erro->errorInfo[1];
      }
    endif;
  }

  //Tabela login

  public function validate_login($usuario, $senha){
    if(!empty($usuario) && !empty($senha)):
      try{
        $sql = "SELECT * FROM tb_login WHERE nome_usuario = ? AND senha = ?";
        $stm = $this->pdo->prepare($sql);
        $stm->bindValue(1, $usuario);
        $stm->bindValue(2, $senha);
        $stm->execute();
        $dados = $stm->fetchAll(PDO::FETCH_OBJ);
      return $dados;
      } catch(PDOException $erro){
        return $erro->errorInfo[1];
      }
    endif;
  }

  public function validate_senha_atual($id_criptografado, $senha){
    if(!empty($id_criptografado) && !empty($senha)):
      try{
        $sql = "SELECT * FROM tb_login WHERE id_criptografado = ? AND senha = ?";
        $stm = $this->pdo->prepare($sql);
        $stm->bindValue(1, $id_criptografado);
        $stm->bindValue(2, $senha);
        $stm->execute();
        $dados = $stm->fetchAll(PDO::FETCH_OBJ);
        return $dados;
      } catch(PDOException $erro){
        return $erro->errorInfo[1];
      }
    endif;
  }

  public function insert_usuario($usuario, $senha, $tipo_usuario){
    if (!empty($usuario) && !empty($senha) && !empty($tipo_usuario)):
      try{
        $sql = "INSERT INTO tb_login (nome_usuario, senha, tipo_usuario, id_criptografado) VALUES (?, ?, ?, ?)";
        $stm = $this->pdo->prepare($sql);
        $stm->bindValue(1, $usuario);
        $stm->bindValue(2, $senha);
        $stm->bindValue(3, $tipo_usuario);
        $stm->bindValue(4, md5('id='.$this->get_auto_increment('tb_login')[0]->auto_increment));
        $stm->execute();
      }catch(PDOException $erro){
        return $erro->errorInfo[1];
      }
    endif;
  }

  public function update_usuario($usuario, $senha, $tipo_usuario, $id_criptografado){
    if (!empty($usuario) && !empty($senha) && !empty($tipo_usuario) && !empty($id_criptografado)):
      try{
        $sql = "UPDATE tb_login SET nome_usuario=?, senha=?, tipo_usuario=? WHERE id_criptografado=?";
        $stm = $this->pdo->prepare($sql);
        $stm->bindValue(1, $usuario);
        $stm->bindValue(2, $senha);
        $stm->bindValue(3, $tipo_usuario);
        $stm->bindValue(4, $id_criptografado);
        $stm->execute();
      }catch(PDOException $erro){
        return $erro->errorInfo[1];
      }
    endif;
  }

  public function set_crm_usuario($novo_crm, $id_criptografado){
    if (!empty($novo_crm) && !empty($id_criptografado)):
      $dados = $this->get_crm_usuario($id_criptografado);
      try{
        $sql = "UPDATE tb_login SET nome_usuario=? WHERE nome_usuario=?";
        $stm = $this->pdo->prepare($sql);
        $stm->bindValue(1, $novo_crm);
        $stm->bindValue(2, $dados[0]->crm_medico);
        $stm->execute();
      }catch(PDOException $erro){
        return $erro->errorInfo[1];
      }
    endif;
  }

  public function set_matricula_usuario($nova_matricula, $id_criptografado){
    if (!empty($nova_matricula) && !empty($id_criptografado)):
      $dados = $this->get_matricula_usuario($id_criptografado);
      try{
        $sql = "UPDATE tb_login SET nome_usuario=? WHERE nome_usuario=?";
        $stm = $this->pdo->prepare($sql);
        $stm->bindValue(1, $nova_matricula);
        $stm->bindValue(2, $dados[0]->num_matricula);
        $stm->execute();
      }catch(PDOException $erro){
        return $erro->errorInfo[1];
      }
    endif;
  }

  public function delete_matricula_usuario($num_matricula){
      try{
        $sql = "DELETE FROM tb_login WHERE nome_usuario=?";
        $stm = $this->pdo->prepare($sql);
        $stm->bindValue(1, $num_matricula);
        $stm->execute();
      }catch(PDOException $erro){
        return $erro->errorInfo[1];
      }
  }

  public function delete_crm_usuario($crm_medico){
      try{
        $sql = "DELETE FROM tb_login WHERE nome_usuario=?";
        $stm = $this->pdo->prepare($sql);
        $stm->bindValue(1, $crm_medico);
        $stm->execute();
      }catch(PDOException $erro){
        return $erro->errorInfo[1];
      }
  }

  public function delete_usuario($id_criptografado){
    if (!empty($id_criptografado)):
      try{
        $sql = "DELETE FROM tb_login WHERE id_criptografado=?";
        $stm = $this->pdo->prepare($sql);
        $stm->bindValue(1, $id_criptografado);
        $stm->execute();
      }catch(PDOException $erro){
        return $erro->errorInfo[1];
      }
    endif;
  }

  public function search_usuario($tag_pesquisa, $select_pesquisa, $select_ordem, $check_desc, $id){

    if(!empty($select_ordem)):
      $order_by = "";
      switch ($select_ordem) {
        case 1:
          $order_by = " ORDER BY nome_usuario";
          break;
        case 2:
          $order_by = " ORDER BY tipo_usuario";
          break;
      }
    endif;

    $desc = "";
    if($check_desc == 'true') $desc = " DESC;";

    if(!empty($tag_pesquisa)):
      if(!empty($select_pesquisa)):
        try{

          switch ($select_pesquisa) {
            case 1:
              $sql = "SELECT * FROM tb_login WHERE id_criptografado != ? AND (nome_usuario LIKE ? OR tipo_usuario LIKE ?) AND nome_usuario != ?" . $order_by . $desc;
              $stm = $this->pdo->prepare($sql);
              $stm->bindValue(1, $id);
              $stm->bindValue(2, '%'.$tag_pesquisa.'%');
              $stm->bindValue(3, '%'.$tag_pesquisa.'%');
              $stm->bindValue(4, 'brenner_admin');
              break;
            case 2:
              $sql = "SELECT * FROM tb_login WHERE id_criptografado != ? AND nome_usuario LIKE ? AND nome_usuario != ?" . $order_by . $desc;
              $stm = $this->pdo->prepare($sql);
              $stm->bindValue(1, $id);
              $stm->bindValue(2, '%'.$tag_pesquisa.'%');
              $stm->bindValue(3, 'brenner_admin');
              break;
            case 3:
              $sql = "SELECT * FROM tb_login WHERE id_criptografado != ? AND tipo_usuario LIKE ? AND nome_usuario != ?" . $order_by . $desc;
              $stm = $this->pdo->prepare($sql);
              $stm->bindValue(1, $id);
              $stm->bindValue(2, '%'.$tag_pesquisa.'%');
              $stm->bindValue(3, 'brenner_admin');
              break;
          }

          $stm->execute();
          $dados = $stm->fetchAll(PDO::FETCH_OBJ);
          return $dados;
        } catch(PDOException $erro){
          return $erro->errorInfo[1];
        }
      endif;
    else:
      try{
        $sql = "SELECT * FROM tb_login WHERE id_criptografado != ? AND nome_usuario != ?" . $order_by . $desc;
        $stm = $this->pdo->prepare($sql);
        $stm->bindValue(1, $id);
        $stm->bindValue(2, 'brenner_admin');
        $stm->execute();
        $dados = $stm->fetchAll(PDO::FETCH_OBJ);
        return $dados;
      }catch(PDOException $erro){
        return $erro->errorInfo[1];
      }
    endif;
  }

  public function search_usuario_by_id($id_criptografado){
    try{
      $sql = "SELECT * FROM tb_login WHERE id_criptografado = ?";
      $stm = $this->pdo->prepare($sql);
      $stm->bindValue(1, $id_criptografado);
      $stm->execute();
      $dados = $stm->fetchAll(PDO::FETCH_OBJ);
      return $dados;
    }catch(PDOException $erro){
      return $erro->errorInfo[1];
    }
  }

  //Tabela médico

  public function insert_medico($nome, $especialidade, $crm){
    if (!empty($nome) && !empty($especialidade) && !empty($crm)):
      try{
        $sql = "INSERT INTO tb_medico (nome_medico, especialidade, crm_medico, id_m_criptografado) VALUES (?, ?, ?, ?)";
        $stm = $this->pdo->prepare($sql);
        $stm->bindValue(1, $nome);
        $stm->bindValue(2, $especialidade);
        $stm->bindValue(3, $crm);
        $stm->bindValue(4, md5('id='.$this->get_auto_increment('tb_medico')[0]->auto_increment));
        $stm->execute();
      }catch(PDOException $erro){
        return $erro->errorInfo[1];
      }
    endif;
  }

  public function update_medico($nome, $especialidade, $crm, $id_criptografado){
    if (!empty($nome) && !empty($especialidade) && !empty($crm) && !empty($id_criptografado)):
      try{
        $this->set_crm_usuario($crm, $id_criptografado);
        $sql = "UPDATE tb_medico SET nome_medico=?, especialidade=?, crm_medico=? WHERE id_m_criptografado=?";
        $stm = $this->pdo->prepare($sql);
        $stm->bindValue(1, $nome);
        $stm->bindValue(2, $especialidade);
        $stm->bindValue(3, $crm);
        $stm->bindValue(4, $id_criptografado);
        $stm->execute();
      }catch(PDOException $erro){
        return $erro->errorInfo[1];
      }
    endif;
  }

  public function delete_medico($id_criptografado){
    if (!empty($id_criptografado)):
      $dados = $this->get_crm_usuario($id_criptografado);
      try{
        $sql = "DELETE FROM tb_medico WHERE id_m_criptografado=?";
        $stm = $this->pdo->prepare($sql);
        $stm->bindValue(1, $id_criptografado);
        $stm->execute();
        $this->delete_crm_usuario($dados[0]->crm_medico);
      }catch(PDOException $erro){
        return $erro->errorInfo[1];
      }
    endif;
  }

  public function search_medico($tag_pesquisa, $select_pesquisa, $select_ordem, $check_desc){

    if(!empty($select_ordem)):
      $order_by = "";
      switch ($select_ordem) {
        case 1:
          $order_by = " ORDER BY crm_medico";
          break;
        case 2:
          $order_by = " ORDER BY nome_medico";
          break;
        case 3:
          $order_by = " ORDER BY especialidade";
          break;
      }
    endif;

    $desc = "";
    if($check_desc == 'true') $desc = " DESC;";

    if(!empty($tag_pesquisa)):
      if(!empty($select_pesquisa)):
        try{

          switch ($select_pesquisa) {
            case 1:
              $sql = "SELECT * FROM tb_medico WHERE nome_medico LIKE ? OR especialidade LIKE ? OR crm_medico LIKE ?" . $order_by . $desc;
              $stm = $this->pdo->prepare($sql);
              $stm->bindValue(1, '%'.$tag_pesquisa.'%');
              $stm->bindValue(2, '%'.$tag_pesquisa.'%');
              $stm->bindValue(3, '%'.$tag_pesquisa.'%');
              break;
            case 2:
              $sql = "SELECT * FROM tb_medico WHERE crm_medico LIKE ?" . $order_by . $desc;
              $stm = $this->pdo->prepare($sql);
              $stm->bindValue(1, '%'.$tag_pesquisa.'%');
              break;
            case 3:
              $sql = "SELECT * FROM tb_medico WHERE nome_medico LIKE ?" . $order_by . $desc;
              $stm = $this->pdo->prepare($sql);
              $stm->bindValue(1, '%'.$tag_pesquisa.'%');
              break;
            case 4:
              $sql = "SELECT * FROM tb_medico WHERE especialidade LIKE ?" . $order_by . $desc;
              $stm = $this->pdo->prepare($sql);
              $stm->bindValue(1, '%'.$tag_pesquisa.'%');
              break;
          }

          $stm->execute();
          $dados = $stm->fetchAll(PDO::FETCH_OBJ);
          return $dados;
        } catch(PDOException $erro){
          return $erro->errorInfo[1];
        }
      endif;
    else:
      try{
        $sql = "SELECT * FROM tb_medico" . $order_by . $desc;
        $stm = $this->pdo->prepare($sql);
        $stm->execute();
        $dados = $stm->fetchAll(PDO::FETCH_OBJ);
        return $dados;
      }catch(PDOException $erro){
        return $erro->errorInfo[1];
      }
    endif;
  }

  public function search_medico_by_id($id_criptografado){
    try{
      $sql = "SELECT * FROM tb_medico WHERE id_m_criptografado = ?";
      $stm = $this->pdo->prepare($sql);
      $stm->bindValue(1, $id_criptografado);
      $stm->execute();
      $dados = $stm->fetchAll(PDO::FETCH_OBJ);
      return $dados;
    }catch(PDOException $erro){
      return $erro->errorInfo[1];
    }
  }

  //Tabela discente

  public function insert_discente($nome, $periodo, $matricula){
    if (!empty($nome) && !empty($periodo) && !empty($matricula)):
      try{
        $sql = "INSERT INTO tb_discente (nome_discente, periodo_faculdade, num_matricula, id_ds_criptografado) VALUES (?, ?, ?, ?)";
        $stm = $this->pdo->prepare($sql);
        $stm->bindValue(1, $nome);
        $stm->bindValue(2, $periodo);
        $stm->bindValue(3, $matricula);
        $stm->bindValue(4, md5('id='.$this->get_auto_increment('tb_discente')[0]->auto_increment));
        $stm->execute();
      }catch(PDOException $erro){
        return $erro->errorInfo[1];
      }
    endif;
  }

  public function update_discente($nome, $periodo, $matricula, $id_criptografado){
    if (!empty($nome) && !empty($periodo) && !empty($matricula) && !empty($id_criptografado)):
      try{
        $this->set_matricula_usuario($matricula, $id_criptografado);
        $sql = "UPDATE tb_discente SET nome_discente=?, periodo_faculdade=?, num_matricula=? WHERE id_ds_criptografado=?";
        $stm = $this->pdo->prepare($sql);
        $stm->bindValue(1, $nome);
        $stm->bindValue(2, $periodo);
        $stm->bindValue(3, $matricula);
        $stm->bindValue(4, $id_criptografado);
        $stm->execute();
      }catch(PDOException $erro){
        return $erro->errorInfo[1];
      }
    endif;
  }

  public function get_crm_usuario($id_criptografado){
    if (!empty($id_criptografado)):
      try{
        $sql = "SELECT crm_medico FROM tb_medico WHERE id_m_criptografado=?";
        $stm = $this->pdo->prepare($sql);
        $stm->bindValue(1, $id_criptografado);
        $stm->execute();
        $dados = $stm->fetchAll(PDO::FETCH_OBJ);
        return $dados;
      }catch(PDOException $erro){
        return $erro->errorInfo[1];
      }
    endif;
  }

  public function get_matricula_usuario($id_criptografado){
    if (!empty($id_criptografado)):
      try{
        $sql = "SELECT num_matricula FROM tb_discente WHERE id_ds_criptografado=?";
        $stm = $this->pdo->prepare($sql);
        $stm->bindValue(1, $id_criptografado);
        $stm->execute();
        $dados = $stm->fetchAll(PDO::FETCH_OBJ);
        return $dados;
      }catch(PDOException $erro){
        return $erro->errorInfo[1];
      }
    endif;
  }

  public function get_nome_usuario($num_matricula){
    if (!empty($num_matricula)):
      try{
        $sql = "SELECT nome_discente FROM tb_discente WHERE num_matricula=?";
        $stm = $this->pdo->prepare($sql);
        $stm->bindValue(1, $num_matricula);
        $stm->execute();
        $dados = $stm->fetchAll(PDO::FETCH_OBJ);
        return $dados;
      }catch(PDOException $erro){
        return $erro->errorInfo[1];
      }
    endif;
  }

  public function get_nome_medico($crm_medico){
    if (!empty($crm_medico)):
      try{
        $sql = "SELECT nome_medico FROM tb_medico WHERE crm_medico=?";
        $stm = $this->pdo->prepare($sql);
        $stm->bindValue(1, $crm_medico);
        $stm->execute();
        $dados = $stm->fetchAll(PDO::FETCH_OBJ);
        return $dados;
      }catch(PDOException $erro){
        return $erro->errorInfo[1];
      }
    endif;
  }

  public function delete_discente($id_criptografado){
    if (!empty($id_criptografado)):
      $dados = $this->get_matricula_usuario($id_criptografado);
      try{
        $sql = "DELETE FROM tb_discente WHERE id_ds_criptografado=?";
        $stm = $this->pdo->prepare($sql);
        $stm->bindValue(1, $id_criptografado);
        $stm->execute();
        $this->delete_matricula_usuario($dados[0]->num_matricula);
      }catch(PDOException $erro){
        return $erro->errorInfo[1];
      }
    endif;
  }

  public function search_discente($tag_pesquisa, $select_pesquisa, $select_ordem, $check_desc){

    if(!empty($select_ordem)):
      $order_by = "";
      switch ($select_ordem) {
        case 1:
          $order_by = " ORDER BY num_matricula";
          break;
        case 2:
          $order_by = " ORDER BY nome_discente";
          break;
        case 3:
          $order_by = " ORDER BY periodo_faculdade";
          break;
      }
    endif;

    $desc = "";
    if($check_desc == 'true') $desc = " DESC;";

    if(!empty($tag_pesquisa)):
      if(!empty($select_pesquisa)):
        try{

          switch ($select_pesquisa) {
            case 1:
              $sql = "SELECT * FROM tb_discente WHERE nome_discente LIKE ? OR periodo_faculdade LIKE ? OR num_matricula LIKE ?" . $order_by . $desc;
              $stm = $this->pdo->prepare($sql);
              $stm->bindValue(1, '%'.$tag_pesquisa.'%');
              $stm->bindValue(2, '%'.$tag_pesquisa.'%');
              $stm->bindValue(3, '%'.$tag_pesquisa.'%');
              break;
            case 2:
              $sql = "SELECT * FROM tb_discente WHERE num_matricula LIKE ?" . $order_by . $desc;
              $stm = $this->pdo->prepare($sql);
              $stm->bindValue(1, '%'.$tag_pesquisa.'%');
              break;
            case 3:
              $sql = "SELECT * FROM tb_discente WHERE nome_discente LIKE ?" . $order_by . $desc;
              $stm = $this->pdo->prepare($sql);
              $stm->bindValue(1, '%'.$tag_pesquisa.'%');
              break;
            case 4:
              $sql = "SELECT * FROM tb_discente WHERE periodo_faculdade LIKE ?" . $order_by . $desc;
              $stm = $this->pdo->prepare($sql);
              $stm->bindValue(1, '%'.$tag_pesquisa.'%');
              break;
          }

          $stm->execute();
          $dados = $stm->fetchAll(PDO::FETCH_OBJ);
          return $dados;
        } catch(PDOException $erro){
          return $erro->errorInfo[1];
        }
      endif;
    else:
      try{
        $sql = "SELECT * FROM tb_discente" . $order_by . $desc;
        $stm = $this->pdo->prepare($sql);
        $stm->execute();
        $dados = $stm->fetchAll(PDO::FETCH_OBJ);
        return $dados;
      }catch(PDOException $erro){
        return $erro->errorInfo[1];
      }
    endif;
  }

  public function search_discente_by_id($id_criptografado){
    try{
      $sql = "SELECT * FROM tb_discente WHERE id_ds_criptografado = ?";
      $stm = $this->pdo->prepare($sql);
      $stm->bindValue(1, $id_criptografado);
      $stm->execute();
      $dados = $stm->fetchAll(PDO::FETCH_OBJ);
      return $dados;
    }catch(PDOException $erro){
      return $erro->errorInfo[1];
    }
  }

  //Tabela docente

  public function insert_docente($nome, $crefito){
    if (!empty($nome) && !empty($crefito)):
      try{
        $sql = "INSERT INTO tb_docente (nome_docente, crefito, id_d_criptografado) VALUES (?, ?, ?)";
        $stm = $this->pdo->prepare($sql);
        $stm->bindValue(1, $nome);
        $stm->bindValue(2, $crefito);
        $stm->bindValue(3, md5('id='.$this->get_auto_increment('tb_docente')[0]->auto_increment));
        $stm->execute();
      }catch(PDOException $erro){
        return $erro->errorInfo[1];
      }
    endif;
  }

  public function update_docente($nome, $crefito, $id_criptografado){
    if (!empty($nome) && !empty($crefito) && !empty($id_criptografado)):
      try{
        $sql = "UPDATE tb_docente SET nome_docente=?, crefito=? WHERE id_d_criptografado=?";
        $stm = $this->pdo->prepare($sql);
        $stm->bindValue(1, $nome);
        $stm->bindValue(2, $crefito);
        $stm->bindValue(3, $id_criptografado);
        $stm->execute();
      }catch(PDOException $erro){
        return $erro->errorInfo[1];
      }
    endif;
  }

  public function delete_docente($id_criptografado){
    if (!empty($id_criptografado)):
      try{
        $sql = "DELETE FROM tb_docente WHERE id_d_criptografado=?";
        $stm = $this->pdo->prepare($sql);
        $stm->bindValue(1, $id_criptografado);
        $stm->execute();
      }catch(PDOException $erro){
        return $erro->errorInfo[1];
      }
    endif;
  }

  public function search_docente($tag_pesquisa, $select_pesquisa, $select_ordem, $check_desc){

    if(!empty($select_ordem)):
      $order_by = "";
      switch ($select_ordem) {
        case 1:
          $order_by = " ORDER BY crefito";
          break;
        case 2:
          $order_by = " ORDER BY nome_docente";
          break;
      }
    endif;

    $desc = "";
    if($check_desc == 'true') $desc = " DESC;";

    if(!empty($tag_pesquisa)):
      if(!empty($select_pesquisa)):
        try{

          switch ($select_pesquisa) {
            case 1:
              $sql = "SELECT * FROM tb_docente WHERE nome_docente LIKE ? OR crefito LIKE ?" . $order_by . $desc;
              $stm = $this->pdo->prepare($sql);
              $stm->bindValue(1, '%'.$tag_pesquisa.'%');
              $stm->bindValue(2, '%'.$tag_pesquisa.'%');
              break;
            case 2:
              $sql = "SELECT * FROM tb_docente WHERE crefito LIKE ?" . $order_by . $desc;
              $stm = $this->pdo->prepare($sql);
              $stm->bindValue(1, '%'.$tag_pesquisa.'%');
              break;
            case 3:
              $sql = "SELECT * FROM tb_docente WHERE nome_docente LIKE ?" . $order_by . $desc;
              $stm = $this->pdo->prepare($sql);
              $stm->bindValue(1, '%'.$tag_pesquisa.'%');
              break;
          }

          $stm->execute();
          $dados = $stm->fetchAll(PDO::FETCH_OBJ);
          return $dados;
        } catch(PDOException $erro){
          return $erro->errorInfo[1];
        }
      endif;
    else:
      try{
        $sql = "SELECT * FROM tb_docente" . $order_by . $desc;
        $stm = $this->pdo->prepare($sql);
        $stm->execute();
        $dados = $stm->fetchAll(PDO::FETCH_OBJ);
        return $dados;
      }catch(PDOException $erro){
        return $erro->errorInfo[1];
      }
    endif;
  }

  public function search_docente_by_id($id_criptografado){
    try{
      $sql = "SELECT * FROM tb_docente WHERE id_d_criptografado = ?";
      $stm = $this->pdo->prepare($sql);
      $stm->bindValue(1, $id_criptografado);
      $stm->execute();
      $dados = $stm->fetchAll(PDO::FETCH_OBJ);
      return $dados;
    }catch(PDOException $erro){
      return $erro->errorInfo[1];
    }
  }

  //Tabela paciente

  public function insert_paciente($nome, $dt_nascimento, $sexo, $nome_mae, $naturalidade, $cep, $endereco, $numero, $complemento, $bairro, $cidade, $estado, $telefone1, $telefone2, $num_sus, $id_medico, $diagnostico){
    if (!empty($nome) && !empty($dt_nascimento) && !empty($sexo) && !empty($nome_mae) && !empty($naturalidade) && !empty($cep) && !empty($endereco) && !empty($numero) && !empty($bairro) && !empty($cidade) && !empty($estado) && !empty($telefone1) && !empty($num_sus) && !empty($id_medico) && !empty($diagnostico)):

      try{
        $sql = "INSERT INTO tb_paciente (nome_paciente, dt_nascimento, sexo, nome_mae, naturalidade, cep, endereco, numero, complemento, bairro, cidade, estado, telefone_1, telefone_2, num_sus, id_medico, diagnostico, id_criptografado) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        $stm = $this->pdo->prepare($sql);
        $stm->bindValue(1, $nome);
        $stm->bindValue(2, $dt_nascimento);
        $stm->bindValue(3, $sexo);
        $stm->bindValue(4, $nome_mae);
        $stm->bindValue(5, $naturalidade);
        $stm->bindValue(6, $cep);
        $stm->bindValue(7, $endereco);
        $stm->bindValue(8, $numero);
        $stm->bindValue(9, $complemento);
        $stm->bindValue(10, $bairro);
        $stm->bindValue(11, $cidade);
        $stm->bindValue(12, $estado);
        $stm->bindValue(13, $telefone1);
        $stm->bindValue(14, $telefone2);
        $stm->bindValue(15, $num_sus);
        $stm->bindValue(16, $id_medico);
        $stm->bindValue(17, $diagnostico);
        $stm->bindValue(18, md5('id='.$this->get_auto_increment('tb_paciente')[0]->auto_increment));
        $stm->execute();
      }catch(PDOException $erro){
        return $erro->errorInfo[1];
      }
    endif;
  }

  public function update_paciente($nome, $dt_nascimento, $sexo, $nome_mae, $naturalidade, $cep, $endereco, $numero, $complemento, $bairro, $cidade, $estado, $telefone1, $telefone2, $num_sus, $id_medico, $diagnostico, $id_criptografado){
    if (!empty($nome) && !empty($dt_nascimento) && !empty($sexo) && !empty($nome_mae) && !empty($naturalidade) && !empty($cep) && !empty($endereco) && !empty($numero) && !empty($bairro) && !empty($cidade) && !empty($estado) && !empty($telefone1) && !empty($num_sus) && !empty($id_medico) && !empty($diagnostico) && !empty($id_criptografado)):
      try{
        $sql = "UPDATE tb_paciente SET nome_paciente=?, dt_nascimento=?, sexo=?, nome_mae=?, naturalidade=?, cep=?, endereco=?, numero=?, complemento=?, bairro=?, cidade=?, estado=?, telefone_1=?, telefone_2=?, num_sus=?, id_medico=?, diagnostico=? WHERE id_criptografado=?";
        $stm = $this->pdo->prepare($sql);
        $stm->bindValue(1, $nome);
        $stm->bindValue(2, $dt_nascimento);
        $stm->bindValue(3, $sexo);
        $stm->bindValue(4, $nome_mae);
        $stm->bindValue(5, $naturalidade);
        $stm->bindValue(6, $cep);
        $stm->bindValue(7, $endereco);
        $stm->bindValue(8, $numero);
        $stm->bindValue(9, $complemento);
        $stm->bindValue(10, $bairro);
        $stm->bindValue(11, $cidade);
        $stm->bindValue(12, $estado);
        $stm->bindValue(13, $telefone1);
        $stm->bindValue(14, $telefone2);
        $stm->bindValue(15, $num_sus);
        $stm->bindValue(16, $id_medico);
        $stm->bindValue(17, $diagnostico);
        $stm->bindValue(18, $id_criptografado);
        $stm->execute();
      }catch(PDOException $erro){
        return $erro->errorInfo[1];
      }
    endif;
  }

  public function delete_paciente($id_criptografado){
    if (!empty($id_criptografado)):
      try{
        $sql = "DELETE FROM tb_paciente WHERE id_criptografado=?";
        $stm = $this->pdo->prepare($sql);
        $stm->bindValue(1, $id_criptografado);
        $stm->execute();
      }catch(PDOException $erro){
        return $erro->errorInfo[1];
      }
    endif;
  }

  public function search_paciente($tag_pesquisa, $select_pesquisa, $select_ordem, $check_desc){

    $desc = "";
    if($check_desc == 'true') $desc = " DESC";

    if(!empty($select_ordem)):
      $order_by = "";
      switch ($select_ordem) {
        case 1:
          $order_by = " ORDER BY P.nome_paciente" . $desc . ";";
          break;
        case 2:
          $order_by = " ORDER BY P.dt_nascimento" . $desc . ";";
          break;
        case 3:
          $order_by = " ORDER BY P.num_sus" . $desc . ";";
          break;
        case 4:
          $order_by = " ORDER BY M.nome_medico" . $desc . ";";
          break;
        case 5:
          $order_by = " ORDER BY P.endereco" . $desc . ", P.numero" . $desc . ", P.complemento" . $desc . ", P.bairro" . $desc . ", P.cidade" . $desc . ", P.estado" . $desc . ";";
          break;
      }
    endif;

    $sql_1 = "SELECT * FROM tb_paciente AS P INNER JOIN tb_medico AS M ON P.id_medico = M.id_m_criptografado";

    if(!empty($tag_pesquisa)):
      if(!empty($select_pesquisa)):
        try{

          switch ($select_pesquisa) {
            case 10:
              $sql = $sql_1. " WHERE P.nome_paciente LIKE ? OR P.num_sus LIKE ? OR M.nome_medico LIKE ?" . $order_by;
              $stm = $this->pdo->prepare($sql);
              $stm->bindValue(1, '%'.$tag_pesquisa.'%');
              $stm->bindValue(2, '%'.$tag_pesquisa.'%');
              $stm->bindValue(3, '%'.$tag_pesquisa.'%');
              break;
            case 1:
              $sql = $sql_1. " WHERE P.nome_paciente LIKE ? OR DATE_FORMAT(P.dt_nascimento,'%d/%m/%Y') LIKE ? OR (CASE WHEN P.sexo = 'M' THEN 'masculino' WHEN P.sexo = 'F' THEN 'feminino' END) LIKE ? OR P.nome_mae LIKE ? OR P.naturalidade LIKE ? OR P.num_sus LIKE ? OR M.nome_medico LIKE ? OR M.especialidade LIKE ? OR P.diagnostico LIKE ? OR (CASE WHEN P.complemento = '' THEN CONCAT(P.endereco,', ',P.numero,' - ',P.bairro,', ',P.cidade,' - ',P.estado,', ',P.cep) ELSE CONCAT(P.endereco,', ',P.numero,', ',P.complemento,' - ',P.bairro,', ',P.cidade,' - ',P.estado,', ',P.cep) END) LIKE ? OR (CASE WHEN P.telefone_2 = '' THEN P.telefone_1 ELSE CONCAT(P.telefone_1,' | ',P.telefone_2) END) LIKE ?" . $order_by;
              $stm = $this->pdo->prepare($sql);
              $stm->bindValue(1, '%'.$tag_pesquisa.'%');
              $stm->bindValue(2, '%'.$tag_pesquisa.'%');
              $stm->bindValue(3, '%'.$tag_pesquisa.'%');
              $stm->bindValue(4, '%'.$tag_pesquisa.'%');
              $stm->bindValue(5, '%'.$tag_pesquisa.'%');
              $stm->bindValue(6, '%'.$tag_pesquisa.'%');
              $stm->bindValue(7, '%'.$tag_pesquisa.'%');
              $stm->bindValue(8, '%'.$tag_pesquisa.'%');
              $stm->bindValue(9, '%'.$tag_pesquisa.'%');
              $stm->bindValue(10, '%'.$tag_pesquisa.'%');
              $stm->bindValue(11, '%'.$tag_pesquisa.'%');
              break;
            case 2:
              $sql = $sql_1. " WHERE P.nome_paciente LIKE ?" . $order_by;
              $stm = $this->pdo->prepare($sql);
              $stm->bindValue(1, '%'.$tag_pesquisa.'%');
              break;
            case 3:
              $sql = $sql_1. " WHERE DATE_FORMAT(P.dt_nascimento,'%d/%m/%Y') LIKE ?" . $order_by;
              $stm = $this->pdo->prepare($sql);
              $stm->bindValue(1, '%'.$tag_pesquisa.'%');
              break;
            case 4:
              $sql = $sql_1. " WHERE P.num_sus LIKE ?" . $order_by;
              $stm = $this->pdo->prepare($sql);
              $stm->bindValue(1, '%'.$tag_pesquisa.'%');
              break;
            case 5:
              $sql = $sql_1. " WHERE M.nome_medico LIKE ?" . $order_by;
              $stm = $this->pdo->prepare($sql);
              $stm->bindValue(1, '%'.$tag_pesquisa.'%');
              break;
            case 6:
              $sql = $sql_1. " WHERE (CASE WHEN P.complemento = '' THEN CONCAT(P.endereco,', ',P.numero,' - ',P.bairro,', ',P.cidade,' - ',P.estado,', ',P.cep) ELSE CONCAT(P.endereco,', ',P.numero,', ',P.complemento,' - ',P.bairro,', ',P.cidade,' - ',P.estado,', ',P.cep) END) LIKE ?" . $order_by;
              $stm = $this->pdo->prepare($sql);
              $stm->bindValue(1, '%'.$tag_pesquisa.'%');
              break;
          }

          $stm->execute();
          $dados = $stm->fetchAll(PDO::FETCH_OBJ);
          return $dados;
        } catch(PDOException $erro){
          return $erro->errorInfo[1];
        }
      endif;
    else:
      try{
        $sql = $sql_1 . $order_by;
        $stm = $this->pdo->prepare($sql);
        $stm->execute();
        $dados = $stm->fetchAll(PDO::FETCH_OBJ);
        return $dados;
      }catch(PDOException $erro){
        return $erro->errorInfo[1];
      }
    endif;
  }

  public function search_paciente_by_id($id_criptografado){
    try{
      $sql = "SELECT * FROM tb_paciente AS P INNER JOIN tb_medico AS M ON P.id_medico = M.id_m_criptografado WHERE P.id_criptografado = ?";
      $stm = $this->pdo->prepare($sql);
      $stm->bindValue(1, $id_criptografado);
      $stm->execute();
      $dados = $stm->fetchAll(PDO::FETCH_OBJ);
      return $dados;
    }catch(PDOException $erro){
      return $erro->errorInfo[1];
    }
  }

  //Tabela prontuário

  public function insert_prontuario($id_paciente, $id_docente, $id_discente, $data, $relatorio, $observacao){
    if (!empty($id_paciente) && !empty($id_docente) && !empty($id_discente) && !empty($data) && !empty($relatorio)):
      try{
        $sql = "INSERT INTO tb_prontuario (id_paciente, id_docente, id_discente, data_prontuario, relatorio, observacao, id_p_criptografado) VALUES (?, ?, ?, ?, ?, ?, ?)";
        $stm = $this->pdo->prepare($sql);
        $stm->bindValue(1, $id_paciente);
        $stm->bindValue(2, $id_docente);
        $stm->bindValue(3, $id_discente);
        $stm->bindValue(4, $data);
        $stm->bindValue(5, $relatorio);
        $stm->bindValue(6, $observacao);
        $stm->bindValue(7, md5('id='.$this->get_auto_increment('tb_prontuario')[0]->auto_increment));
        $stm->execute();
        header('Location: ../prontuario/cadastro.php#success=true&id='.$id_paciente);
      }catch(PDOException $erro){
        return $erro->errorInfo[1];
      }
    endif;
  }

  public function update_prontuario($id_paciente, $id_docente, $id_discente, $data, $relatorio, $observacao, $id_criptografado){
    if (!empty($id_paciente) && !empty($id_docente) && !empty($id_discente) && !empty($data) && !empty($relatorio) && !empty($id_criptografado)):
      try{
        $sql = "UPDATE tb_prontuario SET id_paciente=?, id_docente=?, id_discente=?, data_prontuario=?, relatorio=?, observacao=? WHERE id_p_criptografado=?";
        $stm = $this->pdo->prepare($sql);
        $stm->bindValue(1, $id_paciente);
        $stm->bindValue(2, $id_docente);
        $stm->bindValue(3, $id_discente);
        $stm->bindValue(4, $data);
        $stm->bindValue(5, $relatorio);
        $stm->bindValue(6, $observacao);
        $stm->bindValue(7, $id_criptografado);
        $stm->execute();
        header('Location: ../prontuario/cadastro.php#succeed=true&id='.$id_paciente);
      }catch(PDOException $erro){
        return $erro->errorInfo[1];
      }
    endif;
  }

  public function delete_prontuario($id_criptografado){
    if (!empty($id_criptografado)):
      try{
        $sql = "DELETE FROM tb_prontuario WHERE id_p_criptografado=?";
        $stm = $this->pdo->prepare($sql);
        $stm->bindValue(1, $id_criptografado);
        $stm->execute();
      }catch(PDOException $erro){
        return $erro->errorInfo[1];
      }
    endif;
  }

  public function search_prontuario($tag_pesquisa, $select_pesquisa, $select_ordem, $check_desc, $id_paciente_criptografado){

    $order_by = "";
    if(!empty($select_ordem)):
      switch ($select_ordem) {
        case 1:
          $order_by = " ORDER BY D.nome_docente";
          break;
        case 2:
          $order_by = " ORDER BY DC.nome_discente";
          break;
        case 3:
          $order_by = " ORDER BY P.data_prontuario";
          break;
        case 4:
          $order_by = " ORDER BY P.relatorio";
          break;
      }
    endif;

    $desc = "";
    if($check_desc == 'true') $desc = " DESC;";

    $sql_1 = "SELECT * FROM tb_prontuario AS P INNER JOIN tb_paciente AS PC ON P.id_paciente = PC.id_criptografado INNER JOIN tb_docente AS D ON P.id_docente = D.id_d_criptografado INNER JOIN tb_discente AS DC ON P.id_discente = DC.id_ds_criptografado";

    if(!empty($tag_pesquisa)):
      if(!empty($select_pesquisa)):
        try{

          switch ($select_pesquisa) {
            case 1:
              $sql = $sql_1. " WHERE PC.id_criptografado=? AND (D.nome_docente LIKE ? OR DC.nome_discente LIKE ? OR DATE_FORMAT(P.data_prontuario,'%d/%m/%Y %H:%i') LIKE ? OR P.relatorio LIKE ?)" . $order_by . $desc;
              $stm = $this->pdo->prepare($sql);
              $stm->bindValue(1, $id_paciente_criptografado);
              $stm->bindValue(2, '%'.$tag_pesquisa.'%');
              $stm->bindValue(3, '%'.$tag_pesquisa.'%');
              $stm->bindValue(4, '%'.$tag_pesquisa.'%');
              $stm->bindValue(5, '%'.$tag_pesquisa.'%');
              break;
            case 2:
              $sql = $sql_1. " WHERE PC.id_criptografado=? AND DATE_FORMAT(P.data_prontuario,'%d/%m/%Y %H:%i') LIKE ?" . $order_by . $desc;
              $stm = $this->pdo->prepare($sql);
              $stm->bindValue(1, $id_paciente_criptografado);
              $stm->bindValue(2, '%'.$tag_pesquisa.'%');
              break;
            case 3:
              $sql = $sql_1. " WHERE PC.id_criptografado=? AND D.nome_docente LIKE ?" . $order_by . $desc;
              $stm = $this->pdo->prepare($sql);
              $stm->bindValue(1, $id_paciente_criptografado);
              $stm->bindValue(2, '%'.$tag_pesquisa.'%');
              break;
            case 4:
              $sql = $sql_1. " WHERE PC.id_criptografado=? AND DC.nome_discente LIKE ?" . $order_by . $desc;
              $stm = $this->pdo->prepare($sql);
              $stm->bindValue(1, $id_paciente_criptografado);
              $stm->bindValue(2, '%'.$tag_pesquisa.'%');
              break;
            case 5:
              $sql = $sql_1. " WHERE PC.id_criptografado=? AND P.relatorio LIKE ?" . $order_by . $desc;
              $stm = $this->pdo->prepare($sql);
              $stm->bindValue(1, $id_paciente_criptografado);
              $stm->bindValue(2, '%'.$tag_pesquisa.'%');
              break;
          }

          $stm->execute();
          $dados = $stm->fetchAll(PDO::FETCH_OBJ);
          return $dados;
        } catch(PDOException $erro){
          return $erro->errorInfo[1];
        }
      endif;
    else:
      try{
        $sql = $sql_1 . " WHERE PC.id_criptografado=?" . $order_by . $desc;
        $stm = $this->pdo->prepare($sql);
        $stm->bindValue(1, $id_paciente_criptografado);
        $stm->execute();
        $dados = $stm->fetchAll(PDO::FETCH_OBJ);
        return $dados;
      }catch(PDOException $erro){
        return $erro->errorInfo[1];
      }
    endif;
  }

  public function search_prontuario_by_id($id_criptografado){
    try{
      $sql = "SELECT * FROM tb_prontuario AS P INNER JOIN tb_paciente AS PC ON P.id_paciente = PC.id_criptografado INNER JOIN tb_medico AS M ON PC.id_medico = M.id_m_criptografado INNER JOIN tb_docente AS D ON P.id_docente = D.id_d_criptografado INNER JOIN tb_discente AS DC ON P.id_discente = DC.id_ds_criptografado WHERE P.id_p_criptografado = ?";
      $stm = $this->pdo->prepare($sql);
      $stm->bindValue(1, $id_criptografado);
      $stm->execute();
      $dados = $stm->fetchAll(PDO::FETCH_OBJ);
      return $dados;
    }catch(PDOException $erro){
      return $erro->errorInfo[1];
    }
  }

  public function search_prontuario_completo($tag_pesquisa, $select_pesquisa, $select_ordem, $check_desc){

    $order_by = "";
    if(!empty($select_ordem)):
      switch ($select_ordem) {
        case 1:
          $order_by = " ORDER BY PC.nome_paciente";
          break;
        case 2:
          $order_by = " ORDER BY M.nome_medico";
          break;
        case 3:
          $order_by = " ORDER BY D.nome_docente";
          break;
        case 4:
          $order_by = " ORDER BY DC.nome_discente";
          break;
        case 5:
          $order_by = " ORDER BY P.data_prontuario";
          break;
      }
    endif;

    $desc = "";
    if($check_desc == 'true') $desc = " DESC;";

    $sql_1 = "SELECT * FROM tb_prontuario AS P INNER JOIN tb_paciente AS PC ON P.id_paciente = PC.id_criptografado INNER JOIN tb_docente AS D ON P.id_docente = D.id_d_criptografado INNER JOIN tb_discente AS DC ON P.id_discente = DC.id_ds_criptografado INNER JOIN tb_medico AS M ON PC.id_medico = M.id_m_criptografado";

    if(!empty($tag_pesquisa)):
      if(!empty($select_pesquisa)):
        try{

          switch ($select_pesquisa) {
            case 1:
              $sql = $sql_1. " WHERE PC.nome_paciente LIKE ? OR M.nome_medico LIKE ? OR D.nome_docente LIKE ? OR DC.nome_discente LIKE ? OR DATE_FORMAT(P.data_prontuario,'%d/%m/%Y %H:%i') LIKE ?" . $order_by . $desc;
              $stm = $this->pdo->prepare($sql);
              $stm->bindValue(1, '%'.$tag_pesquisa.'%');
              $stm->bindValue(2, '%'.$tag_pesquisa.'%');
              $stm->bindValue(3, '%'.$tag_pesquisa.'%');
              $stm->bindValue(4, '%'.$tag_pesquisa.'%');
              $stm->bindValue(5, '%'.$tag_pesquisa.'%');
              break;
            case 2:
              $sql = $sql_1. " WHERE DATE_FORMAT(P.data_prontuario,'%d/%m/%Y %H:%i') LIKE ?" . $order_by . $desc;
              $stm = $this->pdo->prepare($sql);
              $stm->bindValue(1, '%'.$tag_pesquisa.'%');
              break;
            case 3:
              $sql = $sql_1. " WHERE PC.nome_paciente LIKE ?" . $order_by . $desc;
              $stm = $this->pdo->prepare($sql);
              $stm->bindValue(1, '%'.$tag_pesquisa.'%');
              break;
            case 4:
              $sql = $sql_1. " WHERE M.nome_medico LIKE ?" . $order_by . $desc;
              $stm = $this->pdo->prepare($sql);
              $stm->bindValue(1, '%'.$tag_pesquisa.'%');
              break;
            case 5:
              $sql = $sql_1. " WHERE D.nome_docente LIKE ?" . $order_by . $desc;
              $stm = $this->pdo->prepare($sql);
              $stm->bindValue(1, '%'.$tag_pesquisa.'%');
              break;
            case 6:
              $sql = $sql_1. " WHERE DC.nome_discente LIKE ?" . $order_by . $desc;
              $stm = $this->pdo->prepare($sql);
              $stm->bindValue(1, '%'.$tag_pesquisa.'%');
              break;
          }

          $stm->execute();
          $dados = $stm->fetchAll(PDO::FETCH_OBJ);
          return $dados;
        } catch(PDOException $erro){
          return $erro->errorInfo[1];
        }
      endif;
    else:
      try{
        $sql = $sql_1 . $order_by . $desc;
        $stm = $this->pdo->prepare($sql);
        $stm->execute();
        $dados = $stm->fetchAll(PDO::FETCH_OBJ);
        return $dados;
      }catch(PDOException $erro){
        return $erro->errorInfo[1];
      }
    endif;
  }

}
