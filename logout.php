<?php

if(!isset($_SESSION)) {
    session_start();
}

if(isset($_POST['logout'])):

    $timeout = isset($_POST['timeout']) ? $_POST['timeout'] : false;

    require_once "conexao_bd/conexao_db_syspront.php";
    require_once "conexao_bd/db_syspront.class.php";

    $db_syspront = sysPront::getInstance(Conexao::getInstance());

    $db_syspront->gravar('', 'Acao: logout; Usuario: ' . $_SESSION['usuario'] . '; ');
    //Variáveis de login do usuario
    unset($_SESSION['id_usuario']);
    unset($_SESSION['usuario']);
    unset($_SESSION['tipo_usuario']);
    unset($_SESSION['nome_do_usuario']);

    //Variáveis de controle das tentativas
    unset($_SESSION['num_tentativas']);
    unset($_SESSION['data_entrada']);
    unset($_SESSION['primeiro_acesso']);
    unset($_SESSION['ultimo_acesso']);

    //Variáveis de controle de inatividadde
    unset($_SESSION['timeout']);
    unset($_SESSION['session_life']);

    if($timeout)
        echo 'index.php#timeout=true';
    else
        echo 'index.php#logout=true';

else:
    if(!isset($_SESSION['usuario'])) header('Location: index.php#erro=1');
    else header('Location: prontuario/cadastro.php');
endif;

?>