<?php

  if(!isset($_SESSION)) {
    session_start();
  }

  if(isset($_POST['id_paciente'])):

    require_once "../conexao_bd/conexao_db_syspront.php";
    require_once "../conexao_bd/db_syspront.class.php";

    $db_syspront = sysPront::getInstance(Conexao::getInstance());

    $id_paciente = $_POST['id_paciente'];

    $erro = $db_syspront->delete_paciente($id_paciente);

    if($erro == 1451){

      echo '<div class="modal fade" id="modal-erro">';
        echo '<div class="modal-dialog">';
          echo '<div class="modal-content" style="border: 3px solid #FA5B5B;">';
            echo '<div class="modal-header" style="text-align: center;">';
              echo '<button type="button" class="close" data-dismiss="modal"><span>×</span></button>';
              echo '<h4 class="modal-title" style="color: red;">ERRO AO REMOVER PACIENTE</h4>';
            echo '</div>';
            echo '<div class="modal-body" style="text-align: center; font-weight: bold;">';
              echo 'Não é possível remover este paciente enquanto ele estiver cadastrado em um ou mais prontuários! Primeiramente remova ou edite estes prontuários relacionados ao paciente, e então será possível excluir este registro!';
            echo '</div>';
            echo '<div class="modal-footer">';
              echo '<button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>';
            echo '</div>';
          echo '</div>';
        echo '</div>';
      echo '</div>';

    } else {
      echo 'no_error';
      $db_syspront->gravar('../', 'Acao: exclusao_paciente; Usuario: ' . $_SESSION['usuario'] . '; ');
    }

  else:
    if(!isset($_SESSION['usuario'])) header('Location: ../index.php#erro=1');
    else header('Location: consulta.php');

  endif;