<?php

    if(!isset($_SESSION)) {
        session_start();
    }

    if(!isset($_SESSION['usuario']) || $_SESSION['tipo_usuario'] != 'Administrador'){
        header('Location: ../index.php#erro=1');
    }

?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
 <meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="shortcut icon" href="../img/favicon.ico">
 <title>Projeto SysPront Fisioterapia - Listagem de pacientes</title>

 <script type="text/javascript" src="../js/jquery.min.js"></script>
 <link href="../css/bootstrap.min.css" rel="stylesheet">
 <link rel="stylesheet" type="text/css" href="../css/sys_pront_custom.css">

 <script type="text/javascript">

    $(document).ready(function(){

/*#################################################################################################*/
        //Controle do timeout
        var intervalo = '';

        contagem_timeout();
        intervalo = setTimeout(contagem_timeout, 1801000);

        function contagem_timeout(){
            $.ajax({
                url: '../sessao_timeout.php',
                method: 'post',
                data: { sessao: true },
                success: function(data){
                    if(data == 'logout'){
                        $.ajax({
                            url: '../logout.php',
                            method: 'post',
                            data: { logout: true,
                                    timeout: true },
                            success: function(data){
                                window.location.href = '../' + data;
                            }
                        });
                    }
                }
            });
        }

        $(document).off('mousedown').on('mousedown', function(){
            clearTimeout(intervalo);
            intervalo = setTimeout(contagem_timeout, 1801000);
        });

        $(document).off('mousemove').on('mousemove', function(){
            clearTimeout(intervalo);
            intervalo = setTimeout(contagem_timeout, 1801000);
        });

        $(document).off('keydown').on('keydown', function(){
            clearTimeout(intervalo);
            intervalo = setTimeout(contagem_timeout, 1801000);
        });

/*#################################################################################################*/
        //Controle da sidebar de acordo com o tamanho da tela
        //Ao redimensionar a tela, certifica-se de que a sidebar esteja na posição correta
        $(window).resize(function(){

            if($(window).width() >= 992){
                $("#sidebar-wrapper").css('width', 220);
                $("#page-content-wrapper").css('padding-left', 10);
                $("#page-content-wrapper").css('margin-left', 230);
            } else {
                $("#sidebar-wrapper").css('width', 0);
                $("#page-content-wrapper").css('padding-left', 15);
                $("#page-content-wrapper").css('margin-left', 0);
            }

        });

        //Ao clicar, mostra a sidebar
        $("#mostra-menu").click(function(e) {
            e.preventDefault();
            $("#sidebar-wrapper").css('width', 220);
            $("#page-content-wrapper").css('padding-left', 10);
            $("#page-content-wrapper").css('margin-left', 0);
        });

        //Ao clicar, fecha a sidebar
        $("#fecha-menu").click(function(e) {
            e.preventDefault();
            $("#sidebar-wrapper").css('width', 0);
            $("#page-content-wrapper").css('padding-left', 15);
            $("#page-content-wrapper").css('margin-left', 0);
        });

/*#################################################################################################*/

        $('#page-content-wrapper').fadeIn(500);
        $('#tag_pesquisa').focus();

        //Ao iniciar traz os dados no banco em forma de tabela
        busca_dados_banco();

        var hash_url = $(location).attr('hash');

        if(hash_url == '#success=true'){

            $('#modal-sucesso').modal();
            $('#page-content-wrapper').hide();

        }

        $('#modal-sucesso').on('hidden.bs.modal', function (e) {
            window.location.replace('consulta.php');
        });

        $(document).off('keydown').keydown(function(event){
            var keycode = (event.keyCode ? event.keyCode : event.which);

            //Tecla esc
            if(keycode == '27'){
                $('#modal-sucesso').modal('hide');
                $('#modal-remocao').modal('hide');
                $('#modal-remover').modal('hide');
                $('#modal-erro').modal('hide');
            }
        });

        //Verifica se o checkbox decrescente está ativo ou não
        var check_desc = false;

        $('#check_desc').off('click').click(function(){

            if ($('#check_desc').is(":checked"))
            {
                check_desc = true;
            } else {
                check_desc = false;
            }

            busca_dados_banco();

        });

        //Ao preencher a caixa de pesquisa, busca as informações no banco de dados
        $('#tag_pesquisa').off('input').on('input',function(e){

            busca_dados_banco();

        });

        //Ao mudar o select da pesquisa, busca as informações no banco de dados
        $('#select_pesquisa').off('change').on('change',function(e){

            busca_dados_banco();

        });

        //Ao mudar o select da ordem, busca as informações no banco de dados
        $('#select_ordem').off('change').on('change',function(e){

            busca_dados_banco();

        });

        $('a').off('click').click(function(){
            if($(this).attr('id') == 'btn_logout'){
                $.ajax({
                    url: '../logout.php',
                    method: 'post',
                    data: { logout: true },
                    success: function(data){
                        window.location.href = '../' + data;
                    }
                });
            } else {
                event.preventDefault();
                linkLocation = this.href;
                $('#page-content-wrapper').fadeOut(200, function(){
                    window.location.href = linkLocation;
                });
            }
        });

        //Busca os dados de pesquisa no banco de dados
        function busca_dados_banco(){

            $.ajax({
                url: 'bd_consulta.php',
                method: 'post',
                //data: $('#form_tweet').serialize(),
                data: { tag_pesquisa: $('#tag_pesquisa').val(),
                    select_pesquisa: $('#select_pesquisa').val(),
                    select_ordem: $('#select_ordem').val(),
                    check_desc: check_desc },
                success: function(data){
                    $('#registros').html(data);
                    btns_tabela();

                }
            });

        }

        //Faz com que os botoes gerados na tabela funcionem corretamente
        function btns_tabela (){

            var id_paciente = 0;

            $('.btn_remover').off('click').click(function(){

                id_paciente = $(this).data('id_paciente');

            });

            $('.btn_editar').off('click').click(function(){

                id_paciente = $(this).data('id_paciente');

                $('#page-content-wrapper').fadeOut(200, function(){
                    window.location.replace('alteracao.php?value='+id_paciente);
                });

            });

            $('.btn_detalhes').off('click').click(function(){

                id_paciente = $(this).data('id_paciente');

                $.ajax({
                    url: 'bd_detalhes.php',
                    method: 'post',
                    data: { id_paciente: id_paciente },
                    success: function(data){
                        $('#modal_detalhes').html(data);
                        $('#modal-mensagem').modal();
                        busca_dados_banco();
                    }
                });

            });

            $('#remove_paciente').off('click').click(function(){

                $.ajax({
                    url: 'bd_remocao.php',
                    method: 'post',
                    data: { id_paciente: id_paciente },
                    success: function(data){
                        $('#modal-mensagem').modal('hide');
                        $('#modal-remover').modal('hide');

                        if(data == 'no_error'){
                            $('#modal-remocao').modal();
                        } else {
                            $('#div_erro_exclusao').html(data);
                            $('#modal-erro').modal();
                        }
                        busca_dados_banco();
                    }
                });

            });
        }

    });

 </script>

</head>
<body>

    <!-- Barra de navegação -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button id="btn_menu" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar_menu" aria-expanded="false" aria-controls="navbar" title="Menu">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div id="navbar" style="padding-left: 5px; padding-top: 2px;">
                    <h4>SISTEMA DE CONTROLE DE PRONTUÁRIO DE ESTAGIÁRIOS DO CURSO DE FISIOTERAPIA</h4>
                </div>
            </div>
            <div id="navbar" class="navbar-collapse collapse navbar_menu" style="padding-right: 5px;">
                <ul class="nav navbar-nav navbar-right">
                    <li><h4 style="color: white; text-shadow: 2px 2px #000; margin-right: 20px; margin-top: 15px; padding-left: 5px;">Bem vindo, <?=$_SESSION['nome_do_usuario']?></h4></li>
                    <li><a href="../prontuario/cadastro.php" title="Início">
                        <span class="glyphicon glyphicon-home"></span>
                    </a></li>
                    <li><a href="../perfil/consulta.php" title="Gerenciar perfis">
                        <span class="glyphicon glyphicon-user"></span>
                    </a></li>
                    <li><a style="cursor: pointer;" id="btn_logout" title="Sair">
                        <span class="glyphicon glyphicon-log-out"></span>
                    </a></li>
                </ul>
            </div>
        </div>
    </nav> <!-- navbar -->

    <div class="container-fluid">
        <div>
            <button id="mostra-menu" type="button" title="Mostrar menu">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </button>

            <!-- Sidebar -->
            <div id="sidebar-wrapper">
                <ul class="sidebar-nav">
                    <button id="fecha-menu" title="Fechar" type="button" class="close" aria-label="Close" style="background: white; margin-right: 20px; padding: 0px 3px;">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <li class="topicos"> <strong style="font-size: 17px">CADASTROS</strong> </li>
                    <li>
                        <a href="../medico/cadastro.php"><strong>MÉDICO</strong></a>
                    </li>
                    <li>
                        <a href="../docente/cadastro.php"><strong>DOCENTE</strong></a>
                    </li>
                    <li>
                        <a href="cadastro.php"><strong>PACIENTE</strong></a>
                    </li>
                    <li>
                        <a href="../discente/cadastro.php"><strong>DISCENTE</strong></a>
                    </li>
                    <li class="topicos"> <strong style="font-size: 17px">MOVIMENTOS</strong> </li>
                    <li>
                        <a href="../prontuario/cadastro.php"><strong>PRONTUÁRIO</strong></a>
                    </li>
                    <li class="topicos"> <strong style="font-size: 17px">CONSULTAS</strong> </li>
                    <li>
                        <a href="../prontuario/consulta.php"><strong>PRONTUÁRIO</strong></a>
                    </li>
                    <li>
                        <a href="../medico/consulta.php"><strong>MÉDICO</strong></a>
                    </li>
                    <li>
                        <a href="../docente/consulta.php"><strong>DOCENTE</strong></a>
                    </li>
                    <li class="selected">
                        <a href="consulta.php"><strong>PACIENTE</strong></a>
                    </li>
                    <li>
                        <a href="../discente/consulta.php"><strong>DISCENTE</strong></a>
                    </li>
                </ul>
            </div> <!-- /#sidebar-wrapper -->

        </div>

        <!-- Modal detalhes -->
        <div id="modal_detalhes"></div>

        <!-- Modal editado com sucesso -->
        <div class="modal fade" id="modal-sucesso">
            <div class="modal-dialog">
                <div class="modal-content" style="border: 3px solid black">
                    <div class="modal-header" style="text-align: center;">
                        <button type="button" class="close" data-dismiss="modal"><span style="color: red">×</span></button>
                        <h4 class="modal-title" style="color: #01C325;">Registro atualizado com sucesso!</h4>
                    </div>
                </div>
            </div>
        </div> <!--Modal-->

        <!-- Modal removido com sucesso -->
        <div class="modal fade" id="modal-remocao">
            <div class="modal-dialog">
                <div class="modal-content" style="border: 3px solid black">
                    <div class="modal-header" style="text-align: center;">
                        <button type="button" class="close" data-dismiss="modal"><span style="color: red">×</span></button>
                        <h4 class="modal-title" style="color: #01C325;">Registro removido com sucesso!</h4>
                    </div>
                </div>
            </div>
        </div> <!--Modal-->

        <!-- Modal remover paciente -->
        <div class="modal fade" id="modal-remover">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header" style="text-align: center;">
                        <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                        <h4 class="modal-title">Remover paciente</h4>
                    </div>
                    <div class="modal-body">
                      Deseja realmente remover este paciente?
                    </div>
                    <div class="modal-footer">
                        <button id="remove_paciente" type="button" class="btn btn-primary">Sim</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Não</button>
                    </div>
                </div>
            </div>
        </div> <!--Modal-->

        <!-- Modal erro de exclusao -->
        <div id="div_erro_exclusao"></div>

        <div id="page-content-wrapper" style="display: none;">

            <h3 class="page-header">Pacientes Cadastrados
            <a href="cadastro.php" style="float: right;" class="btn btn-primary btn-novo" title="Adicionar novo paciente">
                <span class="glyphicon glyphicon-plus"></span>
            </a></h3>

            <form method="get" class="form-group row">
                <div class="col-lg-7 coluna-pesquisa">
                    <div class="input-group">
                        <input type="text" id="tag_pesquisa" name="tag_pesquisa" class="form-control" placeholder="Pesquise aqui..." maxlength="160">
                        <span class="input-group-btn">
                            <select id="select_pesquisa" class="btn btn-default">
                              <option value="1" selected>Geral</option>
                              <option value="2">Nome</option>
                              <option value="3">Data de nascimento</option>
                              <option value="4">Número do SUS</option>
                              <option value="5">Médico</option>
                              <option value="6">Endereço</option>
                            </select>
                        </span>
                    </div>
                </div>
                <div class="col-md-3 coluna-ordem">
                    <div class="input-group ordenacao">
                        <label class="form-control">Ordenar por:</label>
                        <span class="input-group-btn">
                            <select id="select_ordem" class="btn btn-default">
                                <option value="1" selected>Nome</option>
                                <option value="2">Nascimento</option>
                                <option value="3">Número SUS</option>
                                <option value="4">Médico</option>
                                <option value="5">Endereço</option>
                            </select>
                        </span>
                    </div>
                </div>
                <div class="col-md-2 checkbox">
                    <label><input id="check_desc" type="checkbox" value="1"><strong>Decrescente</strong></label>
                </div>
            </form>
            <br>

            <div class="table-responsive" style="white-space: nowrap;">
                <table id="registros" class="table table-hover table-bordered">
                </table>
            </div>

        </div> <!-- page-content-wrapper -->
    </div>

 <script src="../js/bootstrap.min.js"></script>
</body>
</html>