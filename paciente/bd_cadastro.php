<?php

  if(!isset($_SESSION)) {
    session_start();
  }

  if(isset($_POST['nome'])):

    require_once "../conexao_bd/conexao_db_syspront.php";
    require_once "../conexao_bd/db_syspront.class.php";

    $db_syspront = sysPront::getInstance(Conexao::getInstance());

    $nome = $_POST['nome'];
    $dt_nascimento = $_POST['dt_nascimento'];
    $sexo = $_POST['sexo'];
    $nome_mae = $_POST['nome_mae'];
    $naturalidade = $_POST['naturalidade'] . '/' . $_POST['nat_uf'];
    $cep = $_POST['cep'];
    $endereco = $_POST['endereco'];
    $numero = $_POST['numero'];
    $complemento = $_POST['complemento'];
    $bairro = $_POST['bairro'];
    $cidade = $_POST['cidade'];
    $estado = $_POST['estado'];
    $telefone1 = $_POST['telefone1'];
    $telefone2 = $_POST['telefone2'];
    $num_sus = $_POST['num_sus'];
    $id_medico = $_POST['id_medico'];
    $diagnostico = $_POST['diagnostico'];

    $dt_nascimento = new DateTime(str_replace('/', '-', $dt_nascimento));
    $data_formatada = $dt_nascimento->format('Y-m-d');

    $erro = $db_syspront->insert_paciente($nome, $data_formatada, $sexo, $nome_mae, $naturalidade, $cep, $endereco, $numero, $complemento, $bairro, $cidade, $estado, $telefone1, $telefone2, $num_sus, $id_medico, $diagnostico);

    if($erro == 1062){
      header('Location: cadastro.php#success=false');
    } else {
      $db_syspront->gravar('../', 'Acao: cadastro_paciente; Usuario: ' . $_SESSION['usuario'] . '; ');
      header('Location: cadastro.php#success=true');
    }

  else:
    if(!isset($_SESSION['usuario'])) header('Location: ../index.php#erro=1');
    else header('Location: consulta.php');

  endif;