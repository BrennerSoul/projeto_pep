<?php

    if(!isset($_SESSION)) {
        session_start();
    }

    if(!isset($_SESSION['usuario']) || $_SESSION['tipo_usuario'] != 'Administrador'){
        header('Location: ../index.php#erro=1');
    }

?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
 <meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="shortcut icon" href="../img/favicon.ico">
 <title>Projeto SysPront Fisioterapia - Cadastro de pacientes</title>

 <script type="text/javascript" src="../js/jquery.min.js"></script>
 <script type="text/javascript" src="../js/bootstrap-datetimepicker.js"></script>
 <link rel="stylesheet" type="text/css" media="screen" href="../css/datetimepicker.css">
 <script type="text/javascript" src="../js/jquery.mask.min.js"></script>
 <link href="../css/bootstrap.min.css" rel="stylesheet">
 <link rel="stylesheet" type="text/css" href="../css/sys_pront_custom.css">

 <script type="text/javascript">

    $(document).ready(function(){

/*#################################################################################################*/
        //Controle do timeout
        var intervalo = '';

        contagem_timeout();
        intervalo = setTimeout(contagem_timeout, 1801000);

        function contagem_timeout(){
            $.ajax({
                url: '../sessao_timeout.php',
                method: 'post',
                data: { sessao: true },
                success: function(data){
                    if(data == 'logout'){
                        $.ajax({
                            url: '../logout.php',
                            method: 'post',
                            data: { logout: true,
                                    timeout: true },
                            success: function(data){
                                window.location.href = '../' + data;
                            }
                        });
                    }
                }
            });
        }

        $(document).off('mousedown').on('mousedown', function(){
            clearTimeout(intervalo);
            intervalo = setTimeout(contagem_timeout, 1801000);
        });

        $(document).off('mousemove').on('mousemove', function(){
            clearTimeout(intervalo);
            intervalo = setTimeout(contagem_timeout, 1801000);
        });

        $(document).off('keydown').on('keydown', function(){
            clearTimeout(intervalo);
            intervalo = setTimeout(contagem_timeout, 1801000);
        });

/*#################################################################################################*/
        //Controle da sidebar de acordo com o tamanho da tela
        //Ao redimensionar a tela, certifica-se de que a sidebar esteja na posição correta
        $(window).resize(function(){

            if($(window).width() >= 992){
                $("#sidebar-wrapper").css('width', 220);
                $("#page-content-wrapper").css('padding-left', 10);
                $("#page-content-wrapper").css('margin-left', 230);
            } else {
                $("#sidebar-wrapper").css('width', 0);
                $("#page-content-wrapper").css('padding-left', 15);
                $("#page-content-wrapper").css('margin-left', 0);
            }

        });

        //Ao clicar, mostra a sidebar
        $("#mostra-menu").click(function(e) {
            e.preventDefault();
            $("#sidebar-wrapper").css('width', 220);
            $("#page-content-wrapper").css('padding-left', 10);
            $("#page-content-wrapper").css('margin-left', 0);
        });

        //Ao clicar, fecha a sidebar
        $("#fecha-menu").click(function(e) {
            e.preventDefault();
            $("#sidebar-wrapper").css('width', 0);
            $("#page-content-wrapper").css('padding-left', 15);
            $("#page-content-wrapper").css('margin-left', 0);
        });

/*#################################################################################################*/

        $('#page-content-wrapper').fadeIn(500);
        $('#nome').focus();

        //Controle e opções do datetimepicker
        $('.form_datetime').datetimepicker({
            format: 'dd/mm/yyyy',
            minView: 2,
            startDate: '-150y',
            endDate: '+2m',
            weekStart: 1,
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0,
            showMeridian: 0,
            pickTime: false,
            pickerPosition: "bottom-left"
        });

        var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        spOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
            }
        };

        $('#num_sus').mask('0000000000000009');
        $('#cep').mask('00000-000');
        $('#telefone1').mask(SPMaskBehavior, spOptions);
        $('#telefone2').mask(SPMaskBehavior, spOptions);

        $('#numero').on('input',function(e){
            if($('#numero').val() < 0) $('#numero').val(0);
            else if($('#numero').val() > 9999999999) $('#numero').val(9999999999);
        });

        $('#cep').on('input',function(e){

            var cep = $('#cep').cleanVal();
            //Verifica se campo cep possui valor informado.
            if (cep != '' && cep.length >= 8) {

                //Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;
                //Valida o formato do CEP.
                if(validacep.test(cep)) {

                    //Preenche os campos com "..." enquanto consulta webservice.
                    $("#endereco").val("...");
                    $("#bairro").val("...");
                    $("#cidade").val("...");
                    $("#estado").val("...");
                    $('#cep').css('border-color', '');
                    $('.alert').fadeOut();
                    $('#warning-text').html('');

                    //Consulta o webservice viacep.com.br/
                    $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                        if (!("erro" in dados)) {
                            //Atualiza os campos com os valores da consulta.
                            $("#endereco").val(dados.logradouro);
                            $("#bairro").val(dados.bairro);
                            $("#cidade").val(dados.localidade);
                            $("#estado").val(dados.uf);
                            $('#cep').css('border-color', '');
                            $('.alert').fadeOut();
                            $('#warning-text').html('');
                        } //end if.
                        else {
                            //CEP pesquisado não foi encontrado.
                            $("#endereco").val("");
                            $("#bairro").val("");
                            $("#cidade").val("");
                            $("#estado").val("");
                            $('#cep').css('border-color', 'red');
                            $('.alert').fadeIn();
                            $('#warning-text').html('CEP não encontrado!');
                            $('html, body').animate({
                                scrollTop: ($('#warning-text').offset().top)
                            },1000);
                        }
                    });
                } //end if.
                else {
                    //cep é inválido.
                    $("#endereco").val("");
                    $("#bairro").val("");
                    $("#cidade").val("");
                    $("#estado").val("");
                    $('#cep').css('border-color', 'red');
                    $('.alert').fadeIn();
                    $('#warning-text').html('Formato de CEP inválido!');
                    $('html, body').animate({
                        scrollTop: ($('#warning-text').offset().top)
                    },1000);
                }
            } //end if.
        });

        var hash_url = $(location).attr('hash');

        if(hash_url == '#success=true'){

            $('#modal-sucesso').modal();
            $('#page-content-wrapper').hide();

        } else if(hash_url == '#success=false'){

            $('#modal-erro-cadastro').modal();
            $('#page-content-wrapper').hide();

        }

        //Ao fechar modal sucesso
        $('#modal-sucesso').on('hidden.bs.modal', function (e) {
            window.location.replace('cadastro.php');
        });

        //Ao fechar modal sucesso
        $('#modal-erro-cadastro').on('hidden.bs.modal', function (e) {
            window.location.replace('cadastro.php');
        });

        $(document).off('keydown').keydown(function(event){
            var keycode = (event.keyCode ? event.keyCode : event.which);

            //Tecla enter
            if(keycode == '13' && !$('#diagnostico').is(":focus")){
                submit_formulario();
            } else
            if(keycode == '27'){
                $('#modal-sucesso').modal('hide');
                $('#modal-medico').modal('hide');
            }
        });

        //Validações dos campos de cadastro
        $('#btn-logar').off('click').click(function(){

            submit_formulario();

        });

        //Fecha os avisos
        $('#btn-close-warning').off('click').click(function(){

            $('html, body').animate({
                scrollTop: 31,
            }, 700, function(){
                $('.alert').fadeOut();
            });

        });

        $('.btn-reset').off('click').click(function(){

            $('html, body').animate({
                scrollTop: 31,
            }, 700, function(){
                $('.alert').fadeOut();

            });

            $('#nome').css('border-color', '');
            $('#nome').val('');
            $('#dt_nascimento').css('border-color', '');
            $('#dt_nascimento').val('');
            $('#sexo').css('border-color', '');
            $('#sexo').val('-');
            $('#nome_mae').css('border-color', '');
            $('#nome_mae').val('');
            $('#naturalidade').css('border-color', '');
            $('#naturalidade').val('');
            $('#nat_uf').css('border-color', '');
            $('#nat_uf').val('-');
            $('#cep').css('border-color', '');
            $('#cep').val('');
            $('#endereco').css('border-color', '');
            $('#endereco').val('');
            $('#numero').css('border-color', '');
            $('#numero').val('');
            $('#complemento').css('border-color', '');
            $('#complemento').val('');
            $('#bairro').css('border-color', '');
            $('#bairro').val('');
            $('#cidade').css('border-color', '');
            $('#cidade').val('');
            $('#estado').css('border-color', '');
            $('#estado').val('-');
            $('#telefone1').css('border-color', '');
            $('#telefone1').val('');
            $('#telefone2').css('border-color', '');
            $('#telefone2').val('');
            $('#num_sus').css('border-color', '');
            $('#num_sus').val('');
            $('#nome_medico').css('border-color', '');
            $('#nome_medico').val('');
            $('#diagnostico').css('border-color', '');
            $('#diagnostico').val('');
            $('#nome').focus();

        });

        $('a').off('click').click(function(){
            if($(this).attr('id') == 'btn_logout'){
                $.ajax({
                    url: '../logout.php',
                    method: 'post',
                    data: { logout: true },
                    success: function(data){
                        window.location.href = '../' + data;
                    }
                });
            } else {
                event.preventDefault();
                linkLocation = this.href;
                $('#page-content-wrapper').fadeOut(200, function(){
                    window.location.href = linkLocation;
                });
            }
        });

        $('#btn_buscar_medico').off('click').click(function(){

            $('#modal-medico').modal();

            busca_medico_no_banco();

        });

        //Ao preencher a caixa de pesquisa, busca as informações no banco de dados
        $('#tag_pesquisa').off('input').on('input',function(e){

            busca_medico_no_banco();

        });

        //Ao mudar o select da ordem, busca as informações no banco de dados
        $('#select_ordem').off('change').on('change',function(e){

            busca_medico_no_banco();

        });

        //Verifica se o checkbox decrescente está ativo ou não
        var check_desc = false;

        $('#check_desc').off('click').click(function(){

            if ($('#check_desc').is(":checked"))
            {
                check_desc = true;
            } else {
                check_desc = false;
            }

            busca_medico_no_banco();

        });

        //Limpa os campos de pesquisa e ordenacao quando a modal é fechada
        $('#modal-medico').on('hidden.bs.modal', function (e) {
            $('#tag_pesquisa').val('');
            $('#select_ordem').val(2);
            $('#check_desc').prop('checked', false);
        });

        function submit_formulario(){

            var campos_vazios = false;
            var warning_text = '';

            if($('#nome').val() == ''){
                warning_text += 'Campo Nome completo deve ser preenchido!<br>';
                $('#nome').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#nome').css('border-color', '');
            }

            if($('#dt_nascimento').val() == ''){
                warning_text += 'Campo Data de nascimento deve ser preenchido!<br>';
                $('#dt_nascimento').css('border-color', 'red');

                campos_vazios = true;
            } else {
                $('#dt_nascimento').css('border-color', '');
            }

            if($('#sexo').val() == '-'){
                warning_text += 'Campo Sexo deve ser preenchido!<br>';
                $('#sexo').css('border-color', 'red');

                campos_vazios = true;
            } else {
                $('#sexo').css('border-color', '');
            }

            if($('#nome_mae').val() == ''){
                warning_text += 'Campo Nome da mãe deve ser preenchido!<br>';
                $('#nome_mae').css('border-color', 'red');

                campos_vazios = true;
            } else {
                $('#nome_mae').css('border-color', '');
            }

            if($('#naturalidade').val() == ''){
                warning_text += 'Campo Naturalidade deve ser preenchido!<br>';
                $('#naturalidade').css('border-color', 'red');

                campos_vazios = true;
            } else {
                $('#naturalidade').css('border-color', '');
            }

            if($('#nat_uf').val() == '-'){
                warning_text += 'Campo UF da naturalidade deve ser preenchido!<br>';
                $('#nat_uf').css('border-color', 'red');

                campos_vazios = true;
            } else {
                $('#nat_uf').css('border-color', '');
            }

            if($('#cep').val() == ''){
                warning_text += 'Campo CEP deve ser preenchido!<br>';
                $('#cep').css('border-color', 'red');

                campos_vazios = true;
            } else {
                $('#cep').css('border-color', '');
            }

            if($('#endereco').val() == ''){
                warning_text += 'Campo Endereço deve ser preenchido!<br>';
                $('#endereco').css('border-color', 'red');

                campos_vazios = true;
            } else {
                $('#endereco').css('border-color', '');
            }

            if($('#numero').val() == ''){
                warning_text += 'Campo Número deve ser preenchido!<br>';
                $('#numero').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#numero').css('border-color', '');
            }

            if($('#bairro').val() == ''){
                warning_text += 'Campo Bairro deve ser preenchido!<br>';
                $('#bairro').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#bairro').css('border-color', '');
            }

            if($('#cidade').val() == ''){
                warning_text += 'Campo Cidade deve ser preenchido!<br>';
                $('#cidade').css('border-color', 'red');

                campos_vazios = true;
            } else {
                $('#cidade').css('border-color', '');
            }

            if($('#estado').val() == '-'){
                warning_text += 'Campo Estado deve ser preenchido!<br>';
                $('#estado').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#estado').css('border-color', '');
            }

            if($('#telefone1').val() == ''){
                warning_text += 'Campo Telefone 1 deve ser preenchido!<br>';
                $('#telefone1').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#telefone1').css('border-color', '');
            }

            if($('#num_sus').val() == ''){
                warning_text += 'Campo Número do SUS deve ser preenchido!<br>';
                $('#num_sus').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#num_sus').css('border-color', '');
            }

            if($('#nome_medico').val() == ''){
                warning_text += 'Campo Nome do médico deve ser preenchido!<br>';
                $('#nome_medico').css('border-color', 'red');

                campos_vazios = true;
            } else {
                $('#nome_medico').css('border-color', '');
            }

            if($('#diagnostico').val() == ''){
                warning_text += 'Campo Diagnóstico deve ser preenchido!<br>';
                $('#diagnostico').css('border-color', 'red');
                campos_vazios = true;
            } else {
                $('#diagnostico').css('border-color', '');
            }

            if(campos_vazios){
                $('.alert').fadeIn();
                $('#warning-text').html(warning_text);
                $('html, body').animate({
                    scrollTop: ($('#warning-text').offset().top)
                },1000);
            } else {
                $('.alert').fadeOut();
                $('#form-cadastro').attr("action","bd_cadastro.php");
                $('#page-content-wrapper').fadeOut(200, function(){
                    $("#form-cadastro").submit();
                });
            }

        }

        function busca_medico_no_banco(){

            $.ajax({
                url: 'bd_busca_medico.php',
                method: 'post',
                data: { tag_pesquisa: $('#tag_pesquisa').val(),
                        select_ordem: $('#select_ordem').val(),
                        check_desc: check_desc },
                success: function(data){
                    $('#div_medico').html(data);

                    $('.tr_medico').off('click').click(function(){
                        $('#id_medico').val($(this).attr("id"));
                        $('#nome_medico').val($(this).data('nome_medico'));
                        $('#modal-medico').modal('hide');
                    });
                }
            });

        }

    });

 </script>

</head>
<body>

    <!-- Barra de navegação -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button id="btn_menu" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar_menu" aria-expanded="false" aria-controls="navbar" title="Menu">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div id="navbar" style="padding-left: 5px; padding-top: 2px;">
                    <h4>SISTEMA DE CONTROLE DE PRONTUÁRIO DE ESTAGIÁRIOS DO CURSO DE FISIOTERAPIA</h4>
                </div>
            </div>
            <div id="navbar" class="navbar-collapse collapse navbar_menu" style="padding-right: 5px;">
                <ul class="nav navbar-nav navbar-right" style="font-size: 17px;">
                    <li><h4 style="color: white; text-shadow: 2px 2px #000; margin-right: 20px; margin-top: 15px; padding-left: 5px;">Bem vindo, <?=$_SESSION['nome_do_usuario']?></h4></li>
                    <li><a href="../prontuario/cadastro.php" title="Início">
                        <span class="glyphicon glyphicon-home"></span>
                    </a></li>
                    <li><a href="../perfil/consulta.php" title="Gerenciar perfis">
                        <span class="glyphicon glyphicon-user"></span>
                    </a></li>
                    <li><a style="cursor: pointer;" id="btn_logout" title="Sair">
                        <span class="glyphicon glyphicon-log-out"></span>
                    </a></li>
                </ul>
            </div>
        </div>
    </nav> <!-- navbar -->

    <div class="container-fluid">
        <div>
            <button id="mostra-menu" type="button" title="Mostrar menu">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </button>

            <!-- Sidebar -->
            <div id="sidebar-wrapper">
                <ul class="sidebar-nav">
                    <button id="fecha-menu" title="Fechar" type="button" class="close" aria-label="Close" style="background: white; margin-right: 20px; padding: 0px 3px;">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <li class="topicos"> <strong style="font-size: 17px">CADASTROS</strong> </li>
                    <li>
                        <a href="../medico/cadastro.php"><strong>MÉDICO</strong></a>
                    </li>
                    <li>
                        <a href="../docente/cadastro.php"><strong>DOCENTE</strong></a>
                    </li>
                    <li class="selected">
                        <a href="cadastro.php"><strong>PACIENTE</strong></a>
                    </li>
                    <li>
                        <a href="../discente/cadastro.php"><strong>DISCENTE</strong></a>
                    </li>
                    <li class="topicos"> <strong style="font-size: 17px">MOVIMENTOS</strong> </li>
                    <li>
                        <a href="../prontuario/cadastro.php"><strong>PRONTUÁRIO</strong></a>
                    </li>
                    <li class="topicos"> <strong style="font-size: 17px">CONSULTAS</strong> </li>
                    <li>
                        <a href="../prontuario/consulta.php"><strong>PRONTUÁRIO</strong></a>
                    </li>
                    <li>
                        <a href="../medico/consulta.php"><strong>MÉDICO</strong></a>
                    </li>
                    <li>
                        <a href="../docente/consulta.php"><strong>DOCENTE</strong></a>
                    </li>
                    <li>
                        <a href="consulta.php"><strong>PACIENTE</strong></a>
                    </li>
                    <li>
                        <a href="../discente/consulta.php"><strong>DISCENTE</strong></a>
                    </li>
                </ul>
            </div> <!-- /#sidebar-wrapper -->

        </div>

        <!-- Modal cadastro com sucesso -->
        <div class="modal fade" id="modal-sucesso">
            <div class="modal-dialog">
                <div class="modal-content" style="border: 3px solid black">
                    <div class="modal-header" style="text-align: center;">
                        <button type="button" class="close" data-dismiss="modal"><span style="color: red">×</span></button>
                        <h4 class="modal-title" style="color: #01C325;">Registro inserido com sucesso!</h4>
                    </div>
                </div>
            </div>
        </div> <!--Modal-->

        <!-- Modal de erro de cadastro -->
        <div class="modal fade" id="modal-erro-cadastro">
            <div class="modal-dialog">
                <div class="modal-content" style="border: 3px solid #FA5B5B;">
                    <div class="modal-header" style="text-align: center;">
                        <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                        <h4 class="modal-title" style="color: red;">ERRO AO CADASTRAR PACIENTE</h4>
                    </div>
                    <div class="modal-body" style="text-align: center; font-weight: bold;">
                        Não é possível cadastrar este paciente, porque já existe um registro com este número do SUS! Por favor, escolha um número do SUS disponível!
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
        </div> <!-- Modal -->

        <div id="page-content-wrapper" style="display: none;">

            <!-- Modal médico -->
            <div class="modal fade" id="modal-medico">
                <div class="modal-dialog modal-mensagem-class">
                    <div class="modal-content">
                        <div class="modal-header" style="text-align: center;">
                            <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                            <h4 class="modal-title">Médicos Cadastrados</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-6 coluna-pesquisa">
                                    <input type="text" id="tag_pesquisa" class="form-control" placeholder="Pesquise aqui..." maxlength="160">
                                </div>
                                <div class="col-md-4 coluna-ordem">
                                    <div class="input-group ordenacao">
                                        <label class="form-control">Ordenar por:</label>
                                        <span class="input-group-btn">
                                            <select id="select_ordem" class="btn btn-default">
                                                <option value="1">CRM</option>
                                                <option value="2" selected>Nome</option>
                                                <option value="3">Especialidade</option>
                                            </select>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-2 checkbox">
                                    <label><input id="check_desc" type="checkbox" value="1"><strong>Decrescente</strong></label>
                                </div>
                            </div>
                            <hr>
                            <div id="div_medico"></div>
                        </div>
                    </div>
                </div>
            </div>

            <h3 class="page-header">Adicionar Paciente</h3>

            <form id="form-cadastro" action="" method="post">
            <!-- area de campos do form -->
                <div class="row">
                    <div class="form-group col-sm-7">
                        <label for="nome">Nome completo <span style="color: red;">*</span></label>
                        <input type="text" class="form-control" id="nome" name="nome" maxlength="160">
                    </div>
                    <div class="form-group col-sm-3">
                        <label for="dt_nascimento">Data de nascimento <span style="color: red;">*</span></label>
                        <div class="input-group date form_datetime col-md-5" data-link-field="dtp_input1" style="width: 100%;">
                            <input class="form-control" size="16" type="text" id="dt_nascimento" name="dt_nascimento" readonly>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                        </div>
                        <input type="hidden" id="dtp_input1" value="">
                    </div>
                    <div class="form-group col-sm-2">
                        <label for="sexo">Sexo <span style="color: red;">*</span></label>
                        <select class="form-control" id="sexo" name="sexo">
                            <option value="-" style="display: none;" selected></option>
                            <option value="F">Feminino</option>
                            <option value="M">Masculino</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-sm-7">
                        <label for="nome_mae">Nome da mãe <span style="color: red;">*</span></label>
                        <input type="text" class="form-control" id="nome_mae" name="nome_mae" maxlength="160">
                    </div>
                    <div class="form-group col-sm-5">
                        <label for="naturalidade">Naturalidade <span style="color: red;">*</span></label>
                        <div class="input-group">
                            <input type="text" placeholder="Nome da cidade" class="form-control" id="naturalidade" name="naturalidade" maxlength="40">
                            <span class="input-group-btn">
                                <select class="btn btn-default" style="border: 2px solid gray;" id="nat_uf" name="nat_uf">
                                    <option value="-" style="display: none;" selected>UF</option>
                                    <option value="AC">AC</option>
                                    <option value="AL">AL</option>
                                    <option value="AP">AP</option>
                                    <option value="AM">AM</option>
                                    <option value="BA">BA</option>
                                    <option value="CE">CE</option>
                                    <option value="DF">DF</option>
                                    <option value="ES">ES</option>
                                    <option value="GO">GO</option>
                                    <option value="MA">MA</option>
                                    <option value="MT">MT</option>
                                    <option value="MS">MS</option>
                                    <option value="MG">MG</option>
                                    <option value="PA">PA</option>
                                    <option value="PB">PB</option>
                                    <option value="PR">PR</option>
                                    <option value="PE">PE</option>
                                    <option value="PI">PI</option>
                                    <option value="RJ">RJ</option>
                                    <option value="RN">RN</option>
                                    <option value="RS">RS</option>
                                    <option value="RO">RO</option>
                                    <option value="RR">RR</option>
                                    <option value="SC">SC</option>
                                    <option value="SP">SP</option>
                                    <option value="SE">SE</option>
                                    <option value="TO">TO</option>
                                </select>
                            </span>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="form-group col-sm-2">
                        <label for="cep">CEP <span style="color: red;">*</span></label>
                        <input type="text" class="form-control" id="cep" name="cep">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="endereco">Endereço <span style="color: red;">*</span></label>
                        <input type="text" class="form-control" id="endereco" name="endereco" maxlength="90">
                    </div>
                    <div class="form-group col-sm-2">
                        <label for="numero">Número <span style="color: red;">*</span></label>
                        <input type="number" class="form-control" id="numero" name="numero">
                    </div>
                    <div class="form-group col-sm-2">
                        <label for="complemento">Complemento</label>
                        <input type="text" class="form-control" id="complemento" name="complemento" maxlength="40">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-sm-5">
                        <label for="bairro">Bairro <span style="color: red;">*</span></label>
                        <input type="text" class="form-control" id="bairro" name="bairro" maxlength="60">
                    </div>
                    <div class="form-group col-sm-5">
                        <label for="cidade">Cidade <span style="color: red;">*</span></label>
                        <input type="text" class="form-control" id="cidade" name="cidade" maxlength="40">
                    </div>
                    <div class="form-group col-sm-2">
                        <label for="estado">Estado <span style="color: red;">*</span></label>
                        <select class="form-control" id="estado" name="estado">
                            <option value="-" style="display: none;" selected></option>
                            <option value="AC">AC</option>
                            <option value="AL">AL</option>
                            <option value="AP">AP</option>
                            <option value="AM">AM</option>
                            <option value="BA">BA</option>
                            <option value="CE">CE</option>
                            <option value="DF">DF</option>
                            <option value="ES">ES</option>
                            <option value="GO">GO</option>
                            <option value="MA">MA</option>
                            <option value="MT">MT</option>
                            <option value="MS">MS</option>
                            <option value="MG">MG</option>
                            <option value="PA">PA</option>
                            <option value="PB">PB</option>
                            <option value="PR">PR</option>
                            <option value="PE">PE</option>
                            <option value="PI">PI</option>
                            <option value="RJ">RJ</option>
                            <option value="RN">RN</option>
                            <option value="RS">RS</option>
                            <option value="RO">RO</option>
                            <option value="RR">RR</option>
                            <option value="SC">SC</option>
                            <option value="SP">SP</option>
                            <option value="SE">SE</option>
                            <option value="TO">TO</option>
                        </select>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="form-group col-sm-6">
                        <label for="telefone1">Telefone 1 <span style="color: red;">*</span></label>
                        <input type="text" class="form-control" id="telefone1" name="telefone1" placeholder="DDD + número">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="telefone2">Telefone 2</label>
                        <input type="text" class="form-control" id="telefone2" name="telefone2" placeholder="DDD + número">
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="form-group col-sm-5">
                        <label for="num_sus">Número do SUS <span style="color: red;">*</span></label>
                        <input type="text" class="form-control" id="num_sus" name="num_sus" maxlength="20">
                    </div>
                    <div class="form-group col-sm-7">
                        <input type="hidden" name="id_medico" id="id_medico">
                        <label for="nome_medico">Nome do médico <span style="color: red;">*</span></label>
                        <div class="input-group">
                            <input type="text" placeholder="Clique em &quot;Buscar&quot; para encontrar um médico ..." class="form-control" id="nome_medico" readonly="readonly">
                            <span class="input-group-btn">
                                <button class="btn btn-primary" id="btn_buscar_medico" type="button">Buscar médico</button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-sm-12">
                        <label for="diagnostico">Diagnóstico <span style="color: red;">*</span></label>
                        <textarea class="form-control" rows="3" id="diagnostico" name="diagnostico" maxlength="5000"></textarea>
                    </div>
                </div>
                <hr>
                <div id="actions" class="row">
                    <div class="col-md-12" style="text-align: center; margin-bottom: 15px">
                        <button id="btn-logar" type="button" class="btn btn-primary">Salvar</button>
                        <a href="consulta.php" class="btn btn-default">Cancelar</a>
                        <button type="reset" class="btn btn-danger btn-reset">Resetar</button>
                        <a href="consulta.php" class="btn btn-warning" style="color: black">Ver pacientes cadastrados</a>
                    </div>
                </div>
                <div class="row">
                    <div class="alert alert-danger" style="display: none; text-align: center;">
                        <button id="btn-close-warning" type="button" class="close" title="Fechar"><span>×</span></button>
                        <strong id="warning-text"></strong>
                    </div>
                </div>
            </form>
        </div> <!-- page-content-wrapper -->
    </div>

 <script src="../js/bootstrap.min.js"></script>
</body>
</html>